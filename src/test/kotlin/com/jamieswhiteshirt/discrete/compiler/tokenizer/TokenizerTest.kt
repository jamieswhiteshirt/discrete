package com.jamieswhiteshirt.discrete.compiler.tokenizer

import com.jamieswhiteshirt.discrete.compiler.ast.operators.operators
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.*
import com.jamieswhiteshirt.util.DirectExecutor
import io.kotlintest.specs.FlatSpec
import java.util.*

class TokenizerTest : FlatSpec() {
    private val tokenizer = Tokenizer(DirectExecutor)

    private fun tokenizeString(string: String): List<Token> {
        val iterable = tokenizer.read(string.byteInputStream())
        val list = ArrayList<Token>()
        var iter = iterable.begin
        while (iter < iterable.end) {
            list.add(iter.value)
            iter++
        }
        return list
    }

    private fun Token.shouldEqualSelf() {
        val iterable = tokenizer.read(toString().byteInputStream())
        iterable.end - iterable.begin shouldBe 1
        iterable.begin.value shouldEqual this
    }

    init {
        " " should "be zero tokens" {
            val string = " "
            val tokens = tokenizeString(string)
            tokens.size shouldBe 0
        }
        "#comment" should "be zero tokens" {
            val string = "#comment"
            val tokens = tokenizeString(string)
            tokens.size shouldBe 0
        }
        ";" should "be one end of statement token" {
            EndOfStatementToken().shouldEqualSelf()
        }
        "." should "be one separator token" {
            SeparatorToken().shouldEqualSelf()
        }
        "\"string\"" should "be one string literal token" {
            StringLiteralToken("\"string\"").shouldEqualSelf()
        }
        "1.0" should "be one float literal token" {
            FloatLiteralToken("1.0")
        }
        "1" should "be one int literal token" {
            IntLiteralToken("1").shouldEqualSelf()
        }
        "all unambiguous operators" should "be operator tokens" {
            for (operator in operators.keys) {
                OperatorToken(operator).shouldEqualSelf()
            }
        }
        "word" should "be one word token" {
            WordToken("word").shouldEqualSelf()
        }
        "(" should "be one symbol token" {
            SymbolToken('(').shouldEqualSelf()
        }
    }
}
