package com.jamieswhiteshirt.discrete.stl

import com.jamieswhiteshirt.discrete.common.invokable.LambdaInvokable
import com.jamieswhiteshirt.discrete.common.module.library.ReflectionLibrary
import com.jamieswhiteshirt.discrete.common.value.FunctionValue
import com.jamieswhiteshirt.discrete.common.value.ImmutableValue
import com.jamieswhiteshirt.discrete.common.value.Value
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution

object StandardLibrary : ReflectionLibrary() {
    override val moduleName = "STL"

    override fun execute(globalExecution: GlobalExecution) { }

    @LibraryField
    val println: ImmutableValue = FunctionValue(LambdaInvokable(fun(params: Array<Value>) {
        println(params.map { it.string.value }.joinToString(" "))
    }))

    @LibraryField
    val print: ImmutableValue = FunctionValue(LambdaInvokable(fun(params: Array<Value>) {
        print(params.map { it.string.value }.joinToString(" "))
    }))

}
