package com.jamieswhiteshirt.discrete.common.code.scope

import com.jamieswhiteshirt.discrete.common.code.LocalVariableDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.common.code.ScopeDefinition

class FunctionScopeDefinition(
        blocks: Array<BlockScopeDefinition>,
        functions: Array<FunctionScopeDefinition>,
        labels: Array<Int>,
        instructions: Array<Instruction>,
        localVariables: Array<LocalVariableDefinition>,
        val parameterCount: Int
) : ScopeDefinition(blocks, functions, labels, instructions, localVariables)
