package com.jamieswhiteshirt.discrete.common.code.scope

import com.jamieswhiteshirt.discrete.common.code.LocalVariableDefinition
import com.jamieswhiteshirt.discrete.common.code.ModuleVariableDefinition
import com.jamieswhiteshirt.discrete.common.code.ScopeDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.common.module.ModuleDefinition
import java.util.*

class ModuleScopeDefinition(
        override val moduleName: String,
        blocks: Array<BlockScopeDefinition>,
        functions: Array<FunctionScopeDefinition>,
        labels: Array<Int>,
        instructions: Array<Instruction>,
        localVariables: Array<LocalVariableDefinition>,
        val moduleVariables: Array<ModuleVariableDefinition>
) : ScopeDefinition(blocks, functions, labels, instructions, localVariables), ModuleDefinition {
    override val identifiers: Set<String> = HashSet(moduleVariables.map { it.identifier })
}
