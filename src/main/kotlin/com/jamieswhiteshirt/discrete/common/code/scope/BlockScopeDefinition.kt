package com.jamieswhiteshirt.discrete.common.code.scope

import com.jamieswhiteshirt.discrete.common.code.LocalVariableDefinition
import com.jamieswhiteshirt.discrete.common.code.ScopeDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction

class BlockScopeDefinition(
        blocks: Array<BlockScopeDefinition>,
        functions: Array<FunctionScopeDefinition>,
        labels: Array<Int>,
        instructions: Array<Instruction>,
        localVariables: Array<LocalVariableDefinition>
) : ScopeDefinition(blocks, functions, labels, instructions, localVariables)
