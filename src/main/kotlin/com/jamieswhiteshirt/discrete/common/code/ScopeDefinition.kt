package com.jamieswhiteshirt.discrete.common.code

import com.jamieswhiteshirt.discrete.common.code.scope.BlockScopeDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.FunctionScopeDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction

abstract class ScopeDefinition(
        val blocks: Array<BlockScopeDefinition>,
        val functions: Array<FunctionScopeDefinition>,
        val labels: Array<Int>,
        val instructions: Array<Instruction>,
        val localVariables: Array<LocalVariableDefinition>
)
