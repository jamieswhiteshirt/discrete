package com.jamieswhiteshirt.discrete.common.code

import com.jamieswhiteshirt.discrete.common.value.ImmutableValue
import com.jamieswhiteshirt.discrete.common.value.ReferenceMutableValue
import com.jamieswhiteshirt.discrete.common.value.Value

sealed class VariableMutability(val typeName: String) {
    object Var : VariableMutability("var") {
        override fun instantiate(initialValue: ImmutableValue): Value = ReferenceMutableValue(initialValue)
    }

    object Val : VariableMutability("val") {
        override fun instantiate(initialValue: ImmutableValue): Value = initialValue
    }

    abstract fun instantiate(initialValue: ImmutableValue): Value

    override fun toString(): String = typeName
}
