package com.jamieswhiteshirt.discrete.common.code

import com.jamieswhiteshirt.discrete.common.value.ImmutableValue
import com.jamieswhiteshirt.discrete.common.value.Value

abstract class VariableDefinition(val mutability: VariableMutability) {
    fun instantiate(initialValue: ImmutableValue): Value = mutability.instantiate(initialValue)
}

class LocalVariableDefinition(mutability: VariableMutability) : VariableDefinition(mutability)

class ModuleVariableDefinition(val identifier: String, mutability: VariableMutability) : VariableDefinition(mutability)
