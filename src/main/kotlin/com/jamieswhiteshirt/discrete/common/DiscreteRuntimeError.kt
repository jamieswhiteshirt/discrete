package com.jamieswhiteshirt.discrete.common

abstract class DiscreteRuntimeError : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
}

class CastError : DiscreteRuntimeError {
    constructor() : super()
    constructor(message: String) : super(message)
}

class UndeclaredVariableError : DiscreteRuntimeError {
    constructor() : super()
    constructor(message: String) : super(message)
}

class InvokeError : DiscreteRuntimeError {
    constructor() : super()
    constructor(message: String) : super(message)
}

class InvalidArgumentError : DiscreteRuntimeError {
    constructor() : super()
    constructor(message: String) : super(message)
}

class InvalidInstructionError : DiscreteRuntimeError {
    constructor() : super()
    constructor(message: String) : super(message)
}
