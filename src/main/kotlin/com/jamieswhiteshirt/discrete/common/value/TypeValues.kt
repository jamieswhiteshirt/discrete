package com.jamieswhiteshirt.discrete.common.value

import com.jamieswhiteshirt.discrete.common.CastError
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution

abstract class TypeValue(val name: String): InvokableValue() {
    override fun toString(): String = name

    override val number: NumberValue get() = throw CastError()
    override val integer: IntegerValue get() = throw CastError()
    override val float: FloatValue get() = throw CastError()
    override val identity: IdentityValue get() = throw CastError()
    override val string: StringValue get() = StringValue(toString())
    override val boolean: BooleanValue get() = BooleanValue(true)
    override val function: FunctionValue get() = throw CastError()
    override val collection: CollectionValue get() = throw CastError()
    override val set: SetValue get() = throw CastError()
    override val tuple: TupleValue get() = throw CastError()
    override val mapping: MappingValue get() = throw CastError()
    override val type: TypeValue get() = this

    abstract fun isTypeOf(value: ImmutableValue): Boolean
    abstract fun convert(value: Value): Value

    override fun invoke(globalExecution: GlobalExecution, params: Array<Value>) {
        val value = params.firstOrNull()
        if (value != null) {
            convert(value)
        }
    }
    override fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value {
        val value = params.firstOrNull()
        if (value != null) {
            return convert(value)
        }
        else {
            return UndefinedTypeValue
        }
    }

    override fun equals(other: Any?) = this === other
    override fun hashCode() = name.hashCode()
}

object NumberTypeValue : TypeValue("number") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is NumberValue
    override fun convert(value: Value): Value = value.number
}
object IntegerTypeValue : TypeValue("integer") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is IntegerValue
    override fun convert(value: Value): Value = value.integer
}
object FloatTypeValue : TypeValue("float") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is FloatValue
    override fun convert(value: Value): Value = value.float
}
object IdentityTypeValue : TypeValue("identity") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is IdentityValue
    override fun convert(value: Value): Value = value.identity
}
object StringTypeValue : TypeValue("string") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is StringValue
    override fun convert(value: Value): Value = value.string
}
object BooleanTypeValue : TypeValue("boolean") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is BooleanValue
    override fun convert(value: Value): Value = value.boolean
}
object InvokableTypeValue : TypeValue("invokable") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is InvokableValue
    override fun convert(value: Value): Value = value.function
}
object FunctionTypeValue : TypeValue("function") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is FunctionValue
    override fun convert(value: Value): Value = value.function
}
object CollectionTypeValue : TypeValue("collection") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is CollectionValue
    override fun convert(value: Value): Value = value.set
}
object SetTypeValue : TypeValue("set") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is SetValue
    override fun convert(value: Value): Value = value.set
}
object TupleTypeValue : TypeValue("tuple") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is TupleValue
    override fun convert(value: Value): Value = value.tuple
}
object MappingTypeValue : TypeValue("mapping") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is MappingValue
    override fun convert(value: Value): Value = value.mapping
}
object TypeTypeValue : TypeValue("type") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is TypeValue
    override fun convert(value: Value): Value = value.type
}
object UndefinedTypeValue : TypeValue("undefined") {
    override fun isTypeOf(value: ImmutableValue): Boolean = value is UndefinedTypeValue
    override fun convert(value: Value): Value = this
}
