package com.jamieswhiteshirt.discrete.common.value

import com.jamieswhiteshirt.discrete.common.*
import com.jamieswhiteshirt.discrete.common.invokable.Invokable
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution

abstract class Value {
    abstract val mutable: MutableValue
    abstract val immutable: ImmutableValue
    abstract val number: NumberValue
    abstract val integer: IntegerValue
    abstract val float: FloatValue
    abstract val identity: IdentityValue
    abstract val string: StringValue
    abstract val boolean: BooleanValue
    abstract val invokable: InvokableValue
    abstract val function: FunctionValue
    abstract val collection: CollectionValue
    abstract val set: SetValue
    abstract val tuple: TupleValue
    abstract val mapping: MappingValue
    abstract val type: TypeValue

    override fun toString(): String = string.value
}

abstract class ImmutableValue : Value() {
    override val immutable: ImmutableValue get() = this
    override val mutable: MutableValue get() = throw CastError()

    abstract override fun equals(other: Any?): Boolean
    abstract override fun hashCode(): Int
}

abstract class MutableValue() : Value() {
    override val mutable: MutableValue get() = this

    abstract fun assign(value: ImmutableValue)
}

abstract class DelegateMutableValue() : MutableValue() {
    abstract var value: ImmutableValue

    override val immutable: ImmutableValue get() = value
    override val number: NumberValue get() = value.number
    override val integer: IntegerValue get() = value.integer
    override val float: FloatValue get() = value.float
    override val identity: IdentityValue get() = value.identity
    override val string: StringValue get() = value.string
    override val boolean: BooleanValue get() = value.boolean
    override val invokable: InvokableValue get() = value.invokable
    override val function: FunctionValue get() = value.function
    override val collection: CollectionValue get() = value.collection
    override val set: SetValue get() = value.set
    override val tuple: TupleValue get() = value.tuple
    override val mapping: MappingValue get() = value.mapping
    override val type: TypeValue get() = value.type

    override fun assign(value: ImmutableValue) {
        this.value = value
    }
}

class ReferenceMutableValue(override var value: ImmutableValue): DelegateMutableValue()

abstract class DelegateImmutableValue : ImmutableValue() {
    abstract val value: ImmutableValue

    override val number: NumberValue get() = value.number
    override val integer: IntegerValue get() = value.integer
    override val float: FloatValue get() = value.float
    override val identity: IdentityValue get() = value.identity
    override val string: StringValue get() = value.string
    override val boolean: BooleanValue get() = value.boolean
    override val invokable: InvokableValue get() = value.invokable
    override val function: FunctionValue get() = value.function
    override val collection: CollectionValue get() = value.collection
    override val set: SetValue get() = value.set
    override val tuple: TupleValue get() = value.tuple
    override val mapping: MappingValue get() = value.mapping
    override val type: TypeValue get() = value.type

    override fun equals(other: Any?) = value.equals(other)
    override fun hashCode() = value.hashCode()
}

abstract class NumberValue: ImmutableValue() {
    override val number: NumberValue get() = this

    abstract val floatValue: Float
}
class IntegerValue(val intValue: Int): NumberValue() {
    override val integer: IntegerValue get() = this
    override val float: FloatValue get() = FloatValue(floatValue)
    override val identity: IdentityValue get() = throw CastError()
    override val string: StringValue get() = StringValue(intValue.toString())
    override val boolean: BooleanValue get() = BooleanValue(intValue != 0)
    override val invokable: InvokableValue get() = throw CastError()
    override val function: FunctionValue get() = throw CastError()
    override val collection: CollectionValue get() = throw CastError()
    override val set: SetValue get() = throw CastError()
    override val tuple: TupleValue get() = throw CastError()
    override val mapping: MappingValue get() = throw CastError()
    override val type: TypeValue get() = IntegerTypeValue
    override val floatValue: Float get() = intValue.toFloat()

    override fun equals(other: Any?) = if (other is IntegerValue) intValue == other.intValue else false
    override fun hashCode(): Int = intValue.hashCode()
}
class FloatValue(override val floatValue: Float): NumberValue() {
    override val integer: IntegerValue get() = IntegerValue(floatValue.toInt())
    override val float: FloatValue get() = this
    override val identity: IdentityValue get() = throw CastError()
    override val string: StringValue get() = StringValue(floatValue.toString())
    override val boolean: BooleanValue get() = BooleanValue(floatValue != 0.0F)
    override val invokable: InvokableValue get() = throw CastError()
    override val function: FunctionValue get() = throw CastError()
    override val collection: CollectionValue get() = throw CastError()
    override val set: SetValue get() = throw CastError()
    override val tuple: TupleValue get() = throw CastError()
    override val mapping: MappingValue get() = throw CastError()
    override val type: TypeValue get() = FloatTypeValue

    override fun equals(other: Any?) = if (other is FloatValue) floatValue == other.floatValue else false
    override fun hashCode() = floatValue.hashCode()
}
class IdentityValue: ImmutableValue() {
    companion object {
        private var identityCounter: Int = 0
    }
    val value = identityCounter++
    override val number: NumberValue get() = integer
    override val integer: IntegerValue get() = IntegerValue(value)
    override val float: FloatValue get() = FloatValue(value.toFloat())
    override val identity: IdentityValue get() = this
    override val string: StringValue get() = StringValue("@$value")
    override val boolean: BooleanValue get() = BooleanValue(true)
    override val invokable: InvokableValue get() = throw CastError()
    override val function: FunctionValue get() = throw CastError()
    override val collection: CollectionValue get() = throw CastError()
    override val set: SetValue get() = throw CastError()
    override val tuple: TupleValue get() = throw CastError()
    override val mapping: MappingValue get() = throw CastError()
    override val type: TypeValue get() = IdentityTypeValue

    override fun equals(other: Any?) = if (other is IdentityValue) value == other.value else false
    override fun hashCode() = value.hashCode()
}
class StringValue(val value: String): ImmutableValue() {
    override val number: NumberValue get() = throw CastError()
    override val integer: IntegerValue get() = throw CastError()
    override val float: FloatValue get() = throw CastError()
    override val identity: IdentityValue get() = throw CastError()
    override val string: StringValue get() = this
    override val boolean: BooleanValue get() = BooleanValue(true)
    override val invokable: InvokableValue get() = throw CastError()
    override val function: FunctionValue get() = throw CastError()
    override val collection: CollectionValue get() = throw CastError()
    override val set: SetValue get() = throw CastError()
    override val tuple: TupleValue get() = throw CastError()
    override val mapping: MappingValue get() = throw CastError()
    override val type: TypeValue get() = StringTypeValue

    override fun equals(other: Any?) = if (other is StringValue) value == other.value else false
    override fun hashCode() = value.hashCode()
}
class BooleanValue(val value: Boolean): ImmutableValue() {
    override val number: NumberValue get() = integer
    override val integer: IntegerValue get() = IntegerValue(if (value) 1 else 0)
    override val float: FloatValue get() = FloatValue(if (value) 1.0F else 0.0F)
    override val identity: IdentityValue get() = throw CastError()
    override val string: StringValue get() = StringValue(value.toString())
    override val boolean: BooleanValue get() = this
    override val invokable: InvokableValue get() = throw CastError()
    override val function: FunctionValue get() = throw CastError()
    override val collection: CollectionValue get() = throw CastError()
    override val set: SetValue get() = throw CastError()
    override val tuple: TupleValue get() = throw CastError()
    override val mapping: MappingValue get() = throw CastError()
    override val type: TypeValue get() = BooleanTypeValue

    override fun equals(other: Any?) = if (other is BooleanValue) value == other.value else false
    override fun hashCode() = value.hashCode()
}

abstract class InvokableValue : ImmutableValue() {
    override val invokable: InvokableValue get() = this

    abstract fun invoke(globalExecution: GlobalExecution, params: Array<Value>)
    abstract fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value
}
class FunctionValue(val value: Invokable): InvokableValue() {
    override val number: NumberValue get() = throw CastError()
    override val integer: IntegerValue get() = throw CastError()
    override val float: FloatValue get() = throw CastError()
    override val identity: IdentityValue get() = throw CastError()
    override val string: StringValue get() = StringValue("")
    override val boolean: BooleanValue get() = BooleanValue(true)
    override val function: FunctionValue get() = this
    override val collection: CollectionValue get() = throw CastError()
    override val set: SetValue get() = throw CastError()
    override val tuple: TupleValue get() = throw CastError()
    override val mapping: MappingValue get() = throw CastError()
    override val type: TypeValue get() = FunctionTypeValue

    override fun invoke(globalExecution: GlobalExecution, params: Array<Value>) = value.invoke(globalExecution, params)
    override fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value = value.invokeReturn(globalExecution, params)

    override fun equals(other: Any?) = if (other is FunctionValue) value == other.value else false
    override fun hashCode() = value.hashCode()
}

abstract class CollectionValue : InvokableValue() {
    override val collection: CollectionValue get() = this

    abstract fun contains(value: Value): Boolean
}
class SetValue(val values: MutableSet<Value>): CollectionValue() {
    override val number: NumberValue get() = throw CastError()
    override val integer: IntegerValue get() = throw CastError()
    override val float: FloatValue get() = throw CastError()
    override val identity: IdentityValue get() = throw CastError()
    override val string: StringValue get() = StringValue("${values.joinToString(", ")}")
    override val boolean: BooleanValue get() = BooleanValue(true)
    override val function: FunctionValue get() = throw CastError()
    override val set: SetValue get() = this
    override val tuple: TupleValue get() = throw CastError()
    override val mapping: MappingValue get() = throw CastError()
    override val type: TypeValue get() = SetTypeValue

    override fun contains(value: Value): Boolean {
        return values.contains(value)
    }

    override fun invoke(globalExecution: GlobalExecution, params: Array<Value>) = throw InvokeError("Cannot invoke set")
    override fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value = throw InvokeError("Cannot invoke set")

    override fun equals(other: Any?) = if (other is SetValue) values.size == other.values.size && values.containsAll(other.values) else false
    override fun hashCode() = values.hashCode()
}
class TupleValue(var values: Array<Value>): CollectionValue() {
    override val number: NumberValue get() = throw CastError()
    override val integer: IntegerValue get() = throw CastError()
    override val float: FloatValue get() = throw CastError()
    override val identity: IdentityValue get() = throw CastError()
    override val string: StringValue get() = StringValue(values.joinToString(",", "(", ")"))
    override val boolean: BooleanValue get() = BooleanValue(true)
    override val function: FunctionValue get() = throw CastError()
    override val set: SetValue get() = throw CastError()
    override val tuple: TupleValue get() = this
    override val mapping: MappingValue get() = throw CastError()
    override val type: TypeValue get() = TupleTypeValue

    override fun contains(value: Value): Boolean {
        return values.contains(value)
    }

    override fun invoke(globalExecution: GlobalExecution, params: Array<Value>) {}
    override fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value {
        val indexValue = params.firstOrNull()
        if (indexValue != null) {
            val index = indexValue.integer.intValue
            if (index in values.indices) {
                return values[index]
            }
        }
        return UndefinedTypeValue
    }

    override fun equals(other: Any?): Boolean {
        return if (other is TupleValue && values.size == other.values.size) {
            return values.indices.all {
                values[it] == other.values[it]
            }
        } else {
            false
        }
    }
    override fun hashCode() = values.hashCode()
}
class MappingValue(var values: MutableMap<ImmutableValue, ImmutableValue>): CollectionValue() {
    override val number: NumberValue get() = throw CastError()
    override val integer: IntegerValue get() = throw CastError()
    override val float: FloatValue get() = throw CastError()
    override val identity: IdentityValue get() = throw CastError()
    override val string: StringValue get() = StringValue(values.map { "${it.key}: ${it.value}" }.joinToString(",", "[", "]"))
    override val boolean: BooleanValue get() = BooleanValue(true)
    override val function: FunctionValue get() = throw CastError()
    override val set: SetValue get() = throw CastError()
    override val tuple: TupleValue get() = throw CastError()
    override val mapping: MappingValue get() = this
    override val type: TypeValue get() = MappingTypeValue

    override fun contains(value: Value): Boolean {
        if (value is TupleValue) {
            if (value.values.size == 2) {
                return values[value.values[0]] == value.values[1]
            }
        }
        return false
    }

    override fun invoke(globalExecution: GlobalExecution, params: Array<Value>) {}
    override fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value {
        val key = params.firstOrNull()?.immutable
        if (key != null) {
            return object : DelegateMutableValue() {
                override var value: ImmutableValue
                    get() = values[key] ?: UndefinedTypeValue
                    set(value) { values[key] = value }
            }
        }
        return UndefinedTypeValue
    }

    override fun equals(other: Any?): Boolean {
        return if (other is MappingValue && values.size == other.values.size) {
            return values.keys.all {
                values[it] == other.values[it]
            }
        } else {
            false
        }
    }
    override fun hashCode() = values.hashCode()
}
