package com.jamieswhiteshirt.discrete.common.invokable

import com.jamieswhiteshirt.discrete.common.value.UndefinedTypeValue
import com.jamieswhiteshirt.discrete.common.value.Value
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution

class LambdaInvokable : Invokable {
    val lambda: (Array<Value>) -> Unit

    constructor(lambda: (Array<Value>) -> Unit) {
        this.lambda = lambda
    }

    override fun invoke(globalExecution: GlobalExecution, params: Array<Value>) {
        lambda.invoke(params)
    }

    override fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value {
        lambda.invoke(params)
        return UndefinedTypeValue
    }
}
