package com.jamieswhiteshirt.discrete.common.invokable

import com.jamieswhiteshirt.discrete.common.value.Value
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution

interface Invokable {
    fun invoke(globalExecution: GlobalExecution) {
        invoke(globalExecution, arrayOf())
    }

    fun invoke(globalExecution: GlobalExecution, params: Array<Value>)

    fun invokeReturn(globalExecution: GlobalExecution): Value {
        return invokeReturn(globalExecution, arrayOf())
    }

    fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value
}
