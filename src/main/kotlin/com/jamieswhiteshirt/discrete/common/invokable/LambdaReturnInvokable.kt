package com.jamieswhiteshirt.discrete.common.invokable

import com.jamieswhiteshirt.discrete.common.value.Value
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution

class LambdaReturnInvokable : Invokable {
    val lambda: (Array<Value>) -> Value

    constructor(lambda: (Array<Value>) -> Value) {
        this.lambda = lambda
    }

    override fun invoke(globalExecution: GlobalExecution, params: Array<Value>) {
        lambda.invoke(params)
    }

    override fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value {
        return lambda.invoke(params)
    }
}
