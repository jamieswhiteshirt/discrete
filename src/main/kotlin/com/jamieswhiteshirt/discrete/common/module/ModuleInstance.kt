package com.jamieswhiteshirt.discrete.common.module

import com.jamieswhiteshirt.discrete.common.value.Value
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution

interface ModuleInstance {
    val moduleName: String

    operator fun get(identifier: String): Value

    fun execute(globalExecution: GlobalExecution)
}
