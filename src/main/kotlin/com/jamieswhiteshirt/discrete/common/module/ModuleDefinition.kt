package com.jamieswhiteshirt.discrete.common.module

interface ModuleDefinition {
    val identifiers: Set<String>
    val moduleName: String
}
