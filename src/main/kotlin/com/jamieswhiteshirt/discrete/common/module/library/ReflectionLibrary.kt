package com.jamieswhiteshirt.discrete.common.module.library

import com.jamieswhiteshirt.discrete.common.module.ModuleDefinition
import com.jamieswhiteshirt.discrete.common.module.ModuleInstance
import com.jamieswhiteshirt.discrete.common.value.*
import com.jamieswhiteshirt.util.hasAnnotation
import java.util.*
import kotlin.reflect.*
import kotlin.reflect.jvm.javaType

abstract class ReflectionLibrary : ModuleDefinition, ModuleInstance {
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LibraryField

    private class ImmutablePropertyValue(val library: ReflectionLibrary, val property: KProperty1<ReflectionLibrary, ImmutableValue>) : DelegateImmutableValue() {
        override val value: ImmutableValue
            get() = property.get(library)
    }

    private class MutablePropertyValue(val library: ReflectionLibrary, val property: KMutableProperty1<ReflectionLibrary, ImmutableValue>) : DelegateMutableValue() {
        override var value: ImmutableValue
            get() = property.get(library)
            set(value) = property.set(library, value)
    }

    private val values = HashMap<String, Value>()
    override val identifiers: Set<String>

    init {
        for (property in this.javaClass.kotlin.memberProperties) {
            if (property.hasAnnotation(LibraryField::class)) {
                val fieldType = property.returnType
                val fieldName = property.name

                if (!ImmutableValue::class.java.isAssignableFrom(fieldType.javaType as Class<*>)) {
                    throw Exception("ModuleInstance field type must be assignable to ImmutableValue")
                }

                if(property is KProperty1<ReflectionLibrary, *>) {
                    values[fieldName] = ImmutablePropertyValue(this, @Suppress("UNCHECKED_CAST")(property as KProperty1<ReflectionLibrary, ImmutableValue>))
                }
                else if (property is KMutableProperty1<ReflectionLibrary, *>) {
                    if (fieldType.javaType != ImmutableValue::class.java) {
                        throw Exception("ModuleInstance var field type must be ImmutableValue")
                    }

                    @Suppress("UNCHECKED_CAST")
                    values[fieldName] = MutablePropertyValue(this, @Suppress("UNCHECKED_CAST")(property as KMutableProperty1<ReflectionLibrary, ImmutableValue>))
                }
            }
        }

        identifiers = values.keys
    }

    override operator fun get(identifier: String): Value {
        return values[identifier] ?: throw Exception("Unknown identifier")
    }
}
