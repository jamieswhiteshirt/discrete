package com.jamieswhiteshirt.discrete.common.instruction

import com.jamieswhiteshirt.discrete.common.value.*
import com.jamieswhiteshirt.discrete.runtime.execution.BlockLocalExecution
import com.jamieswhiteshirt.discrete.runtime.FunctionInstance
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution
import com.jamieswhiteshirt.discrete.runtime.execution.LocalExecution
import com.jamieswhiteshirt.util.floor
import java.util.*

sealed class Instruction {
    abstract val pops: Int
    abstract val pushes: Int

    abstract fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution)

    object DUP : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 2
        override fun toString(): String = "DUP"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(localExecution.peek())
        }
    }
    object POP : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 0
        override fun toString(): String = "POP"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.pop()
        }
    }
    object SWAP : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 2
        override fun toString(): String = "SWAP"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueA = localExecution.pop()
            val valueB = localExecution.pop()
            localExecution.push(valueA)
            localExecution.push(valueB)
        }
    }
    object RETURN : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 0
        override fun toString(): String = "RETURN"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution._return()
        }
    }
    object RETURN_VALUE : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 0
        override fun toString(): String = "RETURN_VALUE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution._return(localExecution.pop())
        }
    }
    object NEW_IDENTITY : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "NEW_IDENTITY"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(IdentityValue())
        }
    }
    object PUSH_TRUE : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TRUE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanValue(true))
        }
    }
    object PUSH_FALSE : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_FALSE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanValue(false))
        }
    }
    object PUSH_TYPE_NUMBER : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_NUMBER"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(NumberTypeValue)
        }
    }
    object PUSH_TYPE_INTEGER : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_INTEGER"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(IntegerTypeValue)
        }
    }
    object PUSH_TYPE_FLOAT : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_FLOAT"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(FloatTypeValue)
        }
    }
    object PUSH_TYPE_IDENTITY : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_IDENTITY"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(IdentityTypeValue)
        }
    }
    object PUSH_TYPE_STRING : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_STRING"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(StringTypeValue)
        }
    }
    object PUSH_TYPE_BOOLEAN : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_BOOLEAN"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanTypeValue)
        }
    }
    object PUSH_TYPE_INVOKABLE : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_INVOKABLE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(InvokableTypeValue)
        }
    }
    object PUSH_TYPE_FUNCTION : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_FUNCTION"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(FunctionTypeValue)
        }
    }
    object PUSH_TYPE_COLLECTION : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_COLLECTION"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(CollectionTypeValue)
        }
    }
    object PUSH_TYPE_SET : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_SET"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(SetTypeValue)
        }
    }
    object PUSH_TYPE_TUPLE : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_TUPLE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(TupleTypeValue)
        }
    }
    object PUSH_TYPE_MAPPING : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_MAPPING"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(MappingTypeValue)
        }
    }
    object PUSH_TYPE_TYPE : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_TYPE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(TypeTypeValue)
        }
    }
    object PUSH_TYPE_UNDEFINED : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_TYPE_UNDEFINED"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(UndefinedTypeValue)
        }
    }
    object ASSIGN : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 0
        override fun toString(): String = "ASSIGN"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop()
            val valueA = localExecution.pop().mutable
            valueA.assign(valueB.immutable)
        }
    }
    object BITWISE_OR : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "BITWISE_OR"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(IntegerValue(localExecution.pop().integer.intValue or localExecution.pop().integer.intValue))
        }
    }
    object BITWISE_AND : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "BITWISE_AND"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(IntegerValue(localExecution.pop().integer.intValue and localExecution.pop().integer.intValue))
        }
    }
    object BITWISE_XOR : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "BITWISE_XOR"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(IntegerValue(localExecution.pop().integer.intValue xor localExecution.pop().integer.intValue))
        }
    }
    object BITWISE_NOT : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "BITWISE_NOT"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(IntegerValue(localExecution.pop().integer.intValue.inv()))
        }
    }
    object LOGICAL_OR : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "LOGICAL_OR"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanValue(localExecution.pop().boolean.value or localExecution.pop().boolean.value))
        }
    }
    object LOGICAL_AND : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "LOGICAL_AND"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanValue(localExecution.pop().boolean.value and localExecution.pop().boolean.value))
        }
    }
    object LOGICAL_XOR : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "LOGICAL_XOR"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanValue(localExecution.pop().boolean.value xor localExecution.pop().boolean.value))
        }
    }
    object LOGICAL_NOT : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "LOGICAL_NOT"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanValue(localExecution.pop().boolean.value.not()))
        }
    }
    object LEFT_SHIFT : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "LEFT_SHIFT"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop()
            val valueA = localExecution.pop()
            localExecution.push(IntegerValue(valueA.integer.intValue shl valueB.integer.intValue))
        }
    }
    object RIGHT_SHIFT : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "RIGHT_SHIFT"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop()
            val valueA = localExecution.pop()
            localExecution.push(IntegerValue(valueA.integer.intValue shr valueB.integer.intValue))
        }
    }
    object ADD : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "ADD"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            if (valueA is IntegerValue && valueB is IntegerValue) {
                localExecution.push(IntegerValue(valueA.intValue + valueB.intValue))
            }
            else {
                localExecution.push(FloatValue(valueA.floatValue + valueB.floatValue))
            }
        }
    }
    object SUBTRACT : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "SUBTRACT"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            if (valueA is IntegerValue && valueB is IntegerValue) {
                localExecution.push(IntegerValue(valueA.intValue - valueB.intValue))
            }
            else {
                localExecution.push(FloatValue(valueA.floatValue - valueB.floatValue))
            }
        }
    }
    object MULTIPLY : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "MULTIPLY"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            if (valueA is IntegerValue && valueB is IntegerValue) {
                localExecution.push(IntegerValue(valueA.intValue * valueB.intValue))
            }
            else {
                localExecution.push(FloatValue(valueA.floatValue * valueB.floatValue))
            }
        }
    }
    object DIVIDE : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "DIVIDE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            localExecution.push(FloatValue(valueA.floatValue / valueB.floatValue))
        }
    }
    object DIVIDE_FLOOR : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "DIVIDE_FLOOR"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            if (valueA is IntegerValue && valueB is IntegerValue) {
                localExecution.push(IntegerValue(valueA.intValue / valueB.intValue))
            }
            else {
                localExecution.push(IntegerValue((valueA.floatValue / valueB.floatValue).floor()))
            }
        }
    }
    object REMAINDER : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "REMAINDER"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            if (valueA is IntegerValue && valueB is IntegerValue) {
                localExecution.push(IntegerValue(valueA.intValue % valueB.intValue))
            }
            else {
                localExecution.push(FloatValue(valueA.floatValue % valueB.floatValue))
            }
        }
    }
    object CONCATENATE : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "CONCATENATE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop()
            val valueA = localExecution.pop()
            localExecution.push(StringValue(valueA.toString() + valueB.toString()))
        }
    }
    object IS : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "IS"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanValue(localExecution.pop().type.isTypeOf(localExecution.pop().immutable)))
        }
    }
    object IS_NOT : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "IS_NOT"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().type
            val valueA = localExecution.pop().immutable
            localExecution.push(BooleanValue(valueB.isTypeOf(valueA).not()))
        }
    }
    object IN : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "IN"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop()
            val valueA = localExecution.pop().collection
            localExecution.push(BooleanValue(valueA.contains(valueB)))
        }
    }
    object NOT_IN : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "NOT_IN"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop()
            val valueA = localExecution.pop().collection
            localExecution.push(BooleanValue(valueA.contains(valueB).not()))
        }
    }
    object COMPARE_EQUAL : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "COMPARE_EQUAL"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanValue(localExecution.pop() == localExecution.pop()))
        }
    }
    object COMPARE_NOT_EQUAL : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "COMPARE_NOT_EQUAL"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(BooleanValue(localExecution.pop() != localExecution.pop()))
        }
    }
    object COMPARE_LESS_OR_EQUAL : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "COMPARE_LESS_OR_EQUAL"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            if (valueA is IntegerValue && valueB is IntegerValue) {
                localExecution.push(BooleanValue(valueA.intValue <= valueB.intValue))
            }
            else {
                localExecution.push(BooleanValue(valueA.floatValue <= valueB.floatValue))
            }
        }
    }
    object COMPARE_GREATER_OR_EQUAL : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "COMPARE_GREATER_OR_EQUAL"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            if (valueA is IntegerValue && valueB is IntegerValue) {
                localExecution.push(BooleanValue(valueA.intValue >= valueB.intValue))
            }
            else {
                localExecution.push(BooleanValue(valueA.floatValue >= valueB.floatValue))
            }
        }
    }
    object COMPARE_LESS : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "COMPARE_LESS"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            if (valueA is IntegerValue && valueB is IntegerValue) {
                localExecution.push(BooleanValue(valueA.intValue < valueB.intValue))
            }
            else {
                localExecution.push(BooleanValue(valueA.floatValue < valueB.floatValue))
            }
        }
    }
    object COMPARE_GREATER : Instruction() {
        override val pops: Int get() = 2
        override val pushes: Int get() = 1
        override fun toString(): String = "COMPARE_GREATER"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val valueB = localExecution.pop().number
            val valueA = localExecution.pop().number
            if (valueA is IntegerValue && valueB is IntegerValue) {
                localExecution.push(BooleanValue(valueA.intValue > valueB.intValue))
            }
            else {
                localExecution.push(BooleanValue(valueA.floatValue > valueB.floatValue))
            }
        }
    }
    object NEGATE : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 1
        override fun toString(): String = "NEGATE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val value = localExecution.pop().number
            if (value is IntegerValue) {
                localExecution.push(IntegerValue(-value.intValue))
            }
            else {
                localExecution.push(FloatValue(-value.floatValue))
            }
        }
    }
    object INCREMENT : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 0
        override fun toString(): String = "INCREMENT"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val value = localExecution.pop().mutable
            val numberValue = value.number
            if (numberValue is IntegerValue) {
                value.assign(IntegerValue(numberValue.intValue + 1))
            }
            else {
                value.assign(FloatValue(numberValue.floatValue + 1))
            }
        }
    }
    object DECREMENT : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 0
        override fun toString(): String = "DECREMENT"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val value = localExecution.pop().mutable
            val numberValue = value.number
            if (numberValue is IntegerValue) {
                value.assign(IntegerValue(numberValue.intValue - 1))
            }
            else {
                value.assign(FloatValue(numberValue.floatValue - 1))
            }
        }
    }
    object AS_IMMUTABLE : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 1
        override fun toString(): String = "AS_IMMUTABLE"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(localExecution.pop().immutable)
        }
    }
    object AS_BOOLEAN : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 1
        override fun toString(): String = "AS_BOOLEAN"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(localExecution.pop().boolean)
        }
    }
    class DECLARE_LOCAL_VARIABLE(val level: Int, val variable: Int) : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 0
        override fun toString(): String = "DECLARE_LOCAL_VARIABLE(level=$level, variable=$variable)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.localVariables[level][variable] = localExecution.scope.localVariables[variable].instantiate(localExecution.pop().immutable)
        }
    }
    class DECLARE_MODULE_VARIABLE(val module: String, val identifier: String) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 0
        override fun toString(): String = "DECLARE_MODULE_VARIABLE(module=$module, identifier=$identifier)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }
    class GET_LOCAL_VARIABLE(val level: Int, val variable: Int) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "GET_LOCAL_VARIABLE(level=$level, variable=$variable)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(localExecution.localVariables[level][variable])
        }
    }
    class GET_MODULE_VARIABLE(val module: String, val identifier: String) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "GET_MODULE_VARIABLE(module=$module, identifier=$identifier)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(globalExecution[module][identifier])
        }
    }
    class EXECUTE_BLOCK(val block: Int) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 0
        override fun toString(): String = "EXECUTE_BLOCK(block=$block)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val block = localExecution.scope.blocks[block]
            val instance = BlockLocalExecution(block, localExecution)
            instance.execute(globalExecution)
        }
    }
    class JUMP(val label: Int) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 0
        override fun toString(): String = "JUMP(label=$label)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.jump(label)
        }
    }
    class JUMP_IF_FALSE(val label: Int) : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 0
        override fun toString(): String = "JUMP_IF_FALSE(label=$label)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            if (localExecution.pop().boolean.value.not()) {
                localExecution.jump(label)
            }
        }
    }
    class JUMP_IF_TRUE(val label: Int) : Instruction() {
        override val pops: Int get() = 1
        override val pushes: Int get() = 0
        override fun toString(): String = "JUMP_IF_TRUE(label=$label)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            if (localExecution.pop().boolean.value) {
                localExecution.jump(label)
            }
        }
    }
    class NEW_FUNCTION(val function: Int) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "NEW_FUNCTION(function=$function)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val definition = localExecution.scope.functions[function]
            val instance = FunctionInstance(definition, localExecution)
            localExecution.push(FunctionValue(instance))
        }
    }
    class NEW_EXPLICIT_MAPPING(val size: Int) : Instruction() {
        override val pops: Int get() = size * 2
        override val pushes: Int get() = 1
        override fun toString(): String = "NEW_EXPLICIT_MAPPING(size=$size)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val values = Array(size * 2, { localExecution.pop() }).reversed()
            val map = HashMap<ImmutableValue, ImmutableValue>()
            for (i in 0..size - 1) {
                map[values[i * 2].immutable] = values[i * 2 + 1].immutable
            }
            localExecution.push(MappingValue(map))
        }
    }
    class NEW_IMPLICIT_MAPPING(val size: Int) : Instruction() {
        override val pops: Int get() = size
        override val pushes: Int get() = 1
        override fun toString(): String = "NEW_IMPLICIT_MAPPING(size=$size)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val values = Array(size, { localExecution.pop() }).reversed()
            val map = HashMap<ImmutableValue, ImmutableValue>()
            for (i in values.indices) {
                map[IntegerValue(i)] = values[i].immutable
            }
            localExecution.push(MappingValue(map))
        }
    }
    class NEW_SET(val size: Int) : Instruction() {
        override val pops: Int get() = size
        override val pushes: Int get() = 1
        override fun toString(): String = "NEW_SET(size=$size)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val values = Array(size, { localExecution.pop() }).reversed()
            val set = HashSet<Value>(values)
            localExecution.push(SetValue(set))
        }
    }
    class NEW_TUPLE(val size: Int) : Instruction() {
        override val pops: Int get() = size
        override val pushes: Int get() = 1
        override fun toString(): String = "NEW_TUPLE(size=$size)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val values = Array(size, { localExecution.pop() })
            values.reverse()
            localExecution.push(TupleValue(values))
        }
    }
    class PUSH_FLOAT(val value: Float) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_FLOAT(value=$value)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(FloatValue(value))
        }
    }
    class PUSH_INT(val value: Int) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_INT(value=$value)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(IntegerValue(value))
        }
    }
    class PUSH_STRING(val value: String) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "PUSH_STRING(value=$value)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(StringValue(value))
        }
    }
    class INVOKE(val paramCount: Int) : Instruction() {
        override val pops: Int get() = paramCount
        override val pushes: Int get() = 0
        override fun toString(): String = "INVOKE(paramCount=$paramCount)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val params = Array(paramCount, { localExecution.pop() })
            params.reverse()
            localExecution.pop().invokable.invoke(globalExecution, params)
        }
    }
    class INVOKE_RETURN(val paramCount: Int) : Instruction() {
        override val pops: Int get() = paramCount
        override val pushes: Int get() = 1
        override fun toString(): String = "INVOKE_RETURN(paramCount=$paramCount)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            val params = Array(paramCount, { localExecution.pop() })
            params.reverse()
            localExecution.push(localExecution.pop().invokable.invokeReturn(globalExecution, params))
        }
    }
    class GET_LOCAL_VARIABLE_TUPLE(val level: Int, val length: Int) : Instruction() {
        override val pops: Int get() = 0
        override val pushes: Int get() = 1
        override fun toString(): String = "GET_LOCAL_VARIABLE_TUPLE(level=$level, length=$length)"
        override fun execute(globalExecution: GlobalExecution, localExecution: LocalExecution) {
            localExecution.push(TupleValue(localExecution.localVariables[level].sliceArray(0..length - 1)))
        }
    }
}
