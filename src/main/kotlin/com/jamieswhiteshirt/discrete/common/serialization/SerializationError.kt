package com.jamieswhiteshirt.discrete.common.serialization

abstract  class SerializationError : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
}

open class SerializeError : SerializationError {
    constructor() : super()
    constructor(message: String) : super(message)
}

open class DeserializeError : SerializationError {
    constructor() : super()
    constructor(message: String) : super(message)
}
