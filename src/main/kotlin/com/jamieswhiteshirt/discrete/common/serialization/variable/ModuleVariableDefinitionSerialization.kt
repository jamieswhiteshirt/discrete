package com.jamieswhiteshirt.discrete.common.serialization.variable

import com.jamieswhiteshirt.discrete.common.code.ModuleVariableDefinition
import com.jamieswhiteshirt.discrete.common.serialization.BinarySerialization
import java.io.DataInputStream
import java.io.DataOutputStream

object ModuleVariableDefinitionSerialization : BinarySerialization<ModuleVariableDefinition>() {
    override fun serialize(outputStream: DataOutputStream, element: ModuleVariableDefinition) {
        outputStream.writeUTF(element.identifier)
        VariableMutabilitySerialization.serialize(outputStream, element.mutability)
    }

    override fun deserialize(inputStream: DataInputStream): ModuleVariableDefinition {
        return ModuleVariableDefinition(inputStream.readUTF(), VariableMutabilitySerialization.deserialize(inputStream))
    }
}
