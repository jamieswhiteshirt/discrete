package com.jamieswhiteshirt.discrete.common.serialization.scope

import com.jamieswhiteshirt.discrete.common.code.LocalVariableDefinition
import com.jamieswhiteshirt.discrete.common.code.ScopeDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.BlockScopeDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.FunctionScopeDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.common.serialization.BinarySerialization
import com.jamieswhiteshirt.discrete.common.serialization.variable.LocalVariableDefinitionSerialization
import com.jamieswhiteshirt.discrete.common.serialization.InstructionSerialization
import java.io.DataInputStream
import java.io.DataOutputStream
import java.util.*

abstract class ScopeSerialization<T : ScopeDefinition> : BinarySerialization<T>() {
    override fun serialize(outputStream: DataOutputStream, element: T) {
        BlockScopeSerialization.serializeAll(outputStream, element.blocks.asList())
        FunctionScopeSerialization.serializeAll(outputStream, element.functions.asList())
        outputStream.writeByte(element.labels.size)
        for (label in element.labels) {
            outputStream.writeInt(label)
        }
        InstructionSerialization.serializeAll(outputStream, element.instructions.asList())
        LocalVariableDefinitionSerialization.serializeAll(outputStream, element.localVariables.asList())
        internalSerialize(outputStream, element)
    }

    override fun deserialize(inputStream: DataInputStream): T {
        val blocks = BlockScopeSerialization.deserializeAll(inputStream).toTypedArray()
        val functions = FunctionScopeSerialization.deserializeAll(inputStream).toTypedArray()
        val labelsSize = inputStream.readUnsignedByte()
        val labels = Array(labelsSize, { inputStream.readInt() })
        val instructions = InstructionSerialization.deserializeAll(inputStream).toTypedArray()
        val variables = LocalVariableDefinitionSerialization.deserializeAll(inputStream).toTypedArray()
        return internalDeserialize(inputStream, blocks, functions, labels, instructions, variables)
    }

    protected abstract fun internalSerialize(outputStream: DataOutputStream, element: T)

    protected abstract fun internalDeserialize(inputStream: DataInputStream, blocks: Array<BlockScopeDefinition>, functions: Array<FunctionScopeDefinition>, labels: Array<Int>, instructions: Array<Instruction>, localVariables: Array<LocalVariableDefinition>): T
}
