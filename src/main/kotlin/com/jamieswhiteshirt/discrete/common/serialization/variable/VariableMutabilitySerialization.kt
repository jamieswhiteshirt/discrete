package com.jamieswhiteshirt.discrete.common.serialization.variable

import com.jamieswhiteshirt.discrete.common.code.VariableMutability
import com.jamieswhiteshirt.discrete.common.serialization.PolymorphicBinarySerialization

object VariableMutabilitySerialization : PolymorphicBinarySerialization<VariableMutability>() {
    init {
        addBinding(ObjectBinding(0, VariableMutability.Var))
        addBinding(ObjectBinding(1, VariableMutability.Val))
    }
}
