package com.jamieswhiteshirt.discrete.common.serialization.variable

import com.jamieswhiteshirt.discrete.common.code.LocalVariableDefinition
import com.jamieswhiteshirt.discrete.common.serialization.BinarySerialization
import java.io.DataInputStream
import java.io.DataOutputStream

object LocalVariableDefinitionSerialization : BinarySerialization<LocalVariableDefinition>() {
    override fun serialize(outputStream: DataOutputStream, element: LocalVariableDefinition) {
        VariableMutabilitySerialization.serialize(outputStream, element.mutability)
    }

    override fun deserialize(inputStream: DataInputStream): LocalVariableDefinition {
        return LocalVariableDefinition(VariableMutabilitySerialization.deserialize(inputStream))
    }
}
