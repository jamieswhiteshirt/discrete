package com.jamieswhiteshirt.discrete.common.serialization.scope

import com.jamieswhiteshirt.discrete.common.code.LocalVariableDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.BlockScopeDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.FunctionScopeDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import java.io.DataInputStream
import java.io.DataOutputStream

object BlockScopeSerialization : ScopeSerialization<BlockScopeDefinition>() {
    override fun internalSerialize(outputStream: DataOutputStream, element: BlockScopeDefinition) { }

    override fun internalDeserialize(inputStream: DataInputStream, blocks: Array<BlockScopeDefinition>, functions: Array<FunctionScopeDefinition>, labels: Array<Int>, instructions: Array<Instruction>, localVariables: Array<LocalVariableDefinition>): BlockScopeDefinition {
        return BlockScopeDefinition(blocks, functions, labels, instructions, localVariables)
    }
}
