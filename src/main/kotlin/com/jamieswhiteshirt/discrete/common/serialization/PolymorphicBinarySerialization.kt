package com.jamieswhiteshirt.discrete.common.serialization

import java.io.DataInputStream
import java.io.DataOutputStream
import java.util.*
import kotlin.reflect.KClass

abstract class PolymorphicBinarySerialization<T : Any> : BinarySerialization<T>() {
    interface PolymorphicBinding<T> {
        val id: Int
        val elementClass: Class<*>

        fun serialize(outputStream: DataOutputStream, element: T)
        fun deserialize(inputStream: DataInputStream): T
    }

    class ObjectBinding<T : Any>(override val id: Int, val obj: T) : PolymorphicBinding<T> {
        override val elementClass = obj.javaClass

        override fun serialize(outputStream: DataOutputStream, element: T) {}

        override fun deserialize(inputStream: DataInputStream): T {
            return obj
        }
    }

    abstract class ClassBinding<T : Any>(override val id: Int, elementKClass: KClass<T>) : PolymorphicBinding<T> {
        override val elementClass = elementKClass.java
    }

    private val bindingById: Array<PolymorphicBinding<T>?> = arrayOfNulls(256)
    private val bindingByClass: MutableMap<Class<*>, PolymorphicBinding<T>> = HashMap()

    protected fun addBinding(binding: PolymorphicBinding<T>) {
        this.bindingById[binding.id] = binding
        this.bindingByClass[binding.elementClass] = binding
    }

    override fun serialize(outputStream: DataOutputStream, element: T) {
        val instructionClass = element.javaClass
        val binding = bindingByClass[instructionClass]
        if (binding != null) {
            outputStream.writeByte(binding.id)
            binding.serialize(outputStream, element)
        }
        else {
            throw SerializeError("Invalid class $instructionClass for polymorphic serialization")
        }
    }

    override fun deserialize(inputStream: DataInputStream): T {
        val id: Int = inputStream.readUnsignedByte()
        val binding = bindingById[id]
        if (binding != null) {
            return binding.deserialize(inputStream)
        }
        else {
            throw DeserializeError("Invalid polymorphic deserialization id $id")
        }
    }
}