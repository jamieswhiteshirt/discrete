package com.jamieswhiteshirt.discrete.common.serialization

import java.io.DataInputStream
import java.io.DataOutputStream
import java.util.*

abstract class BinarySerialization<T : Any> {
    abstract fun serialize(outputStream: DataOutputStream, element: T)

    abstract fun deserialize(inputStream: DataInputStream): T

    fun serializeAll(outputStream: DataOutputStream, elements: List<T>) {
        outputStream.writeShort(elements.size)

        for (element in elements) {
            serialize(outputStream, element)
        }
    }

    fun deserializeAll(inputStream: DataInputStream): List<T> {
        val list = ArrayList<T>()
        val size = inputStream.readShort()

        for (i in 0..size - 1) {
            list.add(deserialize(inputStream))
        }

        return list
    }
}
