package com.jamieswhiteshirt.discrete.common.serialization.scope

import com.jamieswhiteshirt.discrete.common.code.LocalVariableDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.BlockScopeDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.FunctionScopeDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.ModuleScopeDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.common.serialization.variable.ModuleVariableDefinitionSerialization
import java.io.DataInputStream
import java.io.DataOutputStream

object ModuleScopeSerialization : ScopeSerialization<ModuleScopeDefinition>() {
    override fun internalSerialize(outputStream: DataOutputStream, element: ModuleScopeDefinition) {
        outputStream.writeUTF(element.moduleName)
        ModuleVariableDefinitionSerialization.serializeAll(outputStream, element.moduleVariables.asList())
    }

    override fun internalDeserialize(inputStream: DataInputStream, blocks: Array<BlockScopeDefinition>, functions: Array<FunctionScopeDefinition>, labels: Array<Int>, instructions: Array<Instruction>, localVariables: Array<LocalVariableDefinition>): ModuleScopeDefinition {
        val moduleVariables = ModuleVariableDefinitionSerialization.deserializeAll(inputStream).toTypedArray()
        return ModuleScopeDefinition(inputStream.readUTF(), blocks, functions, labels, instructions, localVariables, moduleVariables)
    }
}
