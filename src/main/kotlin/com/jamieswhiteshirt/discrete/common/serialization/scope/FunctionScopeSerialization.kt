package com.jamieswhiteshirt.discrete.common.serialization.scope

import com.jamieswhiteshirt.discrete.common.code.LocalVariableDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.BlockScopeDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.FunctionScopeDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import java.io.DataInputStream
import java.io.DataOutputStream

object FunctionScopeSerialization : ScopeSerialization<FunctionScopeDefinition>() {
    override fun internalSerialize(outputStream: DataOutputStream, element: FunctionScopeDefinition) {
        outputStream.writeByte(element.parameterCount)
    }

    override fun internalDeserialize(inputStream: DataInputStream, blocks: Array<BlockScopeDefinition>, functions: Array<FunctionScopeDefinition>, labels: Array<Int>, instructions: Array<Instruction>, localVariables: Array<LocalVariableDefinition>): FunctionScopeDefinition {
        val parameterCount = inputStream.readUnsignedByte()
        return FunctionScopeDefinition(blocks, functions, labels, instructions, localVariables, parameterCount)
    }
}
