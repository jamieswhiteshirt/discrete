package com.jamieswhiteshirt.discrete.common.serialization

import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import java.io.DataInputStream
import java.io.DataOutputStream
import com.jamieswhiteshirt.discrete.common.instruction.Instruction.*
import java.util.*
import kotlin.reflect.KClass

object InstructionSerialization : PolymorphicBinarySerialization<Instruction>() {
    init {
        val bindings = arrayOf(
                ObjectBinding(0, DUP),
                ObjectBinding(1, POP),
                ObjectBinding(2, SWAP),
                ObjectBinding(4, RETURN),
                ObjectBinding(5, RETURN_VALUE),
                ObjectBinding(10, NEW_IDENTITY),
                ObjectBinding(11, PUSH_TRUE),
                ObjectBinding(12, PUSH_FALSE),
                ObjectBinding(14, PUSH_TYPE_NUMBER),
                ObjectBinding(15, PUSH_TYPE_INTEGER),
                ObjectBinding(16, PUSH_TYPE_FLOAT),
                ObjectBinding(17, PUSH_TYPE_IDENTITY),
                ObjectBinding(18, PUSH_TYPE_STRING),
                ObjectBinding(19, PUSH_TYPE_BOOLEAN),
                ObjectBinding(58, PUSH_TYPE_INVOKABLE),
                ObjectBinding(20, PUSH_TYPE_FUNCTION),
                ObjectBinding(59, PUSH_TYPE_COLLECTION),
                ObjectBinding(21, PUSH_TYPE_SET),
                ObjectBinding(22, PUSH_TYPE_TUPLE),
                ObjectBinding(60, PUSH_TYPE_MAPPING),
                ObjectBinding(23, PUSH_TYPE_TYPE),
                ObjectBinding(24, PUSH_TYPE_UNDEFINED),
                ObjectBinding(25, ASSIGN),
                ObjectBinding(26, BITWISE_OR),
                ObjectBinding(27, BITWISE_AND),
                ObjectBinding(28, BITWISE_XOR),
                ObjectBinding(29, BITWISE_NOT),
                ObjectBinding(30, LOGICAL_OR),
                ObjectBinding(31, LOGICAL_AND),
                ObjectBinding(32, LOGICAL_XOR),
                ObjectBinding(33, LOGICAL_NOT),
                ObjectBinding(34, LEFT_SHIFT),
                ObjectBinding(35, RIGHT_SHIFT),
                ObjectBinding(36, ADD),
                ObjectBinding(37, SUBTRACT),
                ObjectBinding(38, MULTIPLY),
                ObjectBinding(39, DIVIDE),
                ObjectBinding(40, DIVIDE_FLOOR),
                ObjectBinding(41, REMAINDER),
                ObjectBinding(42, CONCATENATE),
                ObjectBinding(43, IS),
                ObjectBinding(44, IS_NOT),
                ObjectBinding(45, IN),
                ObjectBinding(46, NOT_IN),
                ObjectBinding(47, COMPARE_EQUAL),
                ObjectBinding(48, COMPARE_NOT_EQUAL),
                ObjectBinding(49, COMPARE_LESS_OR_EQUAL),
                ObjectBinding(50, COMPARE_GREATER_OR_EQUAL),
                ObjectBinding(51, COMPARE_LESS),
                ObjectBinding(52, COMPARE_GREATER),
                ObjectBinding(53, NEGATE),
                ObjectBinding(54, INCREMENT),
                ObjectBinding(55, DECREMENT),
                ObjectBinding(56, AS_IMMUTABLE),
                ObjectBinding(57, AS_BOOLEAN),
                object : ClassBinding<DECLARE_LOCAL_VARIABLE>(130, DECLARE_LOCAL_VARIABLE::class) {
                    override fun serialize(outputStream: DataOutputStream, element: DECLARE_LOCAL_VARIABLE) {
                        outputStream.writeByte(element.level)
                        outputStream.writeByte(element.variable)
                    }

                    override fun deserialize(inputStream: DataInputStream): DECLARE_LOCAL_VARIABLE {
                        return DECLARE_LOCAL_VARIABLE(inputStream.readUnsignedByte(), inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<DECLARE_MODULE_VARIABLE>(132, DECLARE_MODULE_VARIABLE::class) {
                    override fun serialize(outputStream: DataOutputStream, element: DECLARE_MODULE_VARIABLE) {
                        outputStream.writeUTF(element.module)
                        outputStream.writeUTF(element.identifier)
                    }

                    override fun deserialize(inputStream: DataInputStream): DECLARE_MODULE_VARIABLE {
                        return DECLARE_MODULE_VARIABLE(inputStream.readUTF(), inputStream.readUTF())
                    }
                },
                object : ClassBinding<GET_LOCAL_VARIABLE>(134, GET_LOCAL_VARIABLE::class) {
                    override fun serialize(outputStream: DataOutputStream, element: GET_LOCAL_VARIABLE) {
                        outputStream.writeByte(element.level)
                        outputStream.writeByte(element.variable)
                    }

                    override fun deserialize(inputStream: DataInputStream): GET_LOCAL_VARIABLE {
                        return GET_LOCAL_VARIABLE(inputStream.readUnsignedByte(), inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<GET_MODULE_VARIABLE>(136, GET_MODULE_VARIABLE::class) {
                    override fun serialize(outputStream: DataOutputStream, element: GET_MODULE_VARIABLE) {
                        outputStream.writeUTF(element.module)
                        outputStream.writeUTF(element.identifier)
                    }

                    override fun deserialize(inputStream: DataInputStream): GET_MODULE_VARIABLE {
                        return GET_MODULE_VARIABLE(inputStream.readUTF(), inputStream.readUTF())
                    }
                },
                object : ClassBinding<EXECUTE_BLOCK>(137, EXECUTE_BLOCK::class) {
                    override fun serialize(outputStream: DataOutputStream, element: EXECUTE_BLOCK) {
                        outputStream.writeByte(element.block)
                    }

                    override fun deserialize(inputStream: DataInputStream): EXECUTE_BLOCK {
                        return EXECUTE_BLOCK(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<JUMP>(140, JUMP::class) {
                    override fun serialize(outputStream: DataOutputStream, element: JUMP) {
                        outputStream.writeByte(element.label)
                    }

                    override fun deserialize(inputStream: DataInputStream): JUMP {
                        return JUMP(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<JUMP_IF_FALSE>(141, JUMP_IF_FALSE::class) {
                    override fun serialize(outputStream: DataOutputStream, element: JUMP_IF_FALSE) {
                        outputStream.writeByte(element.label)
                    }

                    override fun deserialize(inputStream: DataInputStream): JUMP_IF_FALSE {
                        return JUMP_IF_FALSE(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<JUMP_IF_TRUE>(142, JUMP_IF_TRUE::class) {
                    override fun serialize(outputStream: DataOutputStream, element: JUMP_IF_TRUE) {
                        outputStream.writeByte(element.label)
                    }

                    override fun deserialize(inputStream: DataInputStream): JUMP_IF_TRUE {
                        return JUMP_IF_TRUE(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<NEW_FUNCTION>(143, NEW_FUNCTION::class) {
                    override fun serialize(outputStream: DataOutputStream, element: NEW_FUNCTION) {
                        outputStream.writeByte(element.function)
                    }

                    override fun deserialize(inputStream: DataInputStream): NEW_FUNCTION {
                        return NEW_FUNCTION(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<NEW_EXPLICIT_MAPPING>(144, NEW_EXPLICIT_MAPPING::class) {
                    override fun serialize(outputStream: DataOutputStream, element: NEW_EXPLICIT_MAPPING) {
                        outputStream.writeByte(element.size)
                    }

                    override fun deserialize(inputStream: DataInputStream): NEW_EXPLICIT_MAPPING {
                        return NEW_EXPLICIT_MAPPING(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<NEW_IMPLICIT_MAPPING>(145, NEW_IMPLICIT_MAPPING::class) {
                    override fun serialize(outputStream: DataOutputStream, element: NEW_IMPLICIT_MAPPING) {
                        outputStream.writeByte(element.size)
                    }

                    override fun deserialize(inputStream: DataInputStream): NEW_IMPLICIT_MAPPING {
                        return NEW_IMPLICIT_MAPPING(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<NEW_SET>(146, NEW_SET::class) {
                    override fun serialize(outputStream: DataOutputStream, element: NEW_SET) {
                        outputStream.writeByte(element.size)
                    }

                    override fun deserialize(inputStream: DataInputStream): NEW_SET {
                        return NEW_SET(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<NEW_TUPLE>(147, NEW_TUPLE::class) {
                    override fun serialize(outputStream: DataOutputStream, element: NEW_TUPLE) {
                        outputStream.writeByte(element.size)
                    }

                    override fun deserialize(inputStream: DataInputStream): NEW_TUPLE {
                        return NEW_TUPLE(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<PUSH_FLOAT>(148, PUSH_FLOAT::class) {
                    override fun serialize(outputStream: DataOutputStream, element: PUSH_FLOAT) {
                        outputStream.writeFloat(element.value)
                    }

                    override fun deserialize(inputStream: DataInputStream): PUSH_FLOAT {
                        return PUSH_FLOAT(inputStream.readFloat())
                    }
                },
                object : ClassBinding<PUSH_INT>(149, PUSH_INT::class) {
                    override fun serialize(outputStream: DataOutputStream, element: PUSH_INT) {
                        outputStream.writeInt(element.value)
                    }

                    override fun deserialize(inputStream: DataInputStream): PUSH_INT {
                        return PUSH_INT(inputStream.readInt())
                    }
                },
                object : ClassBinding<PUSH_STRING>(150, PUSH_STRING::class) {
                    override fun serialize(outputStream: DataOutputStream, element: PUSH_STRING) {
                        outputStream.writeUTF(element.value)
                    }

                    override fun deserialize(inputStream: DataInputStream): PUSH_STRING {
                        return PUSH_STRING(inputStream.readUTF())
                    }
                },
                object : ClassBinding<INVOKE>(151, INVOKE::class) {
                    override fun serialize(outputStream: DataOutputStream, element: INVOKE) {
                        outputStream.writeByte(element.paramCount)
                    }

                    override fun deserialize(inputStream: DataInputStream): INVOKE {
                        return INVOKE(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<INVOKE_RETURN>(152, INVOKE_RETURN::class) {
                    override fun serialize(outputStream: DataOutputStream, element: INVOKE_RETURN) {
                        outputStream.writeByte(element.paramCount)
                    }

                    override fun deserialize(inputStream: DataInputStream): INVOKE_RETURN {
                        return INVOKE_RETURN(inputStream.readUnsignedByte())
                    }
                },
                object : ClassBinding<GET_LOCAL_VARIABLE_TUPLE>(153, GET_LOCAL_VARIABLE_TUPLE::class) {
                    override fun serialize(outputStream: DataOutputStream, element: GET_LOCAL_VARIABLE_TUPLE) {
                        outputStream.writeByte(element.level)
                        outputStream.writeByte(element.length)
                    }

                    override fun deserialize(inputStream: DataInputStream): GET_LOCAL_VARIABLE_TUPLE {
                        return GET_LOCAL_VARIABLE_TUPLE(inputStream.readUnsignedByte(), inputStream.readUnsignedByte())
                    }
                }
        )

        for (binding in bindings) {
            @Suppress("UNCHECKED_CAST")
            addBinding(binding as PolymorphicBinding<Instruction>)
        }
    }
}
