package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.common.instruction.Instruction

class BlockHandle(val block: Int) {
    val executeInstruction: Instruction get() = Instruction.EXECUTE_BLOCK(block)
}
