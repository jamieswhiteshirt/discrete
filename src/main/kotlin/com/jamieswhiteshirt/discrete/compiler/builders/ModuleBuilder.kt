package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.common.code.ModuleVariableDefinition
import com.jamieswhiteshirt.discrete.common.code.scope.ModuleScopeDefinition
import com.jamieswhiteshirt.discrete.compiler.ast.Provider
import com.jamieswhiteshirt.discrete.compiler.ast.declaration.Declaration
import com.jamieswhiteshirt.discrete.compiler.builders.DeclarationHandle
import com.jamieswhiteshirt.discrete.compiler.ast.statement.Statement
import java.util.*

class ModuleBuilder(val moduleName: String, programBuilder: ProgramBuilder, statements: List<Statement>) : ScopeBuilder(programBuilder) {
    override val localVariableLevel: Int = 0

    init {
        for (statement in statements) {
            statement.evaluate(this)
        }
    }

    val moduleVariables = ArrayList<ModuleVariableDefinition>()

    fun addModuleVariable(variableDeclaration: Declaration.Variable): DeclarationHandle.Variable.Module {
        val handle = addDeclarationHandle(variableDeclaration.localIdentifier, DeclarationHandle.Variable.Module(moduleName, variableDeclaration.localIdentifier))
        moduleVariables.add(ModuleVariableDefinition(variableDeclaration.localIdentifier, variableDeclaration.mutability))
        return handle
    }

    override fun createDefinition(): ModuleScopeDefinition {
        val blocks = this.blocks.map { it.createDefinition() }.toTypedArray()
        val functions = this.functions.map { it.createDefinition() }.toTypedArray()
        val labels = this.labels.toTypedArray()
        val instructions = this.instructions.toTypedArray()
        val localVariables = this.localVariables.toTypedArray()
        val moduleVariables = this.moduleVariables.toTypedArray()
        return ModuleScopeDefinition(moduleName, blocks, functions, labels, instructions, localVariables, moduleVariables)
    }
}