package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.common.code.scope.FunctionScopeDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.compiler.ast.Provider
import com.jamieswhiteshirt.discrete.compiler.ast.declaration.Declaration
import com.jamieswhiteshirt.discrete.compiler.builders.DeclarationHandle
import com.jamieswhiteshirt.discrete.compiler.ast.statement.Statement

class FunctionBuilder(parent: ScopeBuilder, parameters: List<Declaration.Variable>, statements: List<Statement>) : ScopeBuilder(parent) {
    override val localVariableLevel: Int = parent.localVariableLevel + 1
    val parameterCount = parameters.size

    init {
        addDeclarationHandle("params", DeclarationHandle.Builtin(Instruction.GET_LOCAL_VARIABLE_TUPLE(localVariableLevel, parameters.size)))
        for (parameter in parameters) {
            addLocalVariable(parameter)
        }

        for (statement in statements) {
            statement.evaluate(this)
        }
    }

    override fun createDefinition(): FunctionScopeDefinition {
        val blocks = this.blocks.map { it.createDefinition() }.toTypedArray()
        val functions = this.functions.map { it.createDefinition() }.toTypedArray()
        val labels = this.labels.toTypedArray()
        val instructions = this.instructions.toTypedArray()
        val localVariables = this.localVariables.toTypedArray()
        return FunctionScopeDefinition(blocks, functions, labels, instructions, localVariables, parameterCount)
    }
}