package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.common.instruction.Instruction

sealed class DeclarationHandle {
    abstract val pushInstruction: Instruction

    sealed class Variable : DeclarationHandle() {
        abstract val declareInstruction: Instruction

        class Local(val level: Int, val variable: Int) : Variable() {
            override val pushInstruction: Instruction get() = Instruction.GET_LOCAL_VARIABLE(level, variable)
            override val declareInstruction: Instruction get() = Instruction.DECLARE_LOCAL_VARIABLE(level, variable)
        }

        class Module(val module: String, val identifier: String) : Variable() {
            override val pushInstruction: Instruction get() = Instruction.GET_MODULE_VARIABLE(module, identifier)
            override val declareInstruction: Instruction get() = Instruction.DECLARE_MODULE_VARIABLE(module, identifier)
        }
    }

    class Builtin(override val pushInstruction: Instruction) : DeclarationHandle()
}
