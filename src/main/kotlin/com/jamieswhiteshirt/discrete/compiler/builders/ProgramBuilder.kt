package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.common.module.ModuleDefinition
import com.jamieswhiteshirt.discrete.compiler.builders.DeclarationHandle

class ProgramBuilder : DeclarationAccess() {
    init {
        addDeclarationHandle("number"    , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_NUMBER))
        addDeclarationHandle("integer"   , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_INTEGER))
        addDeclarationHandle("float"     , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_FLOAT))
        addDeclarationHandle("identity"  , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_IDENTITY))
        addDeclarationHandle("string"    , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_STRING))
        addDeclarationHandle("boolean"   , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_BOOLEAN))
        addDeclarationHandle("invokable" , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_INVOKABLE))
        addDeclarationHandle("function"  , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_FUNCTION))
        addDeclarationHandle("collection", DeclarationHandle.Builtin(Instruction.PUSH_TYPE_COLLECTION))
        addDeclarationHandle("set"       , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_SET))
        addDeclarationHandle("tuple"     , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_TUPLE))
        addDeclarationHandle("mapping"   , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_MAPPING))
        addDeclarationHandle("type"      , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_TYPE))
        addDeclarationHandle("undefined" , DeclarationHandle.Builtin(Instruction.PUSH_TYPE_UNDEFINED))
        addDeclarationHandle("true"      , DeclarationHandle.Builtin(Instruction.PUSH_TRUE))
        addDeclarationHandle("false"     , DeclarationHandle.Builtin(Instruction.PUSH_FALSE))
    }

    fun importModule(module: ModuleDefinition) {
        for (identifier in module.identifiers) {
            addModuleVariable(identifier, identifier, module.moduleName)
        }
    }
}
