package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.common.code.scope.BlockScopeDefinition
import com.jamieswhiteshirt.discrete.compiler.ast.Provider
import com.jamieswhiteshirt.discrete.compiler.ast.statement.Statement

class BlockBuilder(parent: ScopeBuilder, statements: List<Statement>) : ScopeBuilder(parent) {
    override val localVariableLevel: Int = parent.localVariableLevel + 1

    init {
        for (statement in statements) {
            statement.evaluate(this)
        }
    }

    override fun createDefinition(): BlockScopeDefinition {
        val blocks = this.blocks.map { it.createDefinition() }.toTypedArray()
        val functions = this.functions.map { it.createDefinition() }.toTypedArray()
        val labels = this.labels.toTypedArray()
        val instructions = this.instructions.toTypedArray()
        val localVariables = this.localVariables.toTypedArray()
        return BlockScopeDefinition(blocks, functions, labels, instructions, localVariables)
    }
}