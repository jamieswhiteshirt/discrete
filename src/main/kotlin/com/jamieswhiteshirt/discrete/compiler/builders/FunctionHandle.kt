package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.common.instruction.Instruction

class FunctionHandle(val function: Int) {
    val newInstruction: Instruction get() = Instruction.NEW_FUNCTION(function)
}
