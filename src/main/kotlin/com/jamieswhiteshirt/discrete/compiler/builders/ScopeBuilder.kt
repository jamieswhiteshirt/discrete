package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.common.code.LocalVariableDefinition
import com.jamieswhiteshirt.discrete.common.code.ScopeDefinition
import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.compiler.ast.declaration.Declaration
import com.jamieswhiteshirt.discrete.compiler.builders.DeclarationHandle
import com.jamieswhiteshirt.discrete.compiler.ast.statement.Statement
import java.util.*

abstract class ScopeBuilder(val parentBuilder: DeclarationAccess) : DeclarationAccess() {
    val instructions = ArrayList<Instruction>()

    fun addInstruction(instruction: Instruction) {
        instructions.add(instruction)
    }

    val blocks = ArrayList<BlockBuilder>()
    protected val nextBlockID: Int get() = blocks.size

    fun addBlock(block: BlockBuilder): BlockHandle {
        val handle = BlockHandle(nextBlockID)
        blocks.add(block)
        return handle
    }

    val functions = ArrayList<FunctionBuilder>()
    protected val nextFunctionID: Int get() = functions.size

    fun addFunction(function: FunctionBuilder): FunctionHandle {
        val handle = FunctionHandle(nextFunctionID)
        functions.add(function)
        return handle
    }

    val labels = ArrayList<Int>()
    protected val nextLabelID: Int get() = labels.size

    fun withLabel(applied: (LabelHandle) -> Unit) {
        val handle = LabelHandle(nextLabelID)
        labels.add(-1)
        applied(handle)
        labels[handle.label] = instructions.size
    }

    abstract val localVariableLevel: Int
    val localVariables = ArrayList<LocalVariableDefinition>()
    protected val nextLocalVariableID: Int get() = localVariables.size

    fun addLocalVariable(variableDeclaration: Declaration.Variable): DeclarationHandle.Variable.Local {
        val handle = addDeclarationHandle(variableDeclaration.localIdentifier, DeclarationHandle.Variable.Local(localVariableLevel, nextLocalVariableID))
        localVariables.add(LocalVariableDefinition(variableDeclaration.mutability))
        return handle
    }

    override fun getDeclaration(identifier: String): DeclarationHandle? {
        return super.getDeclaration(identifier) ?: parentBuilder.getDeclaration(identifier)
    }

    abstract fun createDefinition(): ScopeDefinition
}
