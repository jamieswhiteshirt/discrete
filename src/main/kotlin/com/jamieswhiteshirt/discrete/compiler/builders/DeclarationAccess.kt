package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.compiler.ast.InvalidDeclarationError
import com.jamieswhiteshirt.discrete.compiler.builders.DeclarationHandle
import java.util.*

abstract class DeclarationAccess {
    protected val declarationHandles: MutableMap<String, DeclarationHandle> = HashMap()

    open fun getDeclaration(identifier: String): DeclarationHandle? = declarationHandles[identifier]

    protected fun <T : DeclarationHandle> addDeclarationHandle(identifier: String, declarationHandle: T): T {
        if (declarationHandles.containsKey(identifier)) {
            throw InvalidDeclarationError("Cannot shadow $identifier because it is already declared in this scope")
        }
        declarationHandles[identifier] = declarationHandle
        return declarationHandle
    }

    fun addModuleVariable(localIdentifier: String, foreignIdentifier: String, module: String): DeclarationHandle.Variable.Module {
        return addDeclarationHandle(localIdentifier, DeclarationHandle.Variable.Module(module, foreignIdentifier))
    }
}
