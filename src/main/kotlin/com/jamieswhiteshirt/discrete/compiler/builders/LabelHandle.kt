package com.jamieswhiteshirt.discrete.compiler.builders

import com.jamieswhiteshirt.discrete.common.instruction.Instruction

class LabelHandle(val label: Int) {
    val jump = Instruction.JUMP(label)
    val jumpIfFalse = Instruction.JUMP_IF_FALSE(label)
    val jumpIfTrue = Instruction.JUMP_IF_TRUE(label)
}
