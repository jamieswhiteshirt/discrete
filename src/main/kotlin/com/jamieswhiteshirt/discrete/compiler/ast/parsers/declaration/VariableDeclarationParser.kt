package com.jamieswhiteshirt.discrete.compiler.ast.parsers.declaration

import com.jamieswhiteshirt.discrete.compiler.ast.declaration.Declaration
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.CompileParser
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.*
import com.jamieswhiteshirt.util.search.*

object VariableDeclarationParser : CompileParser<Declaration.Variable, VariableDeclarationCompiler>() {
    private val varSearch = SequenceSearch(
            ExactEntry(MatcherSearch(EqualityMatcher<Token>(WordToken("var"))))
    )
    private val valSearch = SequenceSearch(
            ExactEntry(MatcherSearch(EqualityMatcher<Token>(WordToken("val"))))
    )
    private val identifierSearch = SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher<Token>(WordToken::class))),
            ExactEntry(Union(
                    MatcherSearch(EqualityMatcher<Token>(SeparatorToken())),
                    IsEnd()
            ))
    )

    override fun getCompiler(): VariableDeclarationCompiler {
        return VariableDeclarationCompiler()
    }

    override fun getChainReader(compiler: VariableDeclarationCompiler): SearchChainReader<Token> {
        val chainReader = SearchChainReader<Token>()
        //var
        chainReader.appendChainEntry(varSearch, fun(@Suppress("UNUSED_PARAMETER") result: SearchResult<Token>) : Unit {
            compiler.addVar()
        })
        //val
        chainReader.appendChainEntry(valSearch, fun(@Suppress("UNUSED_PARAMETER") result: SearchResult<Token>) : Unit {
            compiler.addVal()
        })
        //identifier
        chainReader.appendChainEntry(identifierSearch, fun(result: SearchResult<Token>) : Unit {
            compiler.addIdentifier((result[0].begin.value as WordToken).value)
        })

        return chainReader
    }
}
