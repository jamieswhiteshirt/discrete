package com.jamieswhiteshirt.discrete.compiler.ast.expression

import com.jamieswhiteshirt.discrete.compiler.builders.LabelHandle
import com.jamieswhiteshirt.discrete.compiler.builders.ScopeBuilder

abstract class Branch(val consequent: Expression) {}

class ConditionalBranch(val condition: Expression, consequent: Expression) : Branch(consequent) {
    fun evaluate(builder: ScopeBuilder, lastLabel: LabelHandle) {
        condition.evaluateReturn(builder)
        builder.withLabel { label ->
            builder.addInstruction(label.jumpIfFalse)
            consequent.evaluate(builder)
            builder.addInstruction(lastLabel.jump)
        }
    }

    fun evaluateReturn(builder: ScopeBuilder, lastLabel: LabelHandle) {
        condition.evaluateReturn(builder)
        builder.withLabel { label ->
            builder.addInstruction(label.jumpIfFalse)
            consequent.evaluateReturn(builder)
            builder.addInstruction(lastLabel.jump)
        }
    }

    override fun toString(): String = "if ($condition) $consequent"
}

class UnconditionalBranch(consequent: Expression) : Branch(consequent) {
    fun evaluateLast(builder: ScopeBuilder) {
        consequent.evaluate(builder)
    }

    fun evaluateReturnLast(builder: ScopeBuilder) {
        consequent.evaluateReturn(builder)
    }

    override fun toString(): String = consequent.toString()
}
