package com.jamieswhiteshirt.discrete.compiler.ast.declaration

import com.jamieswhiteshirt.discrete.common.code.VariableMutability

sealed class Declaration(val localIdentifier: String) {
    abstract override fun toString(): String

    class Variable(identifier: String, val mutability: VariableMutability) : Declaration(identifier) {
        override fun toString(): String = "$mutability $localIdentifier"
    }

    sealed class Import(identifier: String, val foreignIdentifier: String) : Declaration(identifier) {
        class Implicit(identifier: String) : Import(identifier, identifier) {
            override fun toString(): String = localIdentifier
        }

        class Explicit(localIdentifier: String, foreignIdentifier: String) : Import(localIdentifier, foreignIdentifier) {
            override fun toString(): String = "$foreignIdentifier as $localIdentifier"
        }
    }
}

