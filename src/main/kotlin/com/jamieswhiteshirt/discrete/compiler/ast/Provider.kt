package com.jamieswhiteshirt.discrete.compiler.ast

import com.jamieswhiteshirt.discrete.compiler.builders.DeclarationAccess
import com.jamieswhiteshirt.discrete.compiler.builders.ScopeBuilder

interface Provider<T> {
    fun getAll(frame: ScopeBuilder): List<T>

    fun getOne(frame: ScopeBuilder): T
}
