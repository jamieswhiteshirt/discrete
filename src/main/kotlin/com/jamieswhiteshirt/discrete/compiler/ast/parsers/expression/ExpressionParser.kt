package com.jamieswhiteshirt.discrete.compiler.ast.parsers.expression

import com.jamieswhiteshirt.discrete.compiler.ast.*
import com.jamieswhiteshirt.discrete.compiler.ast.expression.*
import com.jamieswhiteshirt.discrete.compiler.builders.*
import com.jamieswhiteshirt.discrete.compiler.ast.operators.*
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.mapping.ExplicitMappingParser
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.statement.StatementParser
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.SplitCompileParser
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.declaration.VariableDeclarationParser
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.*
import com.jamieswhiteshirt.util.iterator.ExtIterator
import com.jamieswhiteshirt.util.search.*

object ExpressionParser : SplitCompileParser<Expression, ExpressionCompiler>(
        SequenceSearch(
                ReluctantEntry(traversalSearch, ZeroOrMore),
                ExactEntry(Union(
                        MatcherSearch<Token>(EqualityMatcher(SeparatorToken())),
                        IsEnd()
                ))
        )
) {

    private val functionSearch = SequenceSearch(
            ExactEntry(roundBlockSearch),
            ExactEntry(curlyBlockSearch)
    )
    private val identitySearch = MatcherSearch<Token>(EqualityMatcher(SymbolToken('?')))
    private val identifierSearch = MatcherSearch<Token>(TypeMatcher(WordToken::class))
    private val stringLiteralSearch = MatcherSearch<Token>(TypeMatcher(StringLiteralToken::class))
    private val intLiteralSearch = MatcherSearch<Token>(TypeMatcher(IntLiteralToken::class))
    private val floatLiteralSearch = MatcherSearch<Token>(TypeMatcher(FloatLiteralToken::class))
    private val ambiguousOperatorSearch = MatcherSearch(OrMatcher<Token>(
            EqualityMatcher(OperatorToken(":")),
            EqualityMatcher(OperatorToken("++")),
            EqualityMatcher(OperatorToken("--")),
            EqualityMatcher(OperatorToken("-"))
    ))
    private val operatorSearch = MatcherSearch<Token>(TypeMatcher(OperatorToken::class))
    private val explicitMappingSearch = SequenceSearch(
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('[')))),
            ReluctantEntry(SequenceSearch(
                    ReluctantEntry(traversalSearch, OneOrMore),
                    ExactEntry(MatcherSearch<Token>(EqualityMatcher(WordToken("to")))),
                    ReluctantEntry(traversalSearch, OneOrMore),
                    ExactEntry(MatcherSearch<Token>(EqualityMatcher(SeparatorToken())))
            ), ZeroOrMore),
            ReluctantEntry(traversalSearch, OneOrMore),
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(WordToken("to")))),
            ReluctantEntry(traversalSearch, OneOrMore),
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken(']'))))
    )
    private val implicitMappingSearch = SequenceSearch(
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('[')))),
            ReluctantEntry(SequenceSearch(
                    ReluctantEntry(traversalSearch, OneOrMore),
                    ExactEntry(MatcherSearch<Token>(EqualityMatcher(SeparatorToken())))
            ), ZeroOrMore),
            ReluctantEntry(traversalSearch, OneOrMore),
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken(']'))))
    )
    private val emptyTupleSearch = SequenceSearch(
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('(')))),
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(SeparatorToken()))),
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken(')'))))
    )
    private val tupleSearch = SequenceSearch(
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('(')))),
            ReluctantEntry(SequenceSearch(
                    ReluctantEntry(traversalSearch, OneOrMore),
                    ExactEntry(MatcherSearch<Token>(EqualityMatcher(SeparatorToken())))
            ), ZeroOrMore),
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken(')'))))
    )

    override fun getCompiler(): ExpressionCompiler {
        return ExpressionCompiler()
    }

    override fun getChainReader(compiler: ExpressionCompiler): SearchChainReader<Token> {
        val chainReader = SearchChainReader<Token>()
        //function expression
        chainReader.appendChainEntry(functionSearch, fun(result: SearchResult<Token>) : Unit {
            val parameterDeclarations = VariableDeclarationParser.getAll(result[0].begin + 1, result[0].end - 1)
            val statements = StatementParser.getAll(result[1].begin + 1, result[1].end - 1)
            compiler.addExpression(FunctionExpression(parameterDeclarations, statements))
        })
        //empty set expression
        chainReader.appendChainEntry(emptyCurlyBlockSearch, fun(@Suppress("UNUSED_PARAMETER") result: SearchResult<Token>) : Unit {
            compiler.addExpression(SetExpression(emptyList()))
        })
        //set expression
        chainReader.appendChainEntry(curlyBlockSearch, fun(result: SearchResult<Token>) : Unit {
            compiler.addExpression(SetExpression(getAll(result.begin + 1, result.end - 1)))
        })
        //empty map expression
        chainReader.appendChainEntry(emptySquareBlockSearch, fun(@Suppress("UNUSED_PARAMETER") result: SearchResult<Token>) : Unit {
            compiler.addExpression(ImplicitMappingExpression(emptyList()))
        })
        //explicit mapping expression
        chainReader.appendChainEntry(explicitMappingSearch, fun(result: SearchResult<Token>) : Unit {
            compiler.addExpression(ExplicitMappingExpression(ExplicitMappingParser.getAll(result.begin + 1, result.end - 1)))
        })
        //implicit mapping expression
        chainReader.appendChainEntry(implicitMappingSearch, fun(result: SearchResult<Token>) : Unit {
            compiler.addExpression(ImplicitMappingExpression(getAll(result.begin + 1, result.end - 1)))
        })
        //empty tuple expression
        chainReader.appendChainEntry(emptyTupleSearch, fun(@Suppress("UNUSED_PARAMETER") result: SearchResult<Token>) : Unit {
            compiler.addExpression(TupleExpression(emptyList()))
        })
        //tuple expression
        chainReader.appendChainEntry(tupleSearch, fun(result: SearchResult<Token>) : Unit {
            compiler.addExpression(TupleExpression(getAll(result.begin + 1, result.end - 1)))
        })
        //identity expression
        chainReader.appendChainEntry(identitySearch, fun(@Suppress("UNUSED_PARAMETER") result: SearchResult<Token>) : Unit {
            compiler.addExpression(IdentityExpression())
        })
        //if
        chainReader.appendChainEntry(ifSearch, fun(result: SearchResult<Token>) : Unit {
            val conditionExpression = getOne(result[1].begin + 1, result[1].end - 1)
            val consequentExpression = getOne(result[2].begin + 1, result[2].end - 1)
            compiler.addIf(conditionExpression, consequentExpression)
        })
        //else if
        chainReader.appendChainEntry(elseIfSearch, fun(result: SearchResult<Token>) : Unit {
            val conditionExpression = getOne(result[1].begin + 1, result[1].end - 1)
            val consequentExpression = getOne(result[2].begin + 1, result[2].end - 1)
            compiler.addElseIf(conditionExpression, consequentExpression)
        })
        //else
        chainReader.appendChainEntry(elseSearch, fun(result: SearchResult<Token>) : Unit {
            val consequentExpression = getOne(result[1].begin + 1, result[1].end - 1)
            compiler.addElse(consequentExpression)
        })
        //get declaration expression
        chainReader.appendChainEntry(identifierSearch, fun(result: SearchResult<Token>) : Unit {
            val identifier = (result.begin.value as WordToken).value
            compiler.addExpression(GetDeclarationExpression(identifier))
        })
        //string literal expression
        chainReader.appendChainEntry(stringLiteralSearch, fun(result: SearchResult<Token>) : Unit {
            val token = result.begin.value as StringLiteralToken
            compiler.addExpression(StringLiteralExpression(token.value))
        })
        //float literal expression
        chainReader.appendChainEntry(floatLiteralSearch, fun(result: SearchResult<Token>) : Unit {
            val token = result.begin.value as FloatLiteralToken
            compiler.addExpression(FloatLiteralExpression(token.value))
        })
        //int literal expression
        chainReader.appendChainEntry(intLiteralSearch, fun(result: SearchResult<Token>) : Unit {
            val token = result.begin.value as IntLiteralToken
            compiler.addExpression(IntLiteralExpression(token.value))
        })
        //parameters/parenthesis
        chainReader.appendChainEntry(roundBlockSearch, fun(result: SearchResult<Token>) : Unit {
            val expressions = getAll(result.begin + 1, result.end - 1)
            compiler.addBlock(expressions)
        })
        //ambiguous operators
        chainReader.appendChainEntry(ambiguousOperatorSearch, fun(result: SearchResult<Token>) : Unit {
            val token = result.begin.value as OperatorToken
            compiler.addAmbiguousOperator(token)
        })
        //operator
        chainReader.appendChainEntry(operatorSearch, fun(result: SearchResult<Token>) : Unit {
            val token = result.begin.value as OperatorToken
            val operator = operators[token.value]
            if (operator is UnaryOperator) {
                compiler.addUnaryOperator(operator)
            }
            else if (operator is BinaryOperator) {
                compiler.addBinaryOperator(operator)
            }
        })
        //unexpected token
        chainReader.appendChainEntry(IsNotEnd<Token>(), fun(result: SearchResult<Token>) : Unit {
            throw UnexpectedTokenError("Unexpected token ${result.begin.value} in expression")
        })

        return chainReader
    }
}