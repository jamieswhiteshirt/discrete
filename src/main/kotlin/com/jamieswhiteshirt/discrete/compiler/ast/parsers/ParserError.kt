package com.jamieswhiteshirt.discrete.compiler.ast.parsers

import com.jamieswhiteshirt.discrete.compiler.ast.SyntaxError

open class ParserError : SyntaxError {
    constructor() : super()
    constructor(message: String) : super(message)
}

open class IntermediateValueError : ParserError {
    constructor() : super()
    constructor(message: String) : super(message)
}

open class InvalidAmountError : ParserError {
    constructor() : super()
    constructor(message: String) : super(message)
}
