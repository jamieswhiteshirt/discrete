package com.jamieswhiteshirt.discrete.compiler.ast.parsers.mapping

import com.jamieswhiteshirt.discrete.compiler.ast.parsers.expression.ExpressionParser
import com.jamieswhiteshirt.discrete.compiler.ast.traversalSearch
import com.jamieswhiteshirt.discrete.compiler.ast.UnexpectedTokenError
import com.jamieswhiteshirt.discrete.compiler.ast.expression.Expression
import com.jamieswhiteshirt.discrete.compiler.builders.ScopeBuilder
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.SplitParser
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.SeparatorToken
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.Token
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.WordToken
import com.jamieswhiteshirt.util.iterator.ExtIterator
import com.jamieswhiteshirt.util.search.*

object ExplicitMappingParser : SplitParser<Pair<Expression, Expression>>(
        SequenceSearch(
                ReluctantEntry(traversalSearch, ZeroOrMore),
                ExactEntry(Union(
                        MatcherSearch<Token>(EqualityMatcher(SeparatorToken())),
                        IsEnd()
                ))
        )
) {
    val explicitMappingSearch = SequenceSearch(
            ReluctantEntry(traversalSearch, OneOrMore),
            ExactEntry(MatcherSearch<Token>(EqualityMatcher(WordToken("to")))),
            ReluctantEntry(traversalSearch, OneOrMore)
    )

    override fun getChainReader(list: MutableList<Pair<Expression, Expression>>): SearchChainReader<Token> {
        val chainReader = SearchChainReader<Token>()
        //explicit mapping
        chainReader.appendChainEntry(explicitMappingSearch, fun(result: SearchResult<Token>) : Unit {
            val key = ExpressionParser.getOne(result[0].begin, result[0].end)
            val value = ExpressionParser.getOne(result[2].begin, result[2].end)
            list.add(Pair(key, value))
        })
        //unexpected token
        chainReader.appendChainEntry(IsNotEnd<Token>(), fun(result: SearchResult<Token>) : Unit {
            throw UnexpectedTokenError("Unexpected token ${result.begin.value} in mapping")
        })

        return chainReader
    }
}
