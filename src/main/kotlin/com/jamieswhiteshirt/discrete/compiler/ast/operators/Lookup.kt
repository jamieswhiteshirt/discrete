package com.jamieswhiteshirt.discrete.compiler.ast.operators

val arithmeticLogicalBinaryOperators = hashMapOf(
        "|" to BitwiseOrOperator,
        "&" to BitwiseAndOperator,
        "^" to BitwiseXorOperator,
        "||" to LogicalOrOperator,
        "&&" to LogicalAndOperator,
        "^^" to LogicalXorOperator,
        "|?" to ShortCircuitLogicalOrOperator,
        "&?" to ShortCircuitLogicalAndOperator,
        "<<" to LeftShiftOperator,
        ">>" to RightShiftOperator,
        "+" to AdditionOperator,
        "*" to MultiplicationOperator,
        "/" to DivisionOperator,
        "//" to FloorDivisionOperator,
        "%" to RemainderOperator,
        "==" to EqualityOperator,
        "!=" to InequalityOperator,
        "<=" to LessOrEqualOperator,
        ">=" to GreaterOrEqualOperator,
        "<" to LessOperator,
        ">" to GreaterOperator,
        "." to ConcatenationOperator,
        "is" to IsOperator,
        "!!is" to IsNotOperator,
        "in" to InOperator,
        "!!in" to NotInOperator
)

val assigningBinaryOperators = hashMapOf(
        "=" to AssignOperator,
        "|=" to BitwiseOrCompoundAssignOperator,
        "&=" to BitwiseAndCompoundAssignOperator,
        "^=" to BitwiseXorCompoundAssignOperator,
        "||=" to LogicalOrCompoundAssignOperator,
        "&&=" to LogicalAndCompoundAssignOperator,
        "^^=" to LogicalXorCompoundAssignOperator,
        "|?=" to ShortCircuitLogicalOrCompoundAssignOperator,
        "&?=" to ShortCircuitLogicalAndCompoundAssignOperator,
        "<<=" to LeftShiftCompoundAssignOperator,
        ">>=" to RightShiftCompoundAssignOperator,
        "+=" to AdditionCompoundAssignOperator,
        "-=" to SubtractionCompoundAssignOperator,
        "*=" to MultiplicationCompoundAssignOperator,
        "/=" to DivisionCompoundAssignOperator,
        "//=" to FloorDivisionCompoundAssignOperator,
        "%=" to RemainderCompoundAssignOperator,
        ".=" to ConcatenationCompoundAssignOperator
)

val binaryOperators = arithmeticLogicalBinaryOperators + assigningBinaryOperators

val unaryOperators = hashMapOf(
        "!" to BitwiseNotOperator,
        "!!" to LogicalNotOperator
)

val operators = binaryOperators + unaryOperators
