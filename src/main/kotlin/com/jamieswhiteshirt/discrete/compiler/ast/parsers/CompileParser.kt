package com.jamieswhiteshirt.discrete.compiler.ast.parsers

import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.Token
import com.jamieswhiteshirt.util.iterator.ExtIterator
import com.jamieswhiteshirt.util.search.SearchChainReader

/**
 * Iterates a list of tokens and parses it into either a list of items or a single item
 * Incorporates a compiler that processes the whole output of the tokenparser
 */
abstract class CompileParser<T, C : Compiler<T>> : Parser<T>() {
    abstract fun getCompiler(): C
    abstract fun getChainReader(compiler: C): SearchChainReader<Token>

    override fun getAll(begin: ExtIterator<Token>, end: ExtIterator<Token>): List<T> {
        val compiler = getCompiler()
        getChainReader(compiler).read(begin, end)
        return compiler.compile()
    }

    override fun getOne(begin: ExtIterator<Token>, end: ExtIterator<Token>): T {
        val compiler = getCompiler()
        getChainReader(compiler).read(begin, end)
        return compiler.compileOne()
    }
}
