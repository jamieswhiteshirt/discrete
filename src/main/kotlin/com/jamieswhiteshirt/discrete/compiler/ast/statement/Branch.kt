package com.jamieswhiteshirt.discrete.compiler.ast.statement

import com.jamieswhiteshirt.discrete.compiler.ast.expression.Expression
import com.jamieswhiteshirt.discrete.compiler.builders.BlockBuilder
import com.jamieswhiteshirt.discrete.compiler.builders.LabelHandle
import com.jamieswhiteshirt.discrete.compiler.builders.ScopeBuilder

abstract class Branch(val consequent: List<Statement>) {
    abstract fun evaluate(builder: ScopeBuilder, lastLabel: LabelHandle)

    abstract fun evaluateLast(builder: ScopeBuilder, lastLabel: LabelHandle)
}

class ConditionalBranch(val condition: Expression, consequent: List<Statement>) : Branch(consequent) {
    override fun evaluate(builder: ScopeBuilder, lastLabel: LabelHandle) {
        condition.evaluateReturn(builder)
        builder.withLabel { label ->
            builder.addInstruction(label.jumpIfFalse)
            builder.addInstruction(builder.addBlock(BlockBuilder(builder, consequent)).executeInstruction)
            builder.addInstruction(lastLabel.jump)
        }
    }

    override fun evaluateLast(builder: ScopeBuilder, lastLabel: LabelHandle) {
        condition.evaluateReturn(builder)
        builder.addInstruction(lastLabel.jumpIfFalse)
        builder.addInstruction(builder.addBlock(BlockBuilder(builder, consequent)).executeInstruction)
    }

    override fun toString(): String = "if ($condition) {\n${consequent.joinToString("\n").prependIndent("    ")}\n}"
}

class UnconditionalBranch(consequent: List<Statement>) : Branch(consequent) {
    override fun evaluate(builder: ScopeBuilder, lastLabel: LabelHandle) {
        builder.addInstruction(builder.addBlock(BlockBuilder(builder, consequent)).executeInstruction)
        builder.addInstruction(lastLabel.jump)
    }

    override fun evaluateLast(builder: ScopeBuilder, lastLabel: LabelHandle) {
        builder.addInstruction(builder.addBlock(BlockBuilder(builder, consequent)).executeInstruction)
    }

    override fun toString(): String = "{\n${consequent.joinToString("\n").prependIndent("    ")}\n}"
}
