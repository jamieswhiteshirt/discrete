package com.jamieswhiteshirt.discrete.compiler.ast.parsers.declaration

import com.jamieswhiteshirt.discrete.compiler.ast.UnexpectedTokenError
import com.jamieswhiteshirt.discrete.compiler.builders.ScopeBuilder
import com.jamieswhiteshirt.discrete.compiler.ast.declaration.Declaration
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.SplitParser
import com.jamieswhiteshirt.discrete.compiler.ast.traversalSearch
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.*
import com.jamieswhiteshirt.util.iterator.ExtIterator
import com.jamieswhiteshirt.util.search.*

object ImportDeclarationParser : SplitParser<Declaration.Import>(
        SequenceSearch(
                ReluctantEntry(traversalSearch, ZeroOrMore),
                ExactEntry(Union(
                        MatcherSearch<Token>(EqualityMatcher(SeparatorToken())),
                        IsEnd()
                ))
        )
) {
    private val identifierAsIdentifierSearch = SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher<Token>(WordToken::class))),
            ExactEntry(MatcherSearch(EqualityMatcher<Token>(WordToken("as")))),
            ExactEntry(MatcherSearch(TypeMatcher<Token>(WordToken::class))),
            ExactEntry(Union(
                    MatcherSearch(EqualityMatcher<Token>(SeparatorToken())),
                    IsEnd()
            ))
    )
    private val identifierSearch = SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher<Token>(WordToken::class))),
            ExactEntry(Union(
                    MatcherSearch(EqualityMatcher<Token>(SeparatorToken())),
                    IsEnd()
            ))
    )

    override fun getChainReader(list: MutableList<Declaration.Import>): SearchChainReader<Token> {
        val chainReader = SearchChainReader<Token>()
        //identifier as identifier
        chainReader.appendChainEntry(identifierAsIdentifierSearch, fun(result: SearchResult<Token>) : Unit {
            val foreignIdentifierToken = result[0].begin.value as WordToken
            val identifierToken = result[2].begin.value as WordToken
            list.add(Declaration.Import.Explicit(identifierToken.value, foreignIdentifierToken.value))
        })
        //identifier
        chainReader.appendChainEntry(identifierSearch, fun(result: SearchResult<Token>) : Unit {
            val identifierToken = result[0].begin.value as WordToken
            list.add(Declaration.Import.Implicit(identifierToken.value))
        })
        //unexpected token
        chainReader.appendChainEntry(IsNotEnd<Token>(), fun(result: SearchResult<Token>) : Unit {
            throw UnexpectedTokenError("Unexpected token ${result.begin.value} in import")
        })

        return chainReader
    }
}
