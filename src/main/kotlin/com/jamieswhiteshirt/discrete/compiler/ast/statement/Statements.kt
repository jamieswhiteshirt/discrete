package com.jamieswhiteshirt.discrete.compiler.ast.statement

import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.compiler.ast.expression.Expression
import com.jamieswhiteshirt.discrete.compiler.ast.declaration.Declaration
import com.jamieswhiteshirt.discrete.compiler.builders.BlockBuilder
import com.jamieswhiteshirt.discrete.compiler.builders.ScopeBuilder
import com.jamieswhiteshirt.discrete.compiler.instruction.providers.*

abstract class Statement {
    abstract fun evaluate(builder: ScopeBuilder)
}

abstract class DeclarationStatement() : Statement() {
    abstract val declarations: List<Declaration>
}

class ImportDeclarationStatement(override val declarations: List<Declaration.Import>, val module: String) : DeclarationStatement() {
    override fun toString() : String = "import ${declarations.joinToString(", ")} from $module;"

    override fun evaluate(builder: ScopeBuilder) { }
}

abstract class VariableDeclarationStatement : DeclarationStatement()

class ValueVariableDeclarationStatement(override val declarations: List<Declaration.Variable>, val value: Expression) : VariableDeclarationStatement() {
    override fun toString() : String = "${declarations.joinToString(", ")} = $value;"

    override fun evaluate(builder: ScopeBuilder) {
        if(declarations.size > 0) {
            value.evaluateReturn(builder)
            for (declaration in declarations) {
                if (declaration !== declarations.last()) {
                    builder.addInstruction(Instruction.DUP)
                }
                builder.addInstruction(builder.addLocalVariable(declaration).declareInstruction)
            }
        }
        else {
            value.evaluate(builder)
        }
    }
}

class EmptyVariableDeclarationStatement(override val declarations: List<Declaration.Variable>) : VariableDeclarationStatement() {
    override fun toString() : String = "${declarations.joinToString(", ")};"

    override fun evaluate(builder: ScopeBuilder) {
        for (declaration in declarations) {
            builder.addLocalVariable(declaration)
        }
    }
}

class BlockStatement(val statements: List<Statement>) : Statement() {
    override fun toString() : String = "{\n${statements.joinToString("\n").prependIndent("    ")}\n}"

    override fun evaluate(builder: ScopeBuilder) {
        builder.addInstruction(builder.addBlock(BlockBuilder(builder, statements)).executeInstruction)
    }
}

class ExpressionStatement(val expression: Expression) : Statement() {
    override fun toString() : String = "$expression;"

    override fun evaluate(builder: ScopeBuilder) {
        return expression.evaluate(builder)
    }
}

class ReturnStatement() : Statement() {
    override fun toString() : String = "return;"

    override fun evaluate(builder: ScopeBuilder) {
        builder.addInstruction(Instruction.RETURN)
    }
}

class ReturnValueStatement(val value: Expression) : Statement() {
    override fun toString() : String = "return $value;"

    override fun evaluate(builder: ScopeBuilder) {
        value.evaluateReturn(builder)
        builder.addInstruction(Instruction.RETURN_VALUE)
    }
}

class BranchingStatement(val branches: List<Branch>) : Statement() {
    override fun toString() : String = "${branches.joinToString("\nelse ")};"

    override fun evaluate(builder: ScopeBuilder) {
        builder.withLabel { lastLabel ->
            for (branch in branches) {
                if (branch != branches.last()) {
                    branch.evaluate(builder, lastLabel)
                }
                else {
                    branch.evaluateLast(builder, lastLabel)
                }
            }
        }
    }
}
