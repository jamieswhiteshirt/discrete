package com.jamieswhiteshirt.discrete.compiler.ast.operators

import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.compiler.builders.LabelHandle


abstract class Operator(val tokenString: String) {
    override fun toString(): String = tokenString
}

abstract class BinaryOperator(tokenString: String, val instruction: Instruction) : Operator(tokenString)
abstract class UnaryOperator(tokenString: String, val instruction: Instruction) : Operator(tokenString)

abstract class EvaluateBinaryOperator(tokenString: String, instruction: Instruction) : BinaryOperator(tokenString, instruction)
abstract class ArithmeticLogicEvaluateBinaryOperator(tokenString: String, instruction: Instruction) : EvaluateBinaryOperator(tokenString, instruction)
abstract class ShortCircuitLogicEvaluateBinaryOperator(tokenString: String, instruction: Instruction) : EvaluateBinaryOperator(tokenString, instruction) {
    abstract fun shortCircuitInstruction(label: LabelHandle) : Instruction
}

abstract class CompoundAssignBinaryOperator(val delegateOperator: BinaryOperator) : BinaryOperator(delegateOperator.tokenString + "=", delegateOperator.instruction)
abstract class ArithmeticLogicCompoundAssignBinaryOperator(operator: ArithmeticLogicEvaluateBinaryOperator) : CompoundAssignBinaryOperator(operator)
abstract class ShortCircuitLogicCompoundAssignBinaryOperator(operator: ShortCircuitLogicEvaluateBinaryOperator) : CompoundAssignBinaryOperator(operator) {
    fun shortCircuitInstruction(label: LabelHandle) : Instruction {
        return (delegateOperator as ShortCircuitLogicEvaluateBinaryOperator).shortCircuitInstruction(label)
    }
}

abstract class PreUnaryOperator(tokenString: String, instruction: Instruction) : UnaryOperator(tokenString, instruction)
abstract class ArithmeticLogicEvaluatePreUnaryOperator(tokenString: String, instruction: Instruction) : PreUnaryOperator(tokenString, instruction)
abstract class ArithmeticLogicCompoundAssignPreUnaryOperator(tokenString: String, instruction: Instruction) : PreUnaryOperator(tokenString, instruction)

abstract class PostUnaryOperator(tokenString: String, instruction: Instruction) : UnaryOperator(tokenString, instruction)
abstract class ArithmeticLogicCompoundAssignPostUnaryOperator(tokenString: String, instruction: Instruction) : PostUnaryOperator(tokenString, instruction)

object AssignOperator : BinaryOperator("=", Instruction.ASSIGN)

object BitwiseOrOperator : ArithmeticLogicEvaluateBinaryOperator("|", Instruction.BITWISE_OR)
object BitwiseAndOperator : ArithmeticLogicEvaluateBinaryOperator("&", Instruction.BITWISE_AND)
object BitwiseXorOperator : ArithmeticLogicEvaluateBinaryOperator("^", Instruction.BITWISE_XOR)
object LogicalOrOperator : ArithmeticLogicEvaluateBinaryOperator("||", Instruction.LOGICAL_OR)
object LogicalAndOperator : ArithmeticLogicEvaluateBinaryOperator("&&", Instruction.LOGICAL_AND)
object LogicalXorOperator : ArithmeticLogicEvaluateBinaryOperator("^^", Instruction.LOGICAL_XOR)
object LeftShiftOperator : ArithmeticLogicEvaluateBinaryOperator("<<", Instruction.LEFT_SHIFT)
object RightShiftOperator : ArithmeticLogicEvaluateBinaryOperator(">>", Instruction.RIGHT_SHIFT)
object AdditionOperator : ArithmeticLogicEvaluateBinaryOperator("+", Instruction.ADD)
object SubtractionOperator : ArithmeticLogicEvaluateBinaryOperator("-", Instruction.SUBTRACT)
object MultiplicationOperator : ArithmeticLogicEvaluateBinaryOperator("*", Instruction.MULTIPLY)
object DivisionOperator : ArithmeticLogicEvaluateBinaryOperator("/", Instruction.DIVIDE)
object FloorDivisionOperator : ArithmeticLogicEvaluateBinaryOperator("//", Instruction.DIVIDE_FLOOR)
object RemainderOperator : ArithmeticLogicEvaluateBinaryOperator("%", Instruction.REMAINDER)
object ConcatenationOperator : ArithmeticLogicEvaluateBinaryOperator(".", Instruction.CONCATENATE)
object IsOperator : ArithmeticLogicEvaluateBinaryOperator("is", Instruction.IS)
object IsNotOperator : ArithmeticLogicEvaluateBinaryOperator("!!is", Instruction.IS_NOT)
object InOperator : ArithmeticLogicEvaluateBinaryOperator("in", Instruction.IN)
object NotInOperator : ArithmeticLogicEvaluateBinaryOperator("!!in", Instruction.NOT_IN)
object EqualityOperator : ArithmeticLogicEvaluateBinaryOperator("==", Instruction.COMPARE_EQUAL)
object InequalityOperator : ArithmeticLogicEvaluateBinaryOperator("!=", Instruction.COMPARE_NOT_EQUAL)
object LessOrEqualOperator : ArithmeticLogicEvaluateBinaryOperator("<=", Instruction.COMPARE_LESS_OR_EQUAL)
object GreaterOrEqualOperator : ArithmeticLogicEvaluateBinaryOperator(">=", Instruction.COMPARE_GREATER_OR_EQUAL)
object LessOperator : ArithmeticLogicEvaluateBinaryOperator("<", Instruction.COMPARE_LESS)
object GreaterOperator : ArithmeticLogicEvaluateBinaryOperator(">", Instruction.COMPARE_GREATER)

object ShortCircuitLogicalOrOperator : ShortCircuitLogicEvaluateBinaryOperator("|?", Instruction.LOGICAL_OR) {
    override fun shortCircuitInstruction(label: LabelHandle): Instruction = label.jumpIfTrue
}
object ShortCircuitLogicalAndOperator : ShortCircuitLogicEvaluateBinaryOperator("&?", Instruction.LOGICAL_AND) {
    override fun shortCircuitInstruction(label: LabelHandle): Instruction = label.jumpIfFalse
}

object BitwiseOrCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(BitwiseOrOperator)
object BitwiseAndCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(BitwiseAndOperator)
object BitwiseXorCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(BitwiseXorOperator)
object LogicalOrCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(LogicalOrOperator)
object LogicalAndCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(LogicalAndOperator)
object LogicalXorCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(LogicalXorOperator)
object LeftShiftCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(LeftShiftOperator)
object RightShiftCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(RightShiftOperator)
object AdditionCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(AdditionOperator)
object SubtractionCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(SubtractionOperator)
object MultiplicationCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(MultiplicationOperator)
object DivisionCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(DivisionOperator)
object FloorDivisionCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(FloorDivisionOperator)
object RemainderCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(RemainderOperator)
object ConcatenationCompoundAssignOperator : ArithmeticLogicCompoundAssignBinaryOperator(ConcatenationOperator)

object ShortCircuitLogicalOrCompoundAssignOperator : ShortCircuitLogicCompoundAssignBinaryOperator(ShortCircuitLogicalOrOperator)
object ShortCircuitLogicalAndCompoundAssignOperator : ShortCircuitLogicCompoundAssignBinaryOperator(ShortCircuitLogicalAndOperator)

object BitwiseNotOperator : ArithmeticLogicEvaluatePreUnaryOperator("!", Instruction.BITWISE_NOT)
object LogicalNotOperator : ArithmeticLogicEvaluatePreUnaryOperator("!!", Instruction.LOGICAL_NOT)
object NegationOperator : ArithmeticLogicEvaluatePreUnaryOperator("-", Instruction.NEGATE)
object PreIncrementOperator : ArithmeticLogicCompoundAssignPreUnaryOperator("++", Instruction.INCREMENT)
object PreDecrementOperator : ArithmeticLogicCompoundAssignPreUnaryOperator("--", Instruction.DECREMENT)

object PostIncrementOperator : ArithmeticLogicCompoundAssignPostUnaryOperator("++", Instruction.INCREMENT)
object PostDecrementOperator : ArithmeticLogicCompoundAssignPostUnaryOperator("--", Instruction.DECREMENT)
