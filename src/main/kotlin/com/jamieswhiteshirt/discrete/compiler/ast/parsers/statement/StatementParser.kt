package com.jamieswhiteshirt.discrete.compiler.ast.parsers.statement

import com.jamieswhiteshirt.discrete.compiler.ast.*
import com.jamieswhiteshirt.discrete.compiler.builders.BlockBuilder
import com.jamieswhiteshirt.discrete.compiler.builders.ScopeBuilder
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.expression.ExpressionParser
import com.jamieswhiteshirt.discrete.compiler.ast.statement.*
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.SplitCompileParser
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.declaration.VariableDeclarationParser
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.declaration.ImportDeclarationParser
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.*
import com.jamieswhiteshirt.util.iterator.ExtIterator
import com.jamieswhiteshirt.util.search.*

object StatementParser : SplitCompileParser<Statement, StatementCompiler>(
        endOfStatementSearch
) {
    private val importDeclarationSearch = SequenceSearch(
            ExactEntry(MatcherSearch(EqualityMatcher<Token>(WordToken("import")))),
            ReluctantEntry(traversalSearch, OneOrMore),
            ExactEntry(MatcherSearch(EqualityMatcher<Token>(WordToken("from")))),
            ExactEntry(MatcherSearch(TypeMatcher(WordToken::class)))
    )
    private val valueDeclarationSearch = SequenceSearch(
            ExactEntry(Union(
                    MatcherSearch(EqualityMatcher<Token>(WordToken("var"))),
                    MatcherSearch(EqualityMatcher<Token>(WordToken("val")))
            )),
            ReluctantEntry(traversalSearch, OneOrMore),
            ExactEntry(MatcherSearch(EqualityMatcher<Token>(OperatorToken("=")))),
            GreedyEntry(traversalSearch, OneOrMore)
    )
    private val emptyDeclarationSearch = SequenceSearch(
            ExactEntry(Union(
                    MatcherSearch(EqualityMatcher<Token>(WordToken("var"))),
                    MatcherSearch(EqualityMatcher<Token>(WordToken("val")))
            )),
            ReluctantEntry(traversalSearch, OneOrMore),
            ExactEntry(IsEnd())
    )
    private val returnSearch = SequenceSearch(
            ExactEntry(MatcherSearch(EqualityMatcher<Token>(WordToken("return")))),
            ExactEntry(IsEnd())
    )
    private val returnValueSearch = SequenceSearch(
            ExactEntry(MatcherSearch(EqualityMatcher<Token>(WordToken("return")))),
            ReluctantEntry(traversalSearch, OneOrMore),
            ExactEntry(IsEnd())
    )
    private val blockSearch = SequenceSearch(
            ExactEntry(curlyBlockSearch),
            ExactEntry(IsEnd())
    )
    private val expressionSearch = SequenceSearch(
            ReluctantEntry(traversalSearch, OneOrMore),
            ExactEntry(IsEnd())
    )

    override fun getCompiler(): StatementCompiler {
        return StatementCompiler()
    }

    override fun getChainReader(compiler: StatementCompiler): SearchChainReader<Token> {
        val chainReader = SearchChainReader<Token>()
        //import declaration statement
        chainReader.appendChainEntry(importDeclarationSearch, fun(result: SearchResult<Token>) : Unit {
            val declarations = ImportDeclarationParser.getAll(result[1].begin, result[1].end)
            val module = (result[3].begin.value as WordToken).value
            compiler.addStatement(ImportDeclarationStatement(declarations, module))
        })
        //variable declaration statement
        chainReader.appendChainEntry(valueDeclarationSearch, fun(result: SearchResult<Token>) : Unit {
            val declarations = VariableDeclarationParser.getAll(result[0].begin, result[1].end)
            val value = ExpressionParser.getOne(result[3].begin, result[3].end)
            compiler.addStatement(ValueVariableDeclarationStatement(declarations, value))
        })
        //empty variable declaration statement
        chainReader.appendChainEntry(emptyDeclarationSearch, fun(result: SearchResult<Token>) : Unit {
            val declarations = VariableDeclarationParser.getAll(result[0].begin, result[1].end)
            compiler.addStatement(EmptyVariableDeclarationStatement(declarations))
        })
        //return statement
        chainReader.appendChainEntry(returnSearch, fun(@Suppress("UNUSED_PARAMETER") result: SearchResult<Token>) : Unit {
            compiler.addStatement(ReturnStatement())
        })
        //return value statement
        chainReader.appendChainEntry(returnValueSearch, fun(result: SearchResult<Token>) : Unit {
            val value = ExpressionParser.getOne(result[1].begin, result[1].end)
            compiler.addStatement(ReturnValueStatement(value))
        })
        //if statement
        chainReader.appendChainEntry(ifSearch, fun(result: SearchResult<Token>) : Unit {
            val condition = ExpressionParser.getOne(result[1].begin + 1, result[1].end - 1)
            val consequent = getAll(result[2].begin + 1, result[2].end - 1)
            compiler.addIf(condition, consequent)
        })
        //else if statement
        chainReader.appendChainEntry(elseIfSearch, fun(result: SearchResult<Token>) : Unit {
            val condition = ExpressionParser.getOne(result[1].begin + 1, result[1].end - 1)
            val consequent = getAll(result[2].begin + 1, result[2].end - 1)
            compiler.addElseIf(condition, consequent)
        })
        //else statement
        chainReader.appendChainEntry(elseSearch, fun(result: SearchResult<Token>) : Unit {
            val consequent = getAll(result[1].begin + 1, result[1].end - 1)
            compiler.addElse(consequent)
        })
        //block statement
        chainReader.appendChainEntry(blockSearch, fun(result: SearchResult<Token>) : Unit {
            val statements = getAll(result.begin + 1, result.end - 1)
            compiler.addStatement(BlockStatement(statements))
        })
        //expression statement
        chainReader.appendChainEntry(expressionSearch, fun(result: SearchResult<Token>) : Unit {
            val expression = ExpressionParser.getOne(result.begin, result.end)
            compiler.addStatement(ExpressionStatement(expression))
        })
        //error
        chainReader.appendChainEntry(IsNotEnd<Token>(), fun(@Suppress("UNUSED_PARAMETER") result: SearchResult<Token>) : Unit {
            throw SyntaxError("Could not parse statement")
        })

        return chainReader
    }
}
