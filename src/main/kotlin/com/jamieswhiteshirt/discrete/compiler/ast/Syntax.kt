package com.jamieswhiteshirt.discrete.compiler.ast

open class SyntaxError : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
}

class UnexpectedTokenError : SyntaxError {
    constructor() : super()
    constructor(message: String) : super(message)
}

class InvalidDeclarationError : SyntaxError {
    constructor() : super()
    constructor(message: String) : super(message)
}

class UnknownIdentifierError : SyntaxError {
    constructor() : super()
    constructor(message: String) : super(message)
}
