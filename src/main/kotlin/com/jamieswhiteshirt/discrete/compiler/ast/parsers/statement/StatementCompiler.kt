package com.jamieswhiteshirt.discrete.compiler.ast.parsers.statement

import com.jamieswhiteshirt.discrete.compiler.ast.expression.*
import com.jamieswhiteshirt.discrete.compiler.builders.BlockBuilder
import com.jamieswhiteshirt.discrete.compiler.builders.BlockHandle
import com.jamieswhiteshirt.discrete.compiler.ast.statement.*
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.Compiler
import com.jamieswhiteshirt.discrete.compiler.ast.statement.Branch
import com.jamieswhiteshirt.discrete.compiler.ast.statement.ConditionalBranch
import com.jamieswhiteshirt.discrete.compiler.ast.statement.UnconditionalBranch
import com.jamieswhiteshirt.util.SplitList
import com.jamieswhiteshirt.util.search.*
import java.util.*

class StatementCompiler : Compiler<Statement>() {
    private class StatementEntry(value: Statement) : NativeEntry<Statement>() {
        override val value = value
    }
    private abstract class BranchingEntry(val consequent: List<Statement>) : Entry()
    private class IfEntry(val condition: Expression, consequentStatements: List<Statement>) : BranchingEntry(consequentStatements) {
        override fun equals(other: Any?) : Boolean {
            if (other is IfEntry) {
                return condition == other.condition && consequent == other.consequent
            }
            return false
        }

        override fun hashCode(): Int{
            return condition.hashCode() + consequent.hashCode()
        }
    }
    private class ElseIfEntry(val condition: Expression, consequent: List<Statement>) : BranchingEntry(consequent) {
        override fun equals(other: Any?) : Boolean {
            if (other is ElseIfEntry) {
                return condition == other.condition && consequent == other.consequent
            }
            return false
        }

        override fun hashCode(): Int{
            return condition.hashCode() + consequent.hashCode()
        }
    }
    private class ElseEntry(consequent: List<Statement>) : BranchingEntry(consequent) {
        override fun equals(other: Any?) : Boolean {
            if (other is ElseEntry) {
                return consequent == other.consequent
            }
            return false
        }

        override fun hashCode(): Int{
            return consequent.hashCode()
        }
    }

    private class BranchL2RTransformer() : L2RTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(IfEntry::class))),
            GreedyEntry(MatcherSearch(TypeMatcher<Entry>(ElseIfEntry::class)), ZeroOrMore),
            GreedyEntry(MatcherSearch(TypeMatcher<Entry>(ElseEntry::class)), OneOrLess)
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val branches = ArrayList<Branch>()
            val ifEntry = result[0].begin.value as IfEntry
            branches.add(ConditionalBranch(ifEntry.condition, ifEntry.consequent))
            for (elseIfResult in result[1]) {
                val elseIfEntry = elseIfResult.begin.value as ElseIfEntry
                branches.add(ConditionalBranch(elseIfEntry.condition, elseIfEntry.consequent))
            }
            if (result[2].children == 1) {
                val elseEntry = result[2].begin.value as ElseEntry
                branches.add(UnconditionalBranch(elseEntry.consequent))
            }
            for (i in 1..result.end - result.begin) {
                entries.middleBackPop()
            }
            entries.middleFrontPush(StatementEntry(BranchingStatement(branches)))
        }
    }

    override fun getTransformers(): List<Transformer> {
        return listOf(BranchL2RTransformer())
    }

    fun addStatement(statement: Statement) {
        add(StatementEntry(statement))
    }
    fun addIf(condition: Expression, consequent: List<Statement>) {
        add(IfEntry(condition, consequent))
    }
    fun addElseIf(condition: Expression, consequent: List<Statement>) {
        add(ElseIfEntry(condition, consequent))
    }
    fun addElse(consequent: List<Statement>) {
        add(ElseEntry(consequent))
    }
}
