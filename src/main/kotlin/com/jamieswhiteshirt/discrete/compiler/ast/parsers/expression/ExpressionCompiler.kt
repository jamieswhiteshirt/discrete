package com.jamieswhiteshirt.discrete.compiler.ast.parsers.expression

import com.jamieswhiteshirt.discrete.compiler.ast.expression.*
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.Compiler
import com.jamieswhiteshirt.discrete.compiler.ast.operators.*
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.OperatorToken
import com.jamieswhiteshirt.util.SplitList
import com.jamieswhiteshirt.util.search.TypeMatcher
import com.jamieswhiteshirt.util.search.*
import java.util.*

class ExpressionCompiler : Compiler<Expression>() {
    private class ExpressionEntry(value: Expression) : NativeEntry<Expression>() {
        override val value = value
    }
    private class BlockEntry(val values: List<Expression>) : Entry() {
        override fun equals(other: Any?) : Boolean {
            if (other is BlockEntry) {
                return values == other.values
            }
            return false
        }

        override fun hashCode(): Int{
            return values.hashCode()
        }
    }
    private class AmbiguousOperatorEntry(val token: OperatorToken) : Entry() {
        override fun equals(other: Any?) : Boolean {
            if (other is AmbiguousOperatorEntry) {
                return token == other.token
            }
            return false
        }

        override fun hashCode(): Int{
            return token.hashCode()
        }
    }
    private class UnaryOperatorEntry(val value: UnaryOperator) : Entry() {
        override fun equals(other: Any?) : Boolean {
            if (other is UnaryOperatorEntry) {
                return value == other.value
            }
            return false
        }

        override fun hashCode(): Int{
            return value.hashCode()
        }
    }
    private class BinaryOperatorEntry(val value: BinaryOperator) : Entry() {
        override fun equals(other: Any?) : Boolean {
            if (other is BinaryOperatorEntry) {
                return value == other.value
            }
            return false
        }

        override fun hashCode(): Int{
            return value.hashCode()
        }
    }
    private abstract class BranchingEntry(val consequent: Expression) : Entry()
    private class IfEntry(val condition: Expression, consequent: Expression) : BranchingEntry(consequent) {
        override fun equals(other: Any?) : Boolean {
            if (other is IfEntry) {
                return condition == other.condition && consequent == other.consequent
            }
            return false
        }

        override fun hashCode(): Int{
            return condition.hashCode() + consequent.hashCode()
        }
    }
    private class ElseIfEntry(val condition: Expression, consequent: Expression) : BranchingEntry(consequent) {
        override fun equals(other: Any?) : Boolean {
            if (other is ElseIfEntry) {
                return condition == other.condition && consequent == other.consequent
            }
            return false
        }

        override fun hashCode(): Int{
            return condition.hashCode() + consequent.hashCode()
        }
    }
    private class ElseEntry(consequent: Expression) : BranchingEntry(consequent) {
        override fun equals(other: Any?) : Boolean {
            if (other is ElseEntry) {
                return consequent == other.consequent
            }
            return false
        }

        override fun hashCode(): Int{
            return consequent.hashCode()
        }
    }

    private class BranchL2RTransformer() : L2RTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(IfEntry::class))),
            GreedyEntry(MatcherSearch(TypeMatcher<Entry>(ElseIfEntry::class)), ZeroOrMore),
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(ElseEntry::class)))
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val branches = ArrayList<ConditionalBranch>()
            val ifEntry = result[0].begin.value as IfEntry
            branches.add(ConditionalBranch(ifEntry.condition, ifEntry.consequent))
            for (elseIfResult in result[1]) {
                val elseIfEntry = elseIfResult.begin.value as ElseIfEntry
                branches.add(ConditionalBranch(elseIfEntry.condition, elseIfEntry.consequent))
            }
            val elseEntry = result[2].begin.value as ElseEntry
            val unconditionalBranch = UnconditionalBranch(elseEntry.consequent)
            for (i in 1..result.end - result.begin) {
                entries.middleBackPop()
            }
            entries.middleFrontPush(ExpressionEntry(BranchingExpression(branches, unconditionalBranch)))
        }
    }

    private class BlockL2RTransformer() : L2RTransformer(SequenceSearch(
            ExactEntry(Union(
                    IsBegin<Entry>(),
                    MatcherSearch(TypeMatcher<Entry>(UnaryOperatorEntry::class)),
                    MatcherSearch(TypeMatcher<Entry>(BinaryOperatorEntry::class))
            )),
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(BlockEntry::class)))
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val a = result[1].begin.value as BlockEntry

            if (a.values.size == 1) {
                entries.middle += result.length
                entries.middleFrontReplace(ExpressionEntry(a.values[0]))
            }
            else {
                entries.middle += result.length
            }
        }
    }

    private class FunctionCallL2RTransformer() : L2RTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(ExpressionEntry::class))),
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(BlockEntry::class)))
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val a = result[0].begin.value as ExpressionEntry
            val b = result[1].begin.value as BlockEntry
            entries.middleBackPop()
            entries.middleBackReplace(ExpressionEntry(InvocationExpression(a.value, b.values)))
        }
    }

    private class AmbiguousPostUnaryOperatorL2RTransformer(val operators: Map<String, UnaryOperator>) : L2RTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(ExpressionEntry::class))),
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(AmbiguousOperatorEntry::class))),
            ExactEntry(Union(
                    IsEnd<Entry>(),
                    MatcherSearch(TypeMatcher<Entry>(UnaryOperatorEntry::class)),
                    MatcherSearch(TypeMatcher<Entry>(BinaryOperatorEntry::class))
            ))
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val a = result[0].begin.value as ExpressionEntry
            val o = result[1].begin.value as AmbiguousOperatorEntry
            val operator = operators[o.token.value]
            if (operator is PostUnaryOperator) {
                entries.middleBackPop()
                entries.middleBackReplace(ExpressionEntry(PostUnaryOperatorExpression.factory(a.value, operator)))
            }
        }
    }

    private class AmbiguousPreUnaryOperatorL2R2LTransformer(val operators: Map<String, UnaryOperator>) : R2LTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(ExpressionEntry::class))),
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(AmbiguousOperatorEntry::class))),
            ExactEntry(Union(
                    IsEnd<Entry>(),
                    MatcherSearch(TypeMatcher<Entry>(UnaryOperatorEntry::class)),
                    MatcherSearch(TypeMatcher<Entry>(BinaryOperatorEntry::class))
            ))
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val a = result[0].begin.value as ExpressionEntry
            val o = result[1].begin.value as AmbiguousOperatorEntry
            val operator = operators[o.token.value]
            if (operator is PreUnaryOperator) {
                entries.middleBackPop()
                entries.middleBackReplace(ExpressionEntry(PreUnaryOperatorExpression.factory(a.value, operator)))
            }
        }
    }

    private class UnaryOperatorR2LTransformer(vararg operators: PreUnaryOperator) : R2LTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(ExpressionEntry::class))),
            ExactEntry(MatcherSearch(TypeMatcher<Entry>(UnaryOperatorEntry::class)))
    )) {
        private val operators = operators

        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val a = result[0].begin.value as ExpressionEntry
            val o = result[1].begin.value as UnaryOperatorEntry
            val operator = o.value
            if (operator in operators) {
                entries.middleBackPop()
                entries.middleBackReplace(ExpressionEntry(PreUnaryOperatorExpression.factory(a.value, o.value as PreUnaryOperator)))
            }
            else {
                entries.middle++
            }
        }
    }

    private class SubtractionOperatorL2RTransformer() : L2RTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher(ExpressionEntry::class))),
            ExactEntry(MatcherSearch(EqualityMatcher<Entry>(AmbiguousOperatorEntry(OperatorToken("-"))))),
            ExactEntry(MatcherSearch(TypeMatcher(ExpressionEntry::class)))
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            entries.middle += 2
            entries.middleFrontReplace(BinaryOperatorEntry(SubtractionOperator))
        }
    }

    private class BinaryOperatorL2RTransformer(vararg val operators: BinaryOperator) : L2RTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher(ExpressionEntry::class))),
            ExactEntry(MatcherSearch(TypeMatcher(BinaryOperatorEntry::class))),
            ExactEntry(MatcherSearch(TypeMatcher(ExpressionEntry::class)))
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val o = result[1].begin.value as BinaryOperatorEntry
            val operator = o.value
            if (operator in operators) {
                val a = result[0].begin.value as ExpressionEntry
                val b = result[2].begin.value as ExpressionEntry
                entries.middleBackPop(2)
                entries.middleBackReplace(ExpressionEntry(BinaryOperatorExpression.factory(a.value, b.value, operator)))
            }
            else {
                entries.middle++
            }
        }
    }

    private class BinaryOperatorR2LTransformer(vararg val operators: BinaryOperator) : R2LTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(TypeMatcher(ExpressionEntry::class))),
            ExactEntry(MatcherSearch(TypeMatcher(BinaryOperatorEntry::class))),
            ExactEntry(MatcherSearch(TypeMatcher(ExpressionEntry::class)))
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val o = result[1].begin.value as BinaryOperatorEntry
            val operator = o.value
            if (operator in operators) {
                val b = result[0].begin.value as ExpressionEntry
                val a = result[2].begin.value as ExpressionEntry
                entries.middleBackPop(2)
                entries.middleBackReplace(ExpressionEntry(BinaryOperatorExpression.factory(a.value, b.value, operator)))
            }
            else {
                entries.middle++
            }
        }
    }

    override fun getTransformers(): List<Transformer> {
        return listOf(
                BranchL2RTransformer(),
                BlockL2RTransformer(),
                FunctionCallL2RTransformer(),
                AmbiguousPostUnaryOperatorL2RTransformer(mapOf(
                        "++" to PostIncrementOperator,
                        "--" to PostDecrementOperator
                )),
                AmbiguousPreUnaryOperatorL2R2LTransformer(mapOf(
                        "++" to PreIncrementOperator,
                        "--" to PreDecrementOperator,
                        "-" to NegationOperator
                )),
                UnaryOperatorR2LTransformer(BitwiseNotOperator, LogicalNotOperator, NegationOperator),
                SubtractionOperatorL2RTransformer(),
                BinaryOperatorL2RTransformer(MultiplicationOperator, DivisionOperator, FloorDivisionOperator, RemainderOperator),
                BinaryOperatorL2RTransformer(AdditionOperator, SubtractionOperator),
                BinaryOperatorL2RTransformer(LeftShiftOperator, RightShiftOperator),
                BinaryOperatorL2RTransformer(LessOperator, LessOrEqualOperator, GreaterOperator, GreaterOrEqualOperator),
                BinaryOperatorL2RTransformer(IsOperator, IsNotOperator, InOperator, NotInOperator),
                BinaryOperatorL2RTransformer(EqualityOperator, InequalityOperator),
                BinaryOperatorL2RTransformer(BitwiseAndOperator),
                BinaryOperatorL2RTransformer(BitwiseXorOperator),
                BinaryOperatorL2RTransformer(BitwiseOrOperator),
                BinaryOperatorL2RTransformer(LogicalAndOperator, ShortCircuitLogicalAndOperator),
                BinaryOperatorL2RTransformer(LogicalXorOperator),
                BinaryOperatorL2RTransformer(LogicalOrOperator, ShortCircuitLogicalOrOperator),
                BinaryOperatorL2RTransformer(ConcatenationOperator),
                BinaryOperatorR2LTransformer(AssignOperator),
                BinaryOperatorR2LTransformer(MultiplicationCompoundAssignOperator, DivisionCompoundAssignOperator, FloorDivisionCompoundAssignOperator, RemainderCompoundAssignOperator),
                BinaryOperatorR2LTransformer(AdditionCompoundAssignOperator, SubtractionCompoundAssignOperator),
                BinaryOperatorR2LTransformer(LeftShiftCompoundAssignOperator, RightShiftCompoundAssignOperator),
                BinaryOperatorR2LTransformer(BitwiseAndCompoundAssignOperator),
                BinaryOperatorR2LTransformer(BitwiseXorCompoundAssignOperator),
                BinaryOperatorR2LTransformer(BitwiseOrCompoundAssignOperator),
                BinaryOperatorR2LTransformer(LogicalAndCompoundAssignOperator, ShortCircuitLogicalAndCompoundAssignOperator),
                BinaryOperatorR2LTransformer(LogicalXorCompoundAssignOperator),
                BinaryOperatorR2LTransformer(LogicalOrCompoundAssignOperator, ShortCircuitLogicalOrCompoundAssignOperator),
                BinaryOperatorR2LTransformer(ConcatenationCompoundAssignOperator)
        )
    }

    fun addExpression(expression: Expression) {
        add(ExpressionEntry(expression))
    }
    fun addBlock(expressions: List<Expression>) {
        add(BlockEntry(expressions))
    }
    fun addAmbiguousOperator(token: OperatorToken) {
        add(AmbiguousOperatorEntry(token))
    }
    fun addUnaryOperator(operator: UnaryOperator) {
        add(UnaryOperatorEntry(operator))
    }
    fun addBinaryOperator(operator: BinaryOperator) {
        add(BinaryOperatorEntry(operator))
    }
    fun addIf(condition: Expression, consequent: Expression) {
        add(IfEntry(condition, consequent))
    }
    fun addElseIf(condition: Expression, consequent: Expression) {
        add(ElseIfEntry(condition, consequent))
    }
    fun addElse(consequent: Expression) {
        add(ElseEntry(consequent))
    }
}

