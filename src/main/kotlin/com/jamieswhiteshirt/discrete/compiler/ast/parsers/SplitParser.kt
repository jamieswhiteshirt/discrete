package com.jamieswhiteshirt.discrete.compiler.ast.parsers

import com.jamieswhiteshirt.discrete.compiler.ast.SyntaxError
import com.jamieswhiteshirt.discrete.compiler.builders.ScopeBuilder
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.Token
import com.jamieswhiteshirt.util.iterator.ExtIterator
import com.jamieswhiteshirt.util.search.Search
import com.jamieswhiteshirt.util.search.SearchChainReader
import java.util.*

/**
 * Iterates a list of tokens and parses it into either a list of items or a single item
 * The list is split up by splitSearch
 */
abstract class SplitParser<T>(val splitSearch: Search<Token>) : Parser<T>() {
    abstract fun getChainReader(list: MutableList<T>): SearchChainReader<Token>

    override fun getAll(begin: ExtIterator<Token>, end: ExtIterator<Token>): List<T> {
        val list = LinkedList<T>()
        val chainReader = getChainReader(list)
        var iter = begin.clone()
        while (iter < end) {
            val res = splitSearch.search(iter, end)

            if (res != null) {
                chainReader.read(res[0].begin, res[0].end)
                iter = res.end
            }
            else {
                break
            }
        }

        return list
    }

    override fun getOne(begin: ExtIterator<Token>, end: ExtIterator<Token>): T {
        val list = getAll(begin, end)
        if (list.size == 1) {
            return list[0]
        }
        else {
            throw SyntaxError("Expected one, got ${list.size}")
        }
    }
}
