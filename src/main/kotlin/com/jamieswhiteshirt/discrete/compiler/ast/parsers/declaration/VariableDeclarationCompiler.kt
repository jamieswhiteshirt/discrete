package com.jamieswhiteshirt.discrete.compiler.ast.parsers.declaration

import com.jamieswhiteshirt.discrete.common.code.VariableMutability
import com.jamieswhiteshirt.discrete.compiler.ast.declaration.Declaration
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.Compiler
import com.jamieswhiteshirt.util.SplitList
import com.jamieswhiteshirt.util.iterator.copyRange
import com.jamieswhiteshirt.util.search.*

class VariableDeclarationCompiler : Compiler<Declaration.Variable>() {
    private class DeclarationEntry<T>(override val value: T) : NativeEntry<T>()
    private class IdentifierEntry(val identifier: String) : Entry() {
        override fun equals(other: Any?): Boolean {
            if (other is IdentifierEntry) {
                return identifier == other.identifier
            }
            return false
        }
        override fun hashCode(): Int = identifier.hashCode()
    }
    private class MutabilityEntry(val mutability: VariableMutability) : Entry() {
        override fun equals(other: Any?): Boolean {
            if (other is MutabilityEntry) {
                return mutability == other.mutability
            }
            return false
        }
        override fun hashCode(): Int = mutability.hashCode()
    }

    private class IdentifierL2RTransformer : L2RTransformer(SequenceSearch(
            ExactEntry(MatcherSearch(OrMatcher(
                    TypeMatcher<Entry>(MutabilityEntry::class)
            ))),
            GreedyEntry(MatcherSearch(TypeMatcher<Entry>(IdentifierEntry::class)), OneOrMore)
    )) {
        override fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>) {
            val mutabilityEntry = result[0].begin.value as MutabilityEntry
            val stringIdentifierEntries = copyRange(result[1].begin, result[1].end)
            entries.middleBackPop(result.length)
            for (entry in stringIdentifierEntries) {
                val identifierEntry = entry as IdentifierEntry
                entries.middleFrontPush(DeclarationEntry(Declaration.Variable(identifierEntry.identifier, mutabilityEntry.mutability)))
            }
        }
    }

    override fun getTransformers(): List<Transformer> {
        return listOf(
                IdentifierL2RTransformer()
        )
    }

    fun addVar() {
        add(MutabilityEntry(VariableMutability.Var))
    }

    fun addVal() {
        add(MutabilityEntry(VariableMutability.Val))
    }

    fun addIdentifier(identifier: String) {
        add(IdentifierEntry(identifier))
    }
}
