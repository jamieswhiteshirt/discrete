package com.jamieswhiteshirt.discrete.compiler.ast.expression

import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.compiler.ast.UnknownIdentifierError
import com.jamieswhiteshirt.discrete.compiler.ast.declaration.Declaration
import com.jamieswhiteshirt.discrete.compiler.ast.expression.stringliteral.StringLiteralDecoder
import com.jamieswhiteshirt.discrete.compiler.ast.expression.stringliteral.StringLiteralEncoder
import com.jamieswhiteshirt.discrete.compiler.builders.*
import com.jamieswhiteshirt.discrete.compiler.ast.operators.*
import com.jamieswhiteshirt.discrete.compiler.ast.statement.Statement

abstract class Expression() {
    abstract override fun toString(): String

    abstract fun evaluate(builder: ScopeBuilder)

    abstract fun evaluateReturn(builder: ScopeBuilder)
}

class ExplicitMappingExpression(val mappings: List<Pair<Expression, Expression>>) : Expression() {
    override fun toString(): String = "[${mappings.map { "${it.first} to ${it.second}" }.joinToString(", ")}]"

    override fun evaluate(builder: ScopeBuilder) {
        for (mapping in mappings) {
            mapping.first.evaluate(builder)
            mapping.second.evaluate(builder)
        }
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        for (mapping in mappings) {
            mapping.first.evaluateReturn(builder)
            mapping.second.evaluateReturn(builder)
        }
        builder.addInstruction(Instruction.NEW_EXPLICIT_MAPPING(mappings.size))
    }
}
class ImplicitMappingExpression(val values: List<Expression>) : Expression() {
    override fun toString(): String = "[${values.joinToString(", ")}]"

    override fun evaluate(builder: ScopeBuilder) {
        for (expression in values) {
            expression.evaluate(builder)
        }
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        for (expression in values) {
            expression.evaluateReturn(builder)
        }
        builder.addInstruction(Instruction.NEW_IMPLICIT_MAPPING(values.size))
    }
}
class SetExpression(val values: List<Expression>) : Expression() {
    override fun toString(): String = "{${values.joinToString(", ")}}"

    override fun evaluate(builder: ScopeBuilder) {
        for (expression in values) {
            expression.evaluate(builder)
        }
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        for (expression in values) {
            expression.evaluateReturn(builder)
        }
        builder.addInstruction(Instruction.NEW_SET(values.size))
    }
}
class TupleExpression(val values: List<Expression>) : Expression() {
    override fun toString(): String = "(${values.joinToString(", ")},)"

    override fun evaluate(builder: ScopeBuilder) {
        for (expression in values) {
            expression.evaluate(builder)
        }
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        for (expression in values) {
            expression.evaluateReturn(builder)
        }
        builder.addInstruction(Instruction.NEW_TUPLE(values.size))
    }
}
class FunctionExpression(val parameterDeclarations: List<Declaration.Variable>, val statements: List<Statement>) : Expression() {
    override fun toString(): String = "(${parameterDeclarations.joinToString(", ")}){\n${statements.joinToString("\n").prependIndent("    ")}\n}"

    override fun evaluate(builder: ScopeBuilder) {
        FunctionBuilder(builder, parameterDeclarations, statements)
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        builder.addInstruction(builder.addFunction(FunctionBuilder(builder, parameterDeclarations, statements)).newInstruction)
    }
}

abstract class LiteralExpression() : Expression() {
    override fun evaluate(builder: ScopeBuilder) { }
}
class FloatLiteralExpression(strValue: String) : LiteralExpression() {
    val value = strValue.toFloat()

    override fun toString() : String = value.toString()

    override fun evaluateReturn(builder: ScopeBuilder) {
        builder.addInstruction(Instruction.PUSH_FLOAT(value))
    }
}
class IntLiteralExpression(strValue: String) : LiteralExpression() {
    val value = strValue.toInt()

    override fun toString() : String = value.toString()

    override fun evaluateReturn(builder: ScopeBuilder) {
        builder.addInstruction(Instruction.PUSH_INT(value))
    }
}
class StringLiteralExpression(strValue: String) : LiteralExpression() {
    val value = StringLiteralDecoder.read(strValue)

    override fun toString() : String = "\"${StringLiteralEncoder.read(value)}\""

    override fun evaluateReturn(builder: ScopeBuilder) {
        builder.addInstruction(Instruction.PUSH_STRING(value))
    }
}

class IdentityExpression() : LiteralExpression() {
    override fun toString(): String = "?"

    override fun evaluateReturn(builder: ScopeBuilder) {
        builder.addInstruction(Instruction.NEW_IDENTITY)
    }
}

class GetDeclarationExpression(val identifier: String) : Expression() {
    override fun toString() : String = identifier

    override fun evaluate(builder: ScopeBuilder) {
        if (builder.getDeclaration(identifier) == null) {
            throw UnknownIdentifierError("Unknown identifier \"$identifier\"")
        }
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        val declaration = builder.getDeclaration(identifier)
        if (declaration != null) {
            builder.addInstruction(declaration.pushInstruction)
        }
        else {
            throw UnknownIdentifierError("Unknown identifier \"$identifier\"")
        }
    }
}

abstract class BinaryOperatorExpression(val a: Expression, val b: Expression) : Expression() {
    abstract val o: BinaryOperator

    companion object {
        fun factory(a: Expression, b: Expression, o: BinaryOperator) : BinaryOperatorExpression {
            when (o) {
                is AssignOperator -> return AssignmentOperatorExpression(a, b)
                is EvaluateBinaryOperator -> return EvaluateBinaryOperatorExpression.factory(a, b, o)
                is CompoundAssignBinaryOperator -> return CompoundAssignBinaryOperatorExpression.factory(a, b, o)
            }
            throw Exception("Invalid operator")
        }
    }

    override fun toString(): String = "$a $o $b"
}

class AssignmentOperatorExpression(a: Expression, b: Expression) : BinaryOperatorExpression(a, b) {
    override val o: AssignOperator = AssignOperator

    override fun evaluate(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        b.evaluateReturn(builder)
        builder.addInstruction(Instruction.ASSIGN)
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(Instruction.DUP)
        b.evaluateReturn(builder)
        builder.addInstruction(Instruction.ASSIGN)
    }
}

abstract class EvaluateBinaryOperatorExpression(a: Expression, b: Expression) : BinaryOperatorExpression(a, b) {
    override abstract val o: EvaluateBinaryOperator

    companion object {
        fun factory(a: Expression, b: Expression, o: EvaluateBinaryOperator) : EvaluateBinaryOperatorExpression {
            when (o) {
                is ArithmeticLogicEvaluateBinaryOperator -> return ArithmeticLogicBinaryEvaluateOperatorExpression(a, b, o)
                is ShortCircuitLogicEvaluateBinaryOperator -> return ShortCircuitLogicEvaluateBinaryOperatorExpression(a, b, o)
            }
            throw Exception("Invalid operator")
        }
    }
}

class ArithmeticLogicBinaryEvaluateOperatorExpression(a: Expression, b: Expression, override val o: ArithmeticLogicEvaluateBinaryOperator) : EvaluateBinaryOperatorExpression(a, b) {
    override fun evaluate(builder: ScopeBuilder) {
        a.evaluate(builder)
        b.evaluate(builder)
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        b.evaluateReturn(builder)
        builder.addInstruction(o.instruction)
    }
}

class ShortCircuitLogicEvaluateBinaryOperatorExpression(a: Expression, b: Expression, override val o: ShortCircuitLogicEvaluateBinaryOperator) : EvaluateBinaryOperatorExpression(a, b) {
    override fun evaluate(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.withLabel { label ->
            builder.addInstruction(o.shortCircuitInstruction(label))
            b.evaluate(builder)
        }
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(Instruction.DUP)
        builder.withLabel { label ->
            builder.addInstruction(o.shortCircuitInstruction(label))
            builder.addInstruction(Instruction.POP)
            b.evaluateReturn(builder)
        }
    }
}

abstract class CompoundAssignBinaryOperatorExpression(a: Expression, b: Expression) : BinaryOperatorExpression(a, b) {
    override abstract val o: CompoundAssignBinaryOperator

    companion object {
        fun factory(a: Expression, b: Expression, o: CompoundAssignBinaryOperator) : CompoundAssignBinaryOperatorExpression {
            when (o) {
                is ArithmeticLogicCompoundAssignBinaryOperator -> return ArithmeticLogicCompoundAssignBinaryOperatorExpression(a, b, o)
                is ShortCircuitLogicCompoundAssignBinaryOperator -> return ShortCircuitLogicCompoundAssignBinaryOperatorExpression(a, b, o)
            }
            throw Exception("Invalid operator")
        }
    }
}

class ArithmeticLogicCompoundAssignBinaryOperatorExpression(a: Expression, b: Expression, override val o: CompoundAssignBinaryOperator) : CompoundAssignBinaryOperatorExpression(a, b) {
    override fun evaluate(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(Instruction.DUP)
        b.evaluateReturn(builder)
        builder.addInstruction(o.instruction)
        builder.addInstruction(Instruction.ASSIGN)
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(Instruction.DUP)
        builder.addInstruction(Instruction.DUP)
        b.evaluateReturn(builder)
        builder.addInstruction(o.instruction)
        builder.addInstruction(Instruction.ASSIGN)
    }
}

class ShortCircuitLogicCompoundAssignBinaryOperatorExpression(a: Expression, b: Expression, override val o: ShortCircuitLogicCompoundAssignBinaryOperator) : CompoundAssignBinaryOperatorExpression(a, b) {
    override fun evaluate(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(Instruction.DUP)
        builder.withLabel { label ->
            builder.addInstruction(o.shortCircuitInstruction(label))
            builder.addInstruction(Instruction.DUP)
            b.evaluateReturn(builder)
            builder.addInstruction(Instruction.ASSIGN)
        }
        builder.addInstruction(Instruction.POP)
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(Instruction.DUP)
        builder.withLabel { label ->
            builder.addInstruction(o.shortCircuitInstruction(label))
            builder.addInstruction(Instruction.DUP)
            b.evaluateReturn(builder)
            builder.addInstruction(Instruction.ASSIGN)
        }
    }
}

abstract class UnaryOperatorExpression(val a: Expression) : Expression() {
    abstract val o: UnaryOperator
}

abstract class PreUnaryOperatorExpression(a: Expression) : UnaryOperatorExpression(a) {
    override abstract val o: PreUnaryOperator

    companion object {
        fun factory(a: Expression, o: PreUnaryOperator) : PreUnaryOperatorExpression {
            when (o) {
                is ArithmeticLogicEvaluatePreUnaryOperator -> return ArithmeticLogicPreUnaryOperatorExpression(a, o)
                is ArithmeticLogicCompoundAssignPreUnaryOperator -> return CompoundAssignmentPreUnaryOperatorExpression(a, o)
            }
            throw Exception("Invalid operator")
        }
    }

    override fun toString(): String = "$o$a"
}

class ArithmeticLogicPreUnaryOperatorExpression(a: Expression, override val o: ArithmeticLogicEvaluatePreUnaryOperator) : PreUnaryOperatorExpression(a) {
    override fun evaluate(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(o.instruction)
        builder.addInstruction(Instruction.POP)
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(o.instruction)
    }
}

class CompoundAssignmentPreUnaryOperatorExpression(a: Expression, override val o: ArithmeticLogicCompoundAssignPreUnaryOperator) : PreUnaryOperatorExpression(a) {
    override fun evaluate(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(o.instruction)
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(Instruction.DUP)
        builder.addInstruction(o.instruction)
    }
}

abstract class PostUnaryOperatorExpression(a: Expression) : UnaryOperatorExpression(a) {
    override abstract val o: PostUnaryOperator

    companion object {
        fun factory(a: Expression, o: PostUnaryOperator) : PostUnaryOperatorExpression {
            when (o) {
                is ArithmeticLogicCompoundAssignPostUnaryOperator -> return CompoundAssignmentPostUnaryOperatorExpression(a, o)
            }
            throw Exception("Invalid operator")
        }
    }

    override fun toString(): String = "$a$o"
}

class CompoundAssignmentPostUnaryOperatorExpression(a: Expression, override val o: ArithmeticLogicCompoundAssignPostUnaryOperator) : PostUnaryOperatorExpression(a) {
    override fun evaluate(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(o.instruction)
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        a.evaluateReturn(builder)
        builder.addInstruction(Instruction.DUP)
        builder.addInstruction(Instruction.AS_IMMUTABLE)
        builder.addInstruction(Instruction.SWAP)
        builder.addInstruction(o.instruction)
    }
}

class InvocationExpression(val invokable: Expression, val params: List<Expression>) : Expression() {
    override fun toString(): String = "$invokable(${params.joinToString(", ")})"

    override fun evaluate(builder: ScopeBuilder) {
        invokable.evaluateReturn(builder)
        for (param in params) {
            param.evaluateReturn(builder)
        }
        builder.addInstruction(Instruction.INVOKE(params.size))
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        invokable.evaluateReturn(builder)
        for (param in params) {
            param.evaluateReturn(builder)
        }
        builder.addInstruction(Instruction.INVOKE_RETURN(params.size))
    }
}

class BranchingExpression(val branches: List<ConditionalBranch>, val unconditionalBranch: UnconditionalBranch) : Expression() {
    override fun toString() : String = "${branches.joinToString("\nelse ")};"

    override fun evaluate(builder: ScopeBuilder) {
        builder.withLabel { lastLabel ->
            for (branch in branches) {
                branch.evaluate(builder, lastLabel)
            }
            unconditionalBranch.evaluateLast(builder)
        }
    }

    override fun evaluateReturn(builder: ScopeBuilder) {
        builder.withLabel { lastLabel ->
            for (branch in branches) {
                branch.evaluateReturn(builder, lastLabel)
            }
            unconditionalBranch.evaluateReturnLast(builder)
        }
    }
}
