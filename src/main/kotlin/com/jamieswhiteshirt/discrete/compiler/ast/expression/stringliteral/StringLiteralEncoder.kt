package com.jamieswhiteshirt.discrete.compiler.ast.expression.stringliteral

import com.jamieswhiteshirt.util.RegexChainReader
import kotlin.text.MatchResult
import kotlin.text.Regex

object StringLiteralEncoder {
    private lateinit var result: StringBuilder

    private val regexChainReader = RegexChainReader()

    init {
        regexChainReader.appendChainEntry(Regex("^\\n"), fun(@Suppress("UNUSED_PARAMETER") match: MatchResult) : Unit {
            result.append("\\n")
        })
        regexChainReader.appendChainEntry(Regex("^\\r"), fun(@Suppress("UNUSED_PARAMETER") match: MatchResult) : Unit {
            result.append("\\r")
        })
        regexChainReader.appendChainEntry(Regex("^\\t"), fun(@Suppress("UNUSED_PARAMETER") match: MatchResult) : Unit {
            result.append("\\t")
        })
        regexChainReader.appendChainEntry(Regex("^\\\""), fun(@Suppress("UNUSED_PARAMETER") match: MatchResult) : Unit {
            result.append("\\\"")
        })
        regexChainReader.appendChainEntry(Regex("^."), fun(match: MatchResult) : Unit {
            result.append(match.value)
        })
    }

    fun read(stringLiteral: String) : String {
        result = StringBuilder()
        regexChainReader.read(stringLiteral.substring(1, stringLiteral.length - 1))
        return result.toString()
    }
}
