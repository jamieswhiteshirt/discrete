package com.jamieswhiteshirt.discrete.compiler.ast.parsers

import com.jamieswhiteshirt.discrete.compiler.ast.SyntaxError
import com.jamieswhiteshirt.discrete.compiler.builders.DeclarationAccess
import com.jamieswhiteshirt.discrete.compiler.builders.ScopeBuilder
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.Token
import com.jamieswhiteshirt.util.iterator.ExtIterator
import com.jamieswhiteshirt.util.iterator.printRange
import com.jamieswhiteshirt.util.search.Search
import com.jamieswhiteshirt.util.search.SearchChainReader
import java.util.*

/**
 * Iterates a list of tokens and parses it into either a list of items or a single item
 * The list is split up by splitSearch. Each sub-list is expected to produce one item when compiled
 */
abstract class SplitCompileParser<T, C : Compiler<T>>(val splitSearch: Search<Token>) : Parser<T>() {
    abstract fun getCompiler(): C
    abstract fun getChainReader(compiler: C): SearchChainReader<Token>

    override fun getAll(begin: ExtIterator<Token>, end: ExtIterator<Token>): List<T> {
        val compiler = getCompiler()
        val chainReader = getChainReader(compiler)
        val list = LinkedList<T>()
        var iter = begin.clone()
        while (iter < end) {
            val res = splitSearch.search(iter, end)

            if (res != null) {
                chainReader.read(res[0].begin, res[0].end)
                iter = res.end
            }
            else {
                break
            }

            list.add(compiler.compileOne())
        }

        return list
    }

    override fun getOne(begin: ExtIterator<Token>, end: ExtIterator<Token>): T {
        val list = getAll(begin, end)
        if (list.size == 1) {
            return list[0]
        }
        else {
            throw SyntaxError("Expected one, got ${list.size}")
        }
    }
}
