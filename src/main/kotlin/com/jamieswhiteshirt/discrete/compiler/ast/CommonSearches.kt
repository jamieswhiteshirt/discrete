package com.jamieswhiteshirt.discrete.compiler.ast

import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.*
import com.jamieswhiteshirt.util.search.EqualityMatcher
import com.jamieswhiteshirt.util.search.ZeroOrMore
import com.jamieswhiteshirt.util.search.*

val nestSearch = Union(
        SequenceSearch(
                ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('{')))),
                ReluctantEntry(Union<Token>(
                        Recursion(),
                        AnySearch()
                ), ZeroOrMore),
                ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('}'))))
        ),
        SequenceSearch(
                ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('[')))),
                ReluctantEntry(Union(
                        Recursion(),
                        AnySearch()
                ), ZeroOrMore),
                ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken(']'))))
        ),
        SequenceSearch(
                ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('(')))),
                ReluctantEntry(Union(
                        Recursion(),
                        AnySearch()
                ), ZeroOrMore),
                ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken(')'))))
        )
).recurse()
val traversalSearch = Union(
        nestSearch,
        AnySearch()
)
val curlyBlockSearch = SequenceSearch(
        ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('{')))),
        ReluctantEntry(traversalSearch, ZeroOrMore),
        ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('}'))))
)
val emptyCurlyBlockSearch = MatcherSearch<Token>(EqualityMatcher(SymbolToken('{')), EqualityMatcher(SymbolToken('}')))
val squareBlockSearch = SequenceSearch(
        ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('[')))),
        ReluctantEntry(traversalSearch, ZeroOrMore),
        ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken(']'))))
)
val emptySquareBlockSearch = MatcherSearch<Token>(EqualityMatcher(SymbolToken('[')), EqualityMatcher(SymbolToken(']')))
val roundBlockSearch = SequenceSearch(
        ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken('(')))),
        ReluctantEntry(traversalSearch, ZeroOrMore),
        ExactEntry(MatcherSearch<Token>(EqualityMatcher(SymbolToken(')'))))
)
val emptyRoundBlockSearch = MatcherSearch<Token>(EqualityMatcher(SymbolToken('(')), EqualityMatcher(SymbolToken(')')))
val ifSearch = SequenceSearch(
        ExactEntry(MatcherSearch(EqualityMatcher<Token>(WordToken("if")))),
        ExactEntry(roundBlockSearch),
        ExactEntry(curlyBlockSearch)
)
val elseIfSearch = SequenceSearch(
        ExactEntry(MatcherSearch(
                EqualityMatcher<Token>(WordToken("else")),
                EqualityMatcher<Token>(WordToken("if"))
        )),
        ExactEntry(roundBlockSearch),
        ExactEntry(curlyBlockSearch)
)
val elseSearch = SequenceSearch(
        ExactEntry(MatcherSearch(EqualityMatcher<Token>(WordToken("else")))),
        ExactEntry(curlyBlockSearch)
)
val endOfStatementSearch = SequenceSearch(
        ReluctantEntry(traversalSearch, ZeroOrMore),
        ExactEntry(Union(
                MatcherSearch<Token>(EqualityMatcher(EndOfStatementToken())),
                IsEnd()
        ))
)
val endOfExpressionSearch = SequenceSearch(
        ReluctantEntry(traversalSearch, ZeroOrMore),
        ExactEntry(Union(
                MatcherSearch<Token>(EqualityMatcher(SeparatorToken())),
                IsEnd()
        ))
)
