package com.jamieswhiteshirt.discrete.compiler.ast.parsers

import com.jamieswhiteshirt.util.SplitList
import com.jamieswhiteshirt.util.iterator.begin
import com.jamieswhiteshirt.util.iterator.end
import com.jamieswhiteshirt.util.search.*
import java.util.*

/**
 * Contains a list of items of various types
 * One item type is native, meaning that the compiler will only output items of this type
 */
abstract class Compiler<T> {
    abstract class Entry

    abstract class NativeEntry<T>() : Entry() {
        abstract val value: T
        override fun equals(other: Any?) : Boolean {
            if (other is NativeEntry<*>) {
                return value == other.value
            }
            return false
        }

        override fun hashCode(): Int{
            return value?.hashCode() ?: 0
        }
    }

    abstract class Transformer {
        abstract fun transform(entries: SplitList<Entry>) : SplitList<Entry>
    }

    abstract class L2RTransformer(val search: Search<Entry>) : Transformer() {
        override fun transform(entries: SplitList<Entry>) : SplitList<Entry> {
            entries.middle = 0
            var begin = entries.begin
            var end = entries.end
            var iter = begin.clone()
            while (iter < end) {
                val result = search.search(begin, iter, end)
                if (result != null) {
                    entries.middle = iter - begin
                    handleResult(result, entries)
                    iter = begin + entries.middle
                }
                else {
                    iter++
                }
            }

            return entries
        }

        abstract fun handleResult(result: SearchResult<Entry>, entries: SplitList<Entry>)
    }

    abstract class R2LTransformer(search: Search<Entry>) : L2RTransformer(search) {
        override fun transform(entries: SplitList<Entry>) : SplitList<Entry> {
            return super.transform(entries.reversed()).reversed()
        }
    }

    abstract protected fun getTransformers(): List<Transformer>

    private var entries = SplitList<Entry>()

    val length: Int
        get() = entries.size

    protected fun add(entry: Entry) {
        entries.add(entry)
    }

    fun compileOne() : T {
        for (transformer in getTransformers()) {
            entries = transformer.transform(entries)
        }

        if (length == 1) {
            val finalEntry = entries.last()
            if (finalEntry is NativeEntry<*>) {
                entries.clear()
                @Suppress("UNCHECKED_CAST") return finalEntry.value as T
            }
            else {
                throw IntermediateValueError("Got an intermediate value after compiling")
            }
        }
        else {
            throw InvalidAmountError("Expected one remaining entry, got $length")
        }
    }

    fun compile() : List<T> {
        for (transformer in getTransformers()) {
            entries = transformer.transform(entries)
        }

        val compiledValues = ArrayList<T>()
        for (entry in entries) {
            if (entry is NativeEntry<*>) {
                @Suppress("UNCHECKED_CAST") compiledValues.add(entry.value as T)
            }
            else {
                throw IntermediateValueError("Got an intermediate value after compiling")
            }
        }

        entries.clear()
        return compiledValues
    }
}
