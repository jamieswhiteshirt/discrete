package com.jamieswhiteshirt.discrete.compiler.ast.parsers

import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.Token
import com.jamieswhiteshirt.util.iterator.ExtIterator

/**
 * Iterates a list of tokens and parses it into either a list of items or a single item
 */
abstract class Parser<T> {
    abstract fun getAll(begin: ExtIterator<Token>, end: ExtIterator<Token>): List<T>

    abstract fun getOne(begin: ExtIterator<Token>, end: ExtIterator<Token>): T
}
