package com.jamieswhiteshirt.discrete.compiler.instruction

import com.jamieswhiteshirt.discrete.common.instruction.Instruction

abstract class AbstractInstructionProvider {
    abstract fun get(instructions: MutableList<Instruction>)
}

abstract class AbstractPushInstructionProvider : AbstractInstructionProvider() {
    fun thenPop() : InstructionProvider {
        return object : InstructionProvider() {
            override fun get(instructions: MutableList<Instruction>) {
                this@AbstractPushInstructionProvider.get(instructions)
                instructions.add(Instruction.POP)
            }
        }
    }

    fun thenConsume(consumeInstructionProvider: ConsumeInstructionProvider) : InstructionProvider {
        return object : InstructionProvider() {
            override fun get(instructions: MutableList<Instruction>) {
                this@AbstractPushInstructionProvider.get(instructions)
                consumeInstructionProvider.get(instructions)
            }
        }
    }
}

abstract class InstructionProvider : AbstractInstructionProvider()

abstract class ConsumeInstructionProvider : AbstractInstructionProvider()

abstract class PushInstructionProvider : AbstractPushInstructionProvider()

abstract class ConditionInstructionProvider : AbstractInstructionProvider()

abstract class ConditionPushInstructionProvider : AbstractInstructionProvider()
