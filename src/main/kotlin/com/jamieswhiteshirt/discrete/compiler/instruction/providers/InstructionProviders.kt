package com.jamieswhiteshirt.discrete.compiler.instruction.providers

import com.jamieswhiteshirt.discrete.common.instruction.Instruction
import com.jamieswhiteshirt.discrete.compiler.ast.declaration.Declaration
import com.jamieswhiteshirt.discrete.compiler.builders.DeclarationHandle
import com.jamieswhiteshirt.discrete.compiler.builders.BlockHandle
import com.jamieswhiteshirt.discrete.compiler.builders.FunctionHandle
import com.jamieswhiteshirt.discrete.compiler.ast.operators.ShortCircuitLogicCompoundAssignBinaryOperator
import com.jamieswhiteshirt.discrete.compiler.ast.operators.ShortCircuitLogicEvaluateBinaryOperator
import com.jamieswhiteshirt.discrete.compiler.instruction.*

/*object NoInstructionProvider : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) { }
}

class ValueDeclareVariableInstructionProvider(val handles: List<DeclarationHandle.Variable>, val value: PushInstructionProvider) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        value.get(instructions)
        if(handles.size > 0) {
            for (handle in handles) {
                if (handle !== handles.last()) {
                    instructions.add(Instruction.DUP)
                }
                instructions.add(handle.declareInstruction)
            }
        }
        else {
            instructions.add(Instruction.POP)
        }
    }
}

class PushDeclarationInstructionProvider(val handle: DeclarationHandle) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        instructions.add(handle.pushInstruction)
    }
}

class ExecuteBlockInstructionProvider(val handle: BlockHandle) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        instructions.add(handle.executeInstruction)
    }
}

class ReturnInstructionProvider() : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        instructions.add(Instruction.RETURN)
    }
}

class ReturnValueInstructionProvider(val value: PushInstructionProvider) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        value.get(instructions)
        instructions.add(Instruction.RETURN_VALUE)
    }
}

abstract class BranchInstructionProvider(val consequent: InstructionProvider, val end: Int, val last: Int) : ConditionInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        consequent.get(instructions)
        instructions.add(Instruction.JUMP(last))
        instructions.add(Instruction.LABEL(end))
    }
}

class ConditionalBranchInstructionProvider(val condition: PushInstructionProvider, consequent: InstructionProvider, end: Int, last: Int): BranchInstructionProvider(consequent, end, last) {
    override fun get(instructions: MutableList<Instruction>) {
        condition.get(instructions)
        instructions.add(Instruction.JUMP_IF_FALSE(end))
        super.get(instructions)
    }
}

class UnconditionalBranchInstructionProvider(consequent: InstructionProvider, end: Int, last: Int) : BranchInstructionProvider(consequent, end, last)

abstract class BranchReturnInstructionProvider(val consequent: PushInstructionProvider, val end: Int, val last: Int) : ConditionPushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        consequent.get(instructions)
        instructions.add(Instruction.JUMP(last))
        instructions.add(Instruction.LABEL(end))
    }
}

class ConditionalBranchReturnInstructionProvider(val condition: PushInstructionProvider, consequent: PushInstructionProvider, end: Int, last: Int): BranchReturnInstructionProvider(consequent, end, last) {
    override fun get(instructions: MutableList<Instruction>) {
        condition.get(instructions)
        instructions.add(Instruction.JUMP_IF_FALSE(end))
        super.get(instructions)
    }
}

class UnconditionalBranchReturnInstructionProvider(consequent: PushInstructionProvider, end: Int, last: Int) : BranchReturnInstructionProvider(consequent, end, last)

class ConditionCollectingInstructionProvider(val instructionProviders: List<ConditionInstructionProvider>) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        for (instructionProvider in instructionProviders) {
            instructionProvider.get(instructions)
        }
    }
}

class ConditionCollectingPushInstructionProvider(val instructionProviders: List<ConditionPushInstructionProvider>) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        for (instructionProvider in instructionProviders) {
            instructionProvider.get(instructions)
        }
    }
}

class NewFunctionInstructionProvider(val function: FunctionHandle) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        instructions.add(function.newInstruction)
    }
}

class NewExplicitMappingInstructionProvider(val mappings: List<Pair<PushInstructionProvider, PushInstructionProvider>>) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        for (mapping in mappings) {
            mapping.first.get(instructions)
            mapping.second.get(instructions)
        }
        instructions.add(Instruction.NEW_EXPLICIT_MAPPING(mappings.size))
    }
}

class NewImplicitMappingInstructionProvider(val values: List<PushInstructionProvider>) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        for (value in values) {
            value.get(instructions)
        }
        instructions.add(Instruction.NEW_IMPLICIT_MAPPING(values.size))
    }
}

class NewSetInstructionProvider(val values: List<PushInstructionProvider>) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        for (value in values) {
            value.get(instructions)
        }
        instructions.add(Instruction.NEW_SET(values.size))
    }
}

class NewTupleInstructionProvider(val values: List<PushInstructionProvider>) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        for (value in values) {
            value.get(instructions)
        }
        instructions.add(Instruction.NEW_TUPLE(values.size))
    }
}

class PushFloatInstructionProvider(val value: Float) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        instructions.add(Instruction.PUSH_FLOAT(value))
    }
}

class PushIntInstructionProvider(val value: Int) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        instructions.add(Instruction.PUSH_INT(value))
    }
}

object PushIdentityInstructionProvider : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        instructions.add(Instruction.NEW_IDENTITY)
    }
}

class PushStringInstructionProvider(val value: String) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        instructions.add(Instruction.PUSH_STRING(value))
    }
}

class InvokeInstructionProvider(val invokable: PushInstructionProvider, val parameters: List<PushInstructionProvider>) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        invokable.get(instructions)
        for (parameter in parameters) {
            parameter.get(instructions)
        }
        instructions.add(Instruction.INVOKE(parameters.size))
    }
}

class InvokeReturnInstructionProvider(val invokable: PushInstructionProvider, val parameters: List<PushInstructionProvider>) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        invokable.get(instructions)
        for (parameter in parameters) {
            parameter.get(instructions)
        }
        instructions.add(Instruction.INVOKE_RETURN(parameters.size))
    }
}

class BinaryEvaluationReturnInstructionProvider(val a: PushInstructionProvider, val b: PushInstructionProvider, val i: Instruction) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        b.get(instructions)
        instructions.add(i)
    }
}

class AssignInstructionProvider(val a: PushInstructionProvider, val b: PushInstructionProvider) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        b.get(instructions)
        instructions.add(Instruction.ASSIGN)
    }
}

class AssignReturnInstructionProvider(val a: PushInstructionProvider, val b: PushInstructionProvider) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(Instruction.DUP)
        b.get(instructions)
        instructions.add(Instruction.ASSIGN)
    }
}

class CompoundAssignInstructionProvider(val a: PushInstructionProvider, val b: PushInstructionProvider, val i: Instruction) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(Instruction.DUP)
        b.get(instructions)
        instructions.add(i)
        instructions.add(Instruction.ASSIGN)
    }
}

class CompoundAssignReturnInstructionProvider(val a: PushInstructionProvider, val b: PushInstructionProvider, val i: Instruction) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(Instruction.DUP)
        instructions.add(Instruction.DUP)
        b.get(instructions)
        instructions.add(i)
        instructions.add(Instruction.ASSIGN)
    }
}

class EvaluateUnaryReturnInstructionProvider(val a: PushInstructionProvider, val i: Instruction) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(i)
    }
}

class AssignUnaryInstructionProvider(val a: PushInstructionProvider, val i: Instruction) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(i)
    }
}

class PreAssignUnaryReturnInstructionProvider(val a: PushInstructionProvider, val i: Instruction) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(Instruction.DUP)
        instructions.add(i)
        instructions.add(Instruction.AS_IMMUTABLE)
    }
}

class PostAssignUnaryReturnInstructionProvider(val a: PushInstructionProvider, val i: Instruction) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(Instruction.DUP)
        instructions.add(Instruction.AS_IMMUTABLE)
        instructions.add(Instruction.SWAP)
        instructions.add(i)
    }
}

class ShortCircuitLogicEvaluateInstructionProvider(val a: PushInstructionProvider, val b: InstructionProvider, val o: ShortCircuitLogicEvaluateBinaryOperator, val end: Int) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(o.shortCircuitInstruction(end))
        b.get(instructions)
        instructions.add(Instruction.LABEL(end))
    }
}

class ShortCircuitLogicEvaluateReturnInstructionProvider(val a: PushInstructionProvider, val b: PushInstructionProvider, val o: ShortCircuitLogicEvaluateBinaryOperator, val end: Int) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(Instruction.DUP)
        instructions.add(o.shortCircuitInstruction(end))
        instructions.add(Instruction.POP)
        b.get(instructions)
        instructions.add(Instruction.LABEL(end))
        instructions.add(Instruction.AS_BOOLEAN)
    }
}

class ShortCircuitLogicCompoundAssignInstructionProvider(val a: PushInstructionProvider, val b: InstructionProvider, val o: ShortCircuitLogicCompoundAssignBinaryOperator, val end: Int) : InstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(Instruction.DUP)
        instructions.add(Instruction.DUP)
        instructions.add(o.shortCircuitInstruction(end))
        instructions.add(Instruction.POP)
        b.get(instructions)
        instructions.add(Instruction.LABEL(end))
        instructions.add(Instruction.AS_BOOLEAN)
        instructions.add(Instruction.ASSIGN)
    }
}

class ShortCircuitLogicCompoundAssignReturnInstructionProvider(val a: PushInstructionProvider, val b: PushInstructionProvider, val o: ShortCircuitLogicCompoundAssignBinaryOperator, val end: Int) : PushInstructionProvider() {
    override fun get(instructions: MutableList<Instruction>) {
        a.get(instructions)
        instructions.add(Instruction.DUP)
        instructions.add(Instruction.DUP)
        instructions.add(Instruction.DUP)
        instructions.add(o.shortCircuitInstruction(end))
        instructions.add(Instruction.POP)
        b.get(instructions)
        instructions.add(Instruction.LABEL(end))
        instructions.add(Instruction.AS_BOOLEAN)
        instructions.add(Instruction.ASSIGN)
    }
}*/
