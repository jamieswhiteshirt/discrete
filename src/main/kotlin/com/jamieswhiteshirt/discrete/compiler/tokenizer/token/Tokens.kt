package com.jamieswhiteshirt.discrete.compiler.tokenizer.token

abstract class Token() {
}

abstract class StringToken(val value: String) : Token() {
    override fun equals(other: Any?) : Boolean {
        return if (other is StringToken) value == other.value else false
    }
    override fun toString() : String = value
}

abstract class LiteralToken(value : String) : StringToken(value)

class EndOfStatementToken() : Token() {
    override fun equals(other: Any?) : Boolean = other is EndOfStatementToken

    override fun toString() : String = ";"
}

class SeparatorToken() : Token() {
    override fun equals(other: Any?) : Boolean = other is SeparatorToken

    override fun toString() : String = ","
}

class StringLiteralToken(value: String) : LiteralToken(value) {
    override fun equals(other: Any?) : Boolean {
        return if (other is StringLiteralToken) super.equals(other) else false
    }
}

class IntLiteralToken(value: String) : LiteralToken(value) {
    override fun equals(other: Any?) : Boolean {
        return if (other is IntLiteralToken) super.equals(other) else false
    }
}

class FloatLiteralToken(value: String) : LiteralToken(value) {
    override fun equals(other: Any?) : Boolean {
        return if (other is FloatLiteralToken) super.equals(other) else false
    }
}

class ReferenceToken() : Token() {
    override fun equals(other: Any?) : Boolean = other is ReferenceToken

    override fun toString() : String = ":"
}

class OperatorToken(value: String) : StringToken(value) {
    override fun equals(other: Any?) : Boolean {
        return if (other is OperatorToken) super.equals(other) else false
    }
}

class WordToken(value : String) : StringToken(value) {
    override fun equals(other: Any?) : Boolean {
        return if (other is WordToken) super.equals(other) else false
    }
}

class SymbolToken(val symbol : Char) : Token() {
    override fun equals(other: Any?) : Boolean {
        return if (other is SymbolToken) symbol == other.symbol else false
    }
    override fun toString() : String = symbol.toString()
}
