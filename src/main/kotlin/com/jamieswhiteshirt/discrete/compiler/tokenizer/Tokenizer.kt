package com.jamieswhiteshirt.discrete.compiler.tokenizer

import com.jamieswhiteshirt.util.StreamList
import com.jamieswhiteshirt.util.iterator.ExtIterable
import com.jamieswhiteshirt.util.RegexChainReader
import com.jamieswhiteshirt.discrete.compiler.tokenizer.token.*
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.util.concurrent.Executor
import kotlin.text.MatchResult
import kotlin.text.Regex

class Tokenizer(val executor: Executor) {
    protected lateinit var tokenStream : StreamList<Token>

    private val regexChainReader = RegexChainReader()
    private val whitespacePattern = Regex("^[\\p{Space}]+")
    private val commentPattern = Regex("^#.*")
    private val endOfStatementPattern = Regex("^;")
    private val separatorPattern = Regex("^\\,")
    private val stringLiteralPattern = Regex("^\"(\\.|[^\"])*\"")
    private val floatLiteralPattern = Regex("^[0-9]*\\.[0-9]+")
    private val intLiteralPattern = Regex("^[0-9]+")
    private val operatorPattern = Regex("^((!!)?is|(!!)?in|\\+\\+|\\-\\-|(\\|[\\|\\?]|\\^\\^|\\&[\\&\\?]|\\<\\<|\\>\\>|//|!!|[\\Q|&^+-*%.=!<>\\E/!])\\=?)")
    private val wordPattern = Regex("^[a-zA-Z0-9_]+")
    private val symbolPattern = Regex("^[\\Q(){}[]?\\E]")
    private val fallbackPattern = Regex(".*")

    init {
        regexChainReader.appendChainEntry(whitespacePattern, fun(@Suppress("UNUSED_PARAMETER") match: MatchResult) : Unit {
        })
        regexChainReader.appendChainEntry(commentPattern, fun(@Suppress("UNUSED_PARAMETER") match: MatchResult) : Unit {
        })
        regexChainReader.appendChainEntry(endOfStatementPattern, fun(@Suppress("UNUSED_PARAMETER") match: MatchResult) : Unit {
            tokenStream.add(EndOfStatementToken())
        })
        regexChainReader.appendChainEntry(separatorPattern, fun(@Suppress("UNUSED_PARAMETER") match: MatchResult) : Unit {
            tokenStream.add(SeparatorToken())
        })
        regexChainReader.appendChainEntry(stringLiteralPattern, fun(match: MatchResult) : Unit {
            tokenStream.add(StringLiteralToken(match.value))
        })
        regexChainReader.appendChainEntry(floatLiteralPattern, fun(match: MatchResult) : Unit {
            tokenStream.add(FloatLiteralToken(match.value))
        })
        regexChainReader.appendChainEntry(intLiteralPattern, fun(match: MatchResult) : Unit {
            tokenStream.add(IntLiteralToken(match.value))
        })
        regexChainReader.appendChainEntry(operatorPattern, fun(match: MatchResult) : Unit {
            tokenStream.add(OperatorToken(match.value))
        })
        regexChainReader.appendChainEntry(wordPattern, fun(match: MatchResult) : Unit {
            tokenStream.add(WordToken(match.value))
        })
        regexChainReader.appendChainEntry(symbolPattern, fun(match: MatchResult) : Unit {
            tokenStream.add(SymbolToken(match.value[0]))
        })
        regexChainReader.appendChainEntry(fallbackPattern, fun(match: MatchResult) : Unit {
            throw Exception("Could not parse ${match.value}")
        })
    }

    fun read(inputStream : InputStream) : ExtIterable<Token> {
        tokenStream = StreamList<Token>()
        executor.execute { this._read(inputStream) }
        return tokenStream
    }

    protected fun _read(inputStream : InputStream) {
        val bufferedReader = BufferedReader(InputStreamReader(inputStream))
        while (true) {
            var line = bufferedReader.readLine()
            if (line != null) {
                regexChainReader.read(line)
            }
            else {
                tokenStream.close()
                break
            }
        }
    }
}
