package com.jamieswhiteshirt.discrete.compiler

import com.jamieswhiteshirt.discrete.common.code.scope.ModuleScopeDefinition
import com.jamieswhiteshirt.discrete.common.serialization.scope.ModuleScopeSerialization
import com.jamieswhiteshirt.discrete.compiler.builders.ModuleBuilder
import com.jamieswhiteshirt.discrete.compiler.builders.ProgramBuilder
import com.jamieswhiteshirt.discrete.compiler.ast.parsers.statement.StatementParser
import com.jamieswhiteshirt.discrete.compiler.tokenizer.Tokenizer
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution
import com.jamieswhiteshirt.discrete.runtime.execution.ModuleLocalExecution
import com.jamieswhiteshirt.discrete.stl.StandardLibrary
import com.jamieswhiteshirt.util.DirectExecutor
import java.io.*

fun main(args : Array<String>) {
    val moduleName = args[0]
    val diFile = File("$moduleName.di")
    val dcFile = File("$moduleName.dc")

    val programBuilder = ProgramBuilder()
    programBuilder.importModule(StandardLibrary)

    val inputStream = FileInputStream(diFile)
    val moduleScope = compile(moduleName, programBuilder, inputStream)
    inputStream.close()

    val globalExecution = GlobalExecution()
    globalExecution.injectModule(StandardLibrary)
    globalExecution.injectModule(ModuleLocalExecution(moduleScope))

    val outputStream = DataOutputStream(FileOutputStream(dcFile))
    ModuleScopeSerialization.serialize(outputStream, moduleScope)
    outputStream.close()
}

fun compile(moduleName: String, programBuilder: ProgramBuilder, inputStream: InputStream) : ModuleScopeDefinition {
    val tokenizer = Tokenizer(DirectExecutor)
    val iterable = tokenizer.read(inputStream)
    val statements = StatementParser.getAll(iterable.begin, iterable.end)
    return ModuleBuilder(moduleName, programBuilder, statements).createDefinition()
}

