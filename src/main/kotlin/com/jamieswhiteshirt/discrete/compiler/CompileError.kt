package com.jamieswhiteshirt.discrete.compiler

class CompileError : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
}
