package com.jamieswhiteshirt.discrete.runtime.execution

import com.jamieswhiteshirt.discrete.common.module.ModuleInstance

class GlobalExecution {
    private val modules = mutableMapOf<String, ModuleInstance>()

    fun injectModule(module: ModuleInstance) {
        modules[module.moduleName] = module
        module.execute(this)
    }

    fun loadModule(module: String): ModuleInstance {
        throw NotImplementedError()
    }

    operator fun get(module: String): ModuleInstance = modules[module] ?: loadModule(module)
}
