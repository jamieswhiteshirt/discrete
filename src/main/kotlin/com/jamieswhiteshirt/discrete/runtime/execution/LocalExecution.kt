package com.jamieswhiteshirt.discrete.runtime.execution

import com.jamieswhiteshirt.discrete.common.code.ScopeDefinition
import com.jamieswhiteshirt.discrete.common.value.Value
import java.util.*

abstract class LocalExecution(val scope: ScopeDefinition, val localVariables: Array<Array<Value>>) {
    private var pc: Int = 0
    private val instructions = scope.instructions
    private val stack = Stack<Value>()

    open fun _return() {
        pc = instructions.size
    }

    abstract fun _return(value: Value)

    fun jump(label: Int) {
        pc = scope.labels[label]
    }

    fun execute(globalExecution: GlobalExecution) {
        while (pc < instructions.size) {
            instructions[pc++].execute(globalExecution, this)
        }
    }

    fun push(value: Value): Value = stack.push(value)

    fun pop(): Value = stack.pop()

    fun peek(): Value = stack.peek()
}
