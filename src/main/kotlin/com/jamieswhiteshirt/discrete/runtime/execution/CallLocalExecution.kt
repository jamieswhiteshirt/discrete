package com.jamieswhiteshirt.discrete.runtime.execution

import com.jamieswhiteshirt.discrete.common.code.ScopeDefinition
import com.jamieswhiteshirt.discrete.common.value.UndefinedTypeValue
import com.jamieswhiteshirt.discrete.common.value.Value

abstract class CallLocalExecution(scope: ScopeDefinition, localVariables: Array<Array<Value>>) : LocalExecution(scope, localVariables) {
    var returnValue: Value = UndefinedTypeValue

    override fun _return(value: Value) {
        returnValue = value
        _return()
    }
}
