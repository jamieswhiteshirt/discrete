package com.jamieswhiteshirt.discrete.runtime.execution

import com.jamieswhiteshirt.discrete.common.code.scope.ModuleScopeDefinition
import com.jamieswhiteshirt.discrete.common.module.ModuleInstance
import com.jamieswhiteshirt.discrete.common.value.UndefinedTypeValue
import com.jamieswhiteshirt.discrete.common.value.Value

class ModuleLocalExecution(definition: ModuleScopeDefinition) : CallLocalExecution(definition, arrayOf(definition.localVariables.map { it.mutability.instantiate(UndefinedTypeValue) }.toTypedArray())), ModuleInstance {
    val moduleVariables = definition.moduleVariables.associateBy({it.identifier}, {it.instantiate(UndefinedTypeValue)})

    override val moduleName = definition.moduleName

    override operator fun get(identifier: String): Value = moduleVariables[identifier]!!
}
