package com.jamieswhiteshirt.discrete.runtime.execution

import com.jamieswhiteshirt.discrete.common.code.scope.FunctionScopeDefinition
import com.jamieswhiteshirt.discrete.common.value.UndefinedTypeValue
import com.jamieswhiteshirt.discrete.common.value.Value

class FunctionLocalExecution(
        definition: FunctionScopeDefinition,
        parent: LocalExecution,
        parameters: Array<Value>
) : CallLocalExecution(
        definition,
        parent.localVariables + definition.localVariables.mapIndexed { i, it ->
            it.instantiate(if (i < parameters.size && i < definition.parameterCount) parameters[i].immutable else UndefinedTypeValue)
        }.toTypedArray()
)
