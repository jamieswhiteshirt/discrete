package com.jamieswhiteshirt.discrete.runtime.execution

import com.jamieswhiteshirt.discrete.common.code.scope.BlockScopeDefinition
import com.jamieswhiteshirt.discrete.common.value.UndefinedTypeValue
import com.jamieswhiteshirt.discrete.common.value.Value

class BlockLocalExecution(
        scope: BlockScopeDefinition,
        val parent: LocalExecution
) : LocalExecution(
        scope,
        parent.localVariables + scope.localVariables.map { it.instantiate(UndefinedTypeValue) }.toTypedArray()
) {
    override fun _return() {
        super._return()
        parent._return()
    }

    override fun _return(value: Value) {
        super._return()
        parent._return(value)
    }
}
