package com.jamieswhiteshirt.discrete.runtime

import com.jamieswhiteshirt.discrete.common.code.scope.FunctionScopeDefinition
import com.jamieswhiteshirt.discrete.common.invokable.Invokable
import com.jamieswhiteshirt.discrete.common.value.Value
import com.jamieswhiteshirt.discrete.runtime.execution.LocalExecution
import com.jamieswhiteshirt.discrete.runtime.execution.FunctionLocalExecution
import com.jamieswhiteshirt.discrete.runtime.execution.GlobalExecution

class FunctionInstance(
        val definition: FunctionScopeDefinition,
        val localExecution: LocalExecution
) : Invokable {
    override fun invoke(globalExecution: GlobalExecution, params: Array<Value>) {
        val execution = FunctionLocalExecution(definition, localExecution, params)
        execution.execute(globalExecution)
    }

    override fun invokeReturn(globalExecution: GlobalExecution, params: Array<Value>): Value {
        val execution = FunctionLocalExecution(definition, localExecution, params)
        execution.execute(globalExecution)
        return execution.returnValue
    }
}
