package com.jamieswhiteshirt.util

fun Float.floor(): Int {
    return Math.floor(this.toDouble()).toInt()
}
