package com.jamieswhiteshirt.util

import com.jamieswhiteshirt.util.iterator.ExtIterable
import com.jamieswhiteshirt.util.iterator.ExtIterator
import com.jamieswhiteshirt.util.iterator.ExtIteratorComparisonException
import java.util.*
import java.util.concurrent.CompletableFuture

class StreamList<T> : ExtIterable<T> {
    private abstract class AbstractIter<T>(val stream: StreamList<T>, var i: Int) : ExtIterator<T> {
        abstract val index: Int

        override val value: T
            get() {
                while (index >= stream.currentSize) {
                    if (!stream.hasNext.get()) {
                        break
                    }
                }
                return stream[index]
            }

        override fun inc(): ExtIterator<T> {
            i++
            return this
        }
        override fun dec(): ExtIterator<T> {
            i--
            return this
        }
        override operator fun plus(other: ExtIterator<T>) : Int {
            if (other is AbstractIter && stream == other.stream) {
                return index + other.index
            }
            throw ExtIteratorComparisonException()
        }
        override operator fun minus(other: ExtIterator<T>) : Int {
            if (other is AbstractIter && stream == other.stream) {
                return index - other.index
            }
            throw ExtIteratorComparisonException()
        }
        override fun equals(other: Any?): Boolean {
            if (other is AbstractIter<*>) {
                return index == other.index
            }
            return false
        }
        override fun hashCode(): Int {
            var result = stream.hashCode()
            result += 31 * result + i
            return result
        }
    }

    private class BeginIter<T>(stream: StreamList<T>, i : Int = 0) : AbstractIter<T>(stream, i) {
        override val index: Int
            get() = i

        override operator fun plus(i: Int) : ExtIterator<T> {
            return BeginIter(stream, this.i + i)
        }
        override operator fun minus(i: Int) : ExtIterator<T> {
            return BeginIter(stream, this.i - i)
        }
        override operator fun plus(other: ExtIterator<T>) : Int {
            if (other is BeginIter && stream == other.stream) {
                return i + other.i
            }
            else {
                return super.plus(other)
            }
        }
        override operator fun minus(other: ExtIterator<T>) : Int {
            if (other is BeginIter && stream == other.stream) {
                return i - other.i
            }
            else {
                return super.minus(other)
            }
        }
        override fun clone(): ExtIterator<T> {
            return BeginIter(stream, i)
        }
        override operator fun compareTo(other: ExtIterator<T>) : Int {
            if (other is AbstractIter && stream == other.stream) {
                if (other is EndIter && stream.hasAtLeast(i - other.i + 1)) {
                    return -1
                }
                return java.lang.Integer.compare(index, other.index)
            }
            throw ExtIteratorComparisonException()
        }
        override fun equals(other: Any?): Boolean {
            if (other is BeginIter<*> && stream == other.stream) {
                return i == other.i
            }
            return super.equals(other)
        }
        override fun hashCode(): Int{
            return super.hashCode()
        }
    }

    private class EndIter<T>(stream: StreamList<T>, i: Int = 0) : AbstractIter<T>(stream, i) {
        override val index: Int
            get() = stream.finalSize + i

        override operator fun plus(i: Int) : ExtIterator<T> {
            return EndIter(stream, this.i + i)
        }
        override operator fun minus(i: Int) : ExtIterator<T> {
            return EndIter(stream, this.i - i)
        }
        override operator fun minus(other: ExtIterator<T>) : Int {
            if (other is EndIter && stream == other.stream) {
                return i - other.i
            }
            else {
                return super.minus(other)
            }
        }
        override operator fun compareTo(other: ExtIterator<T>): Int {
            if (other is AbstractIter) {
                if (other is BeginIter && stream.hasAtLeast(other.i - i)) {
                    return -1
                }
                return java.lang.Integer.compare(index, other.index)
            }
            throw ExtIteratorComparisonException()
        }
        override fun clone(): ExtIterator<T> {
            return EndIter(stream, i)
        }
        override fun equals(other: Any?): Boolean {
            if (other is EndIter<*> && stream == other.stream) {
                return i == other.i
            }
            return super.equals(other)
        }
        override fun hashCode(): Int{
            return super.hashCode()
        }
    }

    private val list = LinkedList<T>()
    private var hasNext = CompletableFuture<Boolean>()
    private val currentSize: Int
        get() = list.size
    private val finalSize: Int
        get() {
            while(hasNext.get()) { }
            return list.size
        }

    private fun hasAtLeast(i: Int) : Boolean {
        while (i > list.size) {
            if (!hasNext.get()) {
                return false
            }
        }
        return i <= list.size
    }

    operator fun get(i: Int) : T {
        while (i >= list.size) {
            if (!hasNext.get()) {
                break
            }
        }
        return list[i]
    }

    fun add(obj: T) {
        list.add(obj)

        val future = hasNext
        hasNext = CompletableFuture()

        future.complete(true)
    }

    fun close() {
        hasNext.complete(false)
    }

    override val begin : ExtIterator<T>
        get() {
            return BeginIter(this)
        }

    override val end : ExtIterator<T>
        get() {
            return EndIter(this)
        }
}
