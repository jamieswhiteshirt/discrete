package com.jamieswhiteshirt.util

import java.util.concurrent.Executor

object DirectExecutor : Executor {
    override fun execute(runnable: Runnable) {
        runnable.run()
    }
}

object ThreadExecutor : Executor {
    override fun execute(runnable: Runnable) {
        Thread({ runnable.run() }).start()
    }
}
