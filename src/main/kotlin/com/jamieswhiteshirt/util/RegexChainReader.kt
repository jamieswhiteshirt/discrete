package com.jamieswhiteshirt.util

import java.util.*
import kotlin.text.MatchResult
import kotlin.text.Regex

class RegexChainReader {
    private class ChainEntry(val pattern : Regex, val callback : (MatchResult) -> Unit) {
    }

    private val chain = LinkedList<ChainEntry>()

    fun appendChainEntry(pattern: Regex, callback: (MatchResult) -> Unit) {
        chain.add(ChainEntry(pattern, callback))
    }

    fun read(str: String) {
        var searchString = str
        do {
            if (searchString.length == 0) {
                break
            }

            var match : MatchResult? = null
            for (entry in chain) {
                match = entry.pattern.find(searchString)
                if (match != null) {
                    entry.callback.invoke(match)
                    searchString = searchString.substring(match.range.endInclusive + 1)
                    break
                }
            }
        } while(match != null)
    }
}
