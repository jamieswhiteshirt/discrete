package com.jamieswhiteshirt.util

import kotlin.reflect.KClass
import kotlin.reflect.KProperty

fun <T> KProperty<T>.hasAnnotation(c: KClass<*>): Boolean {
    for (annotation in annotations) {
        if (c.java.isAssignableFrom(annotation.javaClass)) {
            return true
        }
    }
    return false
}
