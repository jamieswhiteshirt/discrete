package com.jamieswhiteshirt.util.search

import com.jamieswhiteshirt.util.iterator.ExtIterator
import java.util.*

abstract class MultiSearchEntry<T>(val search: Search<T>) {
    var next: MultiSearchEntry<T>? = null
    abstract fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : List<SearchResult<T>>?
}

class ExactEntry<T>(search: Search<T>) : MultiSearchEntry<T>(search) {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : List<SearchResult<T>>? {
        val selfResult = search.search(begin, from, end)
        if (selfResult != null) {
            if (next != null) {
                val nextResult = next!!.search(begin, selfResult.end, end)
                if (nextResult != null) {
                    return listOf(selfResult) + nextResult
                }
            }
            else {
                return listOf(selfResult)
            }
        }
        return null
    }
}

class ReluctantEntry<T>(search: Search<T>, val quantifier : Quantifier) : MultiSearchEntry<T>(search) {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : List<SearchResult<T>>? {
        val selfResults = LinkedList<SearchResult<T>>()
        var iter = from.clone()
        while (iter <= end) {
            //if the quantifier does not need more matches, begin to look ahead
            if (!quantifier.needsMore(selfResults.size)) {
                if (next != null) {
                    //if looking ahead is successful, stop searching with the current search
                    val nextResults = next!!.search(begin, iter, end)
                    if (nextResults != null) {
                        return listOf(MultiSearchResult(from, selfResults)) + nextResults
                    }
                }
                else {
                    return listOf(MultiSearchResult(from, selfResults))
                }
            }

            //if the quantifier does not need less matches, continue looking
            if (!quantifier.needsLess(selfResults.size)) {
                val selfResult = search.search(begin, iter, end)
                if (selfResult != null) {
                    selfResults.add(selfResult)
                    iter = selfResult.end
                }
                else {
                    return null
                }
            }
            else {
                return null
            }
        }

        return null
    }
}

class GreedyEntry<T>(search: Search<T>, val quantifier : Quantifier) : MultiSearchEntry<T>(search) {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : List<SearchResult<T>>? {
        val selfResults = LinkedList<SearchResult<T>>()
        var iter = from.clone()

        while (iter <= end) {
            if (!quantifier.needsLess(selfResults.size)) {
                val selfResult = search.search(begin, iter, end)
                if (selfResult != null) {
                    selfResults.add(selfResult)
                    iter = selfResult.end
                }
                else {
                    break
                }
            }
            else {
                break
            }
        }

        if (next != null) {
            while (!quantifier.needsMore(selfResults.size)) {
                if (selfResults.isNotEmpty()) {
                    iter = selfResults.last.end
                }
                else {
                    iter = from
                }
                val nextResults = next!!.search(begin, iter, end)
                if (nextResults != null) {
                    return listOf(MultiSearchResult(from, selfResults)) + nextResults
                }
                else {
                    if (selfResults.isNotEmpty()) {
                        selfResults.removeLast()
                    }
                    else {
                        break
                    }
                }
            }

            return null
        }
        else {
            return listOf(MultiSearchResult(from, selfResults))
        }
    }
}

class SequenceSearch<T>(vararg searches: MultiSearchEntry<T>) : Search<T>() {
    val searches = searches

    init {
        for (i in 0..(searches.size - 2)) {
            searches[i].next = searches[i + 1]
        }
    }

    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>? {
        if (searches.isNotEmpty()) {
            val results = searches.first().search(begin, from, end)
            if (results != null) {
                return MultiSearchResult(from, results)
            }
            return null
        }
        else {
            return MultiSearchResult(from, listOf())
        }
    }

    override fun setRecursor(recursor: Search<T>) {
        for (entry in searches) {
            entry.search.setRecursor(recursor)
        }
    }
}
