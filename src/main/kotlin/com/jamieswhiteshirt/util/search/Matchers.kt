package com.jamieswhiteshirt.util.search

import kotlin.reflect.KClass

abstract class Matcher<T>() {
    abstract fun match(other: T) : Boolean
}

class AnyMatcher<T>() : Matcher<T>() {
    override fun match(other: T) = true
}

class NotMatcher<T>(val matcher: Matcher<T>) : Matcher<T>() {
    override fun match(other: T) = !matcher.match(other)
}

abstract class MultiMatcher<T>(vararg matchers: Matcher<T>) : Matcher<T>() {
    val matchers = matchers
}

class OrMatcher<T>(vararg matchers: Matcher<T>) : MultiMatcher<T>(*matchers) {
    override fun match(other: T) : Boolean {
        for (matcher in matchers) {
            if (matcher.match(other)) {
                return true
            }
        }
        return false
    }
}

class AndMatcher<T>(vararg matchers: Matcher<T>) : MultiMatcher<T>(*matchers) {
    override fun match(other: T) : Boolean {
        for (matcher in matchers) {
            if (!matcher.match(other)) {
                return false
            }
        }
        return true
    }
}

class EqualityMatcher<T>(val toMatch: T) : Matcher<T>() {
    override fun match(other: T) = other == toMatch
}

class TypeMatcher<T : Any>(val toMatch: KClass<*>) : Matcher<T>() {
    override fun match(other: T) = other.javaClass.isAssignableFrom(toMatch.java)
}
