package com.jamieswhiteshirt.util.search

abstract class Quantifier() {
    abstract fun needsMore(i: Int): Boolean
    abstract fun needsLess(i: Int): Boolean
}

object One : Quantifier() {
    override fun needsMore(i: Int) = i < 1
    override fun needsLess(i: Int) = i > 1
}

object OneOrMore : Quantifier() {
    override fun needsMore(i: Int) = i < 1
    override fun needsLess(i: Int) = false
}

object OneOrLess : Quantifier() {
    override fun needsMore(i: Int) = false
    override fun needsLess(i: Int) = i > 1
}

object Zero : Quantifier() {
    override fun needsMore(i: Int) = i < 0
    override fun needsLess(i: Int) = i > 0
}

object ZeroOrMore : Quantifier() {
    override fun needsMore(i: Int) = false
    override fun needsLess(i: Int) = false
}

class Exactly(val exactly: Int) : Quantifier() {
    override fun needsMore(i: Int) = i < exactly
    override fun needsLess(i: Int) = i > exactly
}

class LessThan(val lessThan: Int) : Quantifier() {
    override fun needsMore(i: Int) = false
    override fun needsLess(i: Int) = i > lessThan
}

class MoreThan(val moreThan: Int) : Quantifier() {
    override fun needsMore(i: Int) = i < moreThan
    override fun needsLess(i: Int) = false
}

class Between(val lessThan: Int, val moreThan: Int) : Quantifier() {
    override fun needsMore(i: Int) = i < moreThan
    override fun needsLess(i: Int) = i > lessThan
}
