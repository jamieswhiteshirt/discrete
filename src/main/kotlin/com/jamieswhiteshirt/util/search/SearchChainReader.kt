package com.jamieswhiteshirt.util.search

import com.jamieswhiteshirt.util.iterator.ExtIterator
import java.util.*

class SearchChainReader<T> {
    private data class Entry<T>(val search: Search<T>, val callback: (SearchResult<T>) -> Unit)

    private val chain = LinkedList<Entry<T>>()

    fun appendChainEntry(search: Search<T>, callback: (SearchResult<T>) -> Unit) {
        chain.add(Entry(search, callback))
    }

    fun read(begin: ExtIterator<T>, end: ExtIterator<T>) {
        var current = begin
        while (current < end) {
            for ((search, callback) in chain) {
                val result = search.search(begin, current, end)
                if (result != null) {
                    callback.invoke(result)
                    current = result.end
                    break
                }
            }
        }
    }
}
