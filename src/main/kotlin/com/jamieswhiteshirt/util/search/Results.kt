package com.jamieswhiteshirt.util.search

import com.jamieswhiteshirt.util.iterator.ExtIterator

private class SearchResultIterator<T>(val searchResult: SearchResult<T>, var i: Int) : Iterator<SearchResult<T>> {
    override fun next(): SearchResult<T> {
        return searchResult[i++]
    }
    override fun hasNext(): Boolean {
        return i < searchResult.children
    }
}

abstract class SearchResult<T>() : Iterable<SearchResult<T>> {
    abstract val begin: ExtIterator<T>
    abstract val end: ExtIterator<T>
    abstract val children: Int
    abstract operator fun get(i: Int): SearchResult<T>
    abstract val length: Int
}

class SingleSearchResult<T>(begin: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>() {
    override val begin: ExtIterator<T> = begin
    override val end: ExtIterator<T> = end
    override val children: Int = 0
    override operator fun get(i: Int): SearchResult<T> = throw Exception("Single search results do not have children")
    override val length: Int
        get() = end - begin

    override fun iterator(): Iterator<SearchResult<T>> {
        throw Exception("Cannot iterate over single search result")
    }
}

class EmptySearchResult<T>(val beginEnd: ExtIterator<T>) : SearchResult<T>() {
    override val begin: ExtIterator<T> = beginEnd
    override val end: ExtIterator<T> = beginEnd
    override val children: Int = 0
    override fun get(i: Int): SearchResult<T> = throw IndexOutOfBoundsException()
    override val length: Int = 0

    override fun iterator() : Iterator<SearchResult<T>> {
        return SearchResultIterator(this, 0)
    }
}

class MultiSearchResult<T> : SearchResult<T> {
    private val results: List<SearchResult<T>>

    companion object {
        operator fun <T> invoke(begin: ExtIterator<T>, results: List<SearchResult<T>>) : SearchResult<T> {
            if (results.isNotEmpty()) {
                return MultiSearchResult(results)
            }
            else {
                return EmptySearchResult(begin)
            }
        }
    }

    private constructor(results: List<SearchResult<T>>) {
        this.results = results
    }

    override val begin: ExtIterator<T>
        get() = results.first().begin
    override val end: ExtIterator<T>
        get() = results.last().end
    override val children: Int
        get() = results.size
    override operator fun get(i: Int): SearchResult<T> = results[i]
    override val length: Int
        get() {
            var length = 0
            for (result in results) {
                length += result.length
            }
            return length
        }

    override fun iterator() : Iterator<SearchResult<T>> {
        return SearchResultIterator(this, 0)
    }
}
