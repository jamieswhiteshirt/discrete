package com.jamieswhiteshirt.util.search

import com.jamieswhiteshirt.util.iterator.ExtIterator

abstract class Search<T>() {
    abstract fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>?
    open fun setRecursor(recursor: Search<T>) { }

    fun search(begin: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>? {
        return search(begin, begin, end)
    }

    fun recurse() : Search<T> {
        setRecursor(this)
        return this
    }
}

class AnySearch<T>() : Search<T>() {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>): SearchResult<T>? {
        if (from < end) {
            return SingleSearchResult(from, from + 1)
        }
        return null
    }
}

class Recursion<T>() : Search<T>() {
    private var recursor: Search<T>? = null
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>? {
        val recursor = this.recursor
        if (recursor != null) {
            return recursor.search(begin, from, end)
        }
        else {
            throw Exception("Unsatisfied recursion")
        }
    }

    override fun setRecursor(recursor: Search<T>) {
        if (this.recursor == null) {
            this.recursor = recursor
        }
    }
}

class IsBegin<T>() : Search<T>() {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>? {
        if (begin >= from) {
            return EmptySearchResult(from)
        }
        else {
            return null
        }
    }
}

class IsNotBegin<T>() : Search<T>() {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>? {
        if (begin < from) {
            return EmptySearchResult(from)
        }
        else {
            return null
        }
    }
}

class IsEnd<T>() : Search<T>() {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>? {
        if (from >= end) {
            return EmptySearchResult(from)
        }
        else {
            return null
        }
    }
}

class IsNotEnd<T>() : Search<T>() {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>? {
        if (from < end) {
            return EmptySearchResult(from)
        }
        else {
            return null
        }
    }
}

class Union<T>(vararg val searches: Search<T>) : Search<T>() {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>? {
        for (search in searches) {
            val result = search.search(begin, from, end)
            if (result != null) {
                return result
            }
        }
        return null
    }

    override fun setRecursor(recursor: Search<T>) {
        for (search in searches) {
            search.setRecursor(recursor)
        }
    }
}

class MatcherSearch<T>(vararg val matchers: Matcher<T>) : Search<T>() {
    override fun search(begin: ExtIterator<T>, from: ExtIterator<T>, end: ExtIterator<T>) : SearchResult<T>? {
        var iter = from.clone()

        for (matcher in matchers) {
            if (iter < end) {
                if (!matcher.match(iter.value)) {
                    return null
                }
            }
            else {
                return null
            }
            iter++
        }

        return SingleSearchResult(from, iter)
    }
}
