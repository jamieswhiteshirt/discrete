package com.jamieswhiteshirt.util.iterator

import java.util.*

interface ExtIterable<T> {
    val begin : ExtIterator<T>
    val end : ExtIterator<T>
}

interface ExtIterator<T> {
    operator fun plus(i: Int) : ExtIterator<T>
    operator fun minus(i: Int) : ExtIterator<T>
    operator fun plus(other: ExtIterator<T>) : Int
    operator fun minus(other: ExtIterator<T>) : Int
    operator fun inc() : ExtIterator<T>
    operator fun dec() : ExtIterator<T>
    operator fun compareTo(other: ExtIterator<T>) : Int
    fun clone() : ExtIterator<T>

    val value: T
}


class ExtIteratorComparisonException : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
}

fun <T> copyRange(begin: ExtIterator<T>, end: ExtIterator<T>) : List<T> {
    val range = ArrayList<T>()
    var iter = begin.clone()
    while (iter < end) {
        range.add(iter.value)
        iter++
    }
    return range
}

fun <T> printRange(begin: ExtIterator<T>, end: ExtIterator<T>, separator: String = " ") {
    val list = LinkedList<String>()
    var iter = begin.clone()
    while (iter < end) {
        list.add(iter.value.toString())
        iter++
    }
    println(list.joinToString(separator))
}
