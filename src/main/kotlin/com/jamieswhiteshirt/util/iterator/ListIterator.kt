package com.jamieswhiteshirt.util.iterator

abstract class AbstractListExtIterator<T>(val list: List<T>, var i: Int) : ExtIterator<T> {
    abstract val index: Int

    override fun inc() : ExtIterator<T> {
        i++
        return this
    }
    override fun dec() : ExtIterator<T> {
        i--
        return this
    }
    override operator fun plus(other: ExtIterator<T>) : Int {
        if (other is AbstractListExtIterator<T> && list == other.list) {
            return index + other.index
        }
        throw ExtIteratorComparisonException()
    }
    override operator fun minus(other: ExtIterator<T>) : Int {
        if (other is AbstractListExtIterator<T> && list == other.list) {
            return index - other.index
        }
        throw ExtIteratorComparisonException()
    }
    override operator fun compareTo(other: ExtIterator<T>) : Int {
        if (other is AbstractListExtIterator<T> && list == other.list) {
            return Integer.compare(index, other.index)
        }
        throw ExtIteratorComparisonException()
    }

    override val value: T
        get() = list[index]
}

class BeginListExtIterator<T>(list: List<T>, i: Int) : AbstractListExtIterator<T>(list, i) {
    override val index: Int
        get() = i

    override operator fun plus(i: Int) : ExtIterator<T> {
        return BeginListExtIterator(list, this.i + i)
    }
    override operator fun minus(i: Int) : ExtIterator<T> {
        return BeginListExtIterator(list, this.i - i)
    }
    override fun clone() : ExtIterator<T> {
        return BeginListExtIterator(list, i)
    }
}

class EndListExtIterator<T>(list: List<T>, i: Int) : AbstractListExtIterator<T>(list, i) {
    override val index: Int
        get() = i + list.size

    override operator fun plus(i: Int) : ExtIterator<T> {
        return EndListExtIterator(list, this.i + i)
    }
    override operator fun minus(i: Int) : ExtIterator<T> {
        return EndListExtIterator(list, this.i - i)
    }
    override fun clone() : ExtIterator<T> {
        return EndListExtIterator(list, i)
    }
}

val <T> List<T>.begin : ExtIterator<T>
    get() {
        return BeginListExtIterator(this, 0)
    }

val <T> List<T>.end : ExtIterator<T>
    get() {
        return EndListExtIterator(this, 0)
    }
