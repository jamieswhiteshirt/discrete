package com.jamieswhiteshirt.util

import java.util.*

class SplitList<T> : AbstractSequentialList<T> {
    private class MutableSplitListIterator<T>(list: SplitList<T>, index: Int) : MutableListIterator<T>, Iterator<T> {
        val frontIterator = list.front.listIterator()
        val backIterator = list.back.listIterator()
        var currentIterator = frontIterator

        init {
            for (i in 0..index - 1) {
                next()
            }
        }

        override fun next(): T {
            if (!currentIterator.hasNext()) {
                currentIterator = backIterator
            }
            return currentIterator.next()
        }
        override fun hasNext(): Boolean {
            return currentIterator.hasNext() || backIterator.hasNext()
        }
        override fun previous(): T {
            if (!currentIterator.hasPrevious()) {
                currentIterator = frontIterator
            }
            return currentIterator.previous()
        }
        override fun hasPrevious(): Boolean {
            return currentIterator.hasPrevious() || frontIterator.hasPrevious()
        }
        override fun nextIndex(): Int {
            return frontIterator.nextIndex() + backIterator.nextIndex() - 1
        }
        override fun previousIndex(): Int {
            return frontIterator.previousIndex() + backIterator.previousIndex() + 1
        }
        override fun remove() {
            currentIterator.remove()
        }
        override fun set(element: T) {
            currentIterator.set(element)
        }
        override fun add(element: T) {
            currentIterator.add(element)
        }
    }

    constructor() {
        front = LinkedList()
        back = LinkedList()
    }

    constructor(list: List<T>) : this() {
        for (element in list) {
            add(element)
        }
    }

    private constructor(front: List<T>, back: List<T>) {
        this.front = LinkedList(front)
        this.back = LinkedList(back)
    }

    private val front: LinkedList<T>
    private val back: LinkedList<T>
    var middle: Int
        get() = front.size
        set(i: Int) {
            val difference = i - middle
            if (difference > 0) {
                for (j in 0..difference - 1) {
                    front.add(back.first)
                    back.removeFirst()
                }
            }
            else if (difference < 0) {
                for (j in 0..-difference - 1) {
                    back.add(0, front.last)
                    front.removeLast()
                }
            }
        }

    fun middleFrontPush(element: T) {
        front.add(element)
    }
    fun middleFrontReplace(element: T) {
        front.set(front.size - 1, element)
    }
    fun middleFrontPop() {
        front.removeLast()
    }
    fun middleFrontPop(i: Int) {
        for (j in 0..i - 1) {
            middleFrontPop()
        }
    }
    fun middleBackPush(element: T) {
        back.add(0, element)
    }
    fun middleBackReplace(element: T) {
        back.set(0, element)
    }
    fun middleBackPop() {
        back.removeFirst()
    }
    fun middleBackPop(i: Int) {
        for (j in 0..i - 1) {
            middleBackPop()
        }
    }

    //List functions
    override val size: Int
        get() = front.size + back.size
    override fun add(element: T): Boolean {
        return back.add(element)
    }
    override fun addAll(elements: Collection<T>): Boolean {
        return back.addAll(elements)
    }
    override fun addAll(index: Int, elements: Collection<T>): Boolean {
        if (index < front.size) {
            return back.addAll(index, elements)
        }
        else {
            return front.addAll(index, elements)
        }
    }
    override fun clear() {
        front.clear()
        back.clear()
    }
    override fun set(index: Int, element: T): T {
        if (index < front.size) {
            front[index] = element
        }
        else if (index < front.size + back.size){
            back[index - front.size] = element
        }
        else {
            throw IndexOutOfBoundsException()
        }

        return element
    }
    override fun add(index: Int, element: T) {
        if (index < front.size) {
            front.add(index, element)
        }
        else if (index < front.size + back.size){
            back.add(index - front.size, element)
        }
        else {
            throw IndexOutOfBoundsException()
        }
    }
    override fun listIterator(): MutableListIterator<T> {
        return MutableSplitListIterator(this, 0)
    }
    override fun listIterator(index: Int): MutableListIterator<T> {
        return MutableSplitListIterator(this, index)
    }
    override fun subList(fromIndex: Int, toIndex: Int): MutableList<T> {
        val list = ArrayList<T>()
        if (fromIndex < front.size) {
            if (toIndex <= front.size) {
                list.addAll(front.subList(fromIndex, toIndex))
            }
            else {
                list.addAll(front.subList(fromIndex, front.size))
                list.addAll(back.subList(0, toIndex - front.size))
            }
        }
        else {
            list.addAll(back.subList(fromIndex - front.size, toIndex - front.size))
        }

        return list
    }
    override fun isEmpty(): Boolean {
        return front.isEmpty() && back.isEmpty()
    }
    override fun iterator(): MutableIterator<T> {
        return MutableSplitListIterator(this, 0)
    }
    override fun get(index: Int): T {
        if (index < front.size) {
            return front[index]
        }
        else if (index < front.size + back.size){
            return back[index - front.size]
        }
        else {
            throw IndexOutOfBoundsException()
        }
    }
    override fun toArray(): Array<out Any> {
        return front.toArray().concatAll(back.toArray())
    }
    fun reversed(): SplitList<T> {
        return SplitList(back.reversed(), front.reversed())
    }
}
