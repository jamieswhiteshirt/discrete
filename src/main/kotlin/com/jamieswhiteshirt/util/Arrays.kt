package com.jamieswhiteshirt.util

import java.util.*

fun<T> Array<T>.concatElement(element: T): Array<T> {
    val index = size
    val result = Arrays.copyOf(this, index + 1)
    result[index] = element
    return result
}

fun<T> Array<T>.concatAll(elements: Array<out T>): Array<T> {
    val thisSize = size
    val arraySize = elements.size
    val result = Arrays.copyOf(this, thisSize + arraySize)
    System.arraycopy(elements, 0, result, thisSize, arraySize)
    return result
}
